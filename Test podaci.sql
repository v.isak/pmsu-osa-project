INSERT INTO shop.admin VALUES (
1, 
FALSE, 
"Marko", 
"admin",
 "$2a$09$Qjy.UDoiLfirlQ1RvEJH1OO4PrP0sCWnTCD1WqbRn0UkP8K/JUri2",
 "Marcicevic",
 "ADMINISTRATOR");
 
INSERT INTO shop.kupac VALUES (
2, 
FALSE, 
"Nikola", 
"kupac",
 "$2a$09$FhlqLkNktU7RS9UebecG6eeIOYrOXzHUPZ086pOmSn2AHjICAjNqC",
 "Nikolic",
 "KUPAC",
 "Radnicka 23");
 
INSERT INTO shop.prodavac VALUES (
3, 
FALSE, 
"Milos", 
"prodavac",
 "$2a$09$EK1aRhHuu6XG.xpejBSrmeMR0rhVdz9YDq9vXU9Q2KI1rCpcoz382",
 "Milosevic",
 "PRODAVAC",
 "Zeleznicka 15",
 "milos.milosevic@gmail.com",
 "TechDeal",
 "2021-06-23 15:00",
 "slike\\11");
 
INSERT INTO shop.artikl VALUES (
 4,
 7000,
 "Tablet Xiaomi 6GB RAM",
 "Novo, najbolje ponude!",
 "slike\\1",
 3);
 
INSERT INTO shop.artikl VALUES (
 5,
 1299,
 "Wifi zvucnici Genius",
 "Najbolji kvalitet.",
 "slike\\2",
 3);
 
INSERT INTO shop.artikl VALUES (
 6,
 2000,
 "USB 64GB NOVO !!!",
 "----- TechDeal ----- traje jos 2 dana.",
 "slike\\3",
 3);
 
INSERT INTO shop.artikl VALUES (
 7,
 99700,
 "HP Laptop i7 3790K GX 450",
 "Najbolji u svojoj generaciji!",
 "slike\\4",
 3);
 
 INSERT INTO shop.akcija VALUES (
 8,
 "2021-06-30 15:00",
 "2021-06-23 15:00",
 50,
 "AKCIJA 50% !!!",
 3
 );
 
INSERT INTO shop.akcija_artikli VALUES (8,6);
INSERT INTO shop.akcija_artikli VALUES (8,7);

INSERT INTO shop.porudzbina VALUES (
9,
FALSE,
FALSE,
7000,
TRUE,
"Porudzbina je malo kasnila, osim toga sve preporuke.",
4.5,
"2021-06-23 12:50",
2);

INSERT INTO shop.stavka VALUES (10, 1, 5, 9);