package rs.shop.rest;

import java.util.List;

import model.dto.Kupac;
import model.dto.Prodavac;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface KupciApiService {

    @GET("Kupci")
    Call<List<Kupac>> getKupci();

    @GET("Kupci/{id}")
    Call<Kupac> getKupac(@Path("id") Long id);

    @GET("Kupci/KorisnickoIme/{username}")
    Call<Kupac> getKupacByUsername(@Path("username") String username);

    @POST("Kupci")
    Call<Kupac> createKupac(@Body Kupac kupac);

    @PUT("Kupci/{id}")
    Call<Kupac> updateKupac(@Path("id") Long id, @Body Kupac kupac);

    @DELETE("Kupci/{id}")
    Call<ResponseBody> deleteKupac(@Path("id") Long id);

}
