package rs.shop.activities;

import android.os.Bundle;
import android.os.Parcelable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

import rs.shop.R;
import rs.shop.fragments.KupacFragment;
import rs.shop.fragments.ProdavacFragment;
import rs.shop.fragments.ProdavciFragment;
import rs.shop.tools.FragmentTransition;

public class KupacActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kupac);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Kupac");
        invalidateOptionsMenu();

        FragmentTransition.to(ProdavciFragment.newInstance(), this, true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
