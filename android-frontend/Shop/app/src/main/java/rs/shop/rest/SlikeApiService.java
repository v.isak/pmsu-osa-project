package rs.shop.rest;

import java.util.List;

import model.dto.Administrator;
import model.dto.Slika;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface SlikeApiService {

    @POST("Slike/Preuzmi")
    Call<Slika> loadSlika(@Body Slika slikaValue);

    @POST("Slike/Sacuvaj")
    Call<Slika> createSlika(@Body Slika slikaValue);

    @POST("Slike/Izmeni")
    Call<Slika> updateSlika(@Body Slika slikaValue);

}
