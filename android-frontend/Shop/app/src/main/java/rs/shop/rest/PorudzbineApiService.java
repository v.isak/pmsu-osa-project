package rs.shop.rest;


import java.util.List;

import model.dto.Artikal;
import model.dto.Porudzbina;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface PorudzbineApiService {
    @GET("Porudzbine")
    Call<List<Porudzbina>> getPorudzbine();

    @GET("Porudzbine/Kupac/{id}")
    Call<List<Porudzbina>> getPorudzbineZaKupca(@Path("id") Long idKupca);

    @GET("Porudzbine/Komentari/Prodavac/{id}")
    Call<List<Porudzbina>> getKomentariZaProdavca(@Path("id") Long idProdavca);

    @GET("Porudzbine/{id}")
    Call<Porudzbina> getPorudzbina(@Path("id") Long id);

    @POST("Porudzbine")
    Call<Porudzbina> createPorudzbina(@Body Porudzbina porudzbina);

    @PUT("Porudzbine/{id}")
    Call<Porudzbina> updatePorudzbina(@Path("id") Long id, @Body Porudzbina porudzbina);

    @DELETE("Porudzbine/{id}")
    Call<ResponseBody> deletePorudzbina(@Path("id") Long id);
}
