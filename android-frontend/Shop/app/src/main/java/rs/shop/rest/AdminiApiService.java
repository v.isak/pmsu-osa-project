package rs.shop.rest;

import java.util.List;

import model.dto.Administrator;
import model.dto.Kupac;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AdminiApiService {

    @GET("Admini")
    Call<List<Administrator>> getAdmini();

    @GET("Admini/{id}")
    Call<Administrator> getAdmin(@Path("id") Long id);

    @GET("Admini/KorisnickoIme/{username}")
    Call<Administrator> getAdminByUsername(@Path("username") String username);

    @POST("Admini")
    Call<Administrator> createAdmin(@Body Administrator Adminstrator);

    @PUT("Admini/{id}")
    Call<Administrator> updateAdmin(@Path("id") Long id, @Body Administrator admin);

    @DELETE("Admini/{id}")
    Call<ResponseBody> deleteAdmin(@Path("id") Long id);

}
