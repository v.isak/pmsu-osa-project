package rs.shop;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import rs.shop.activities.AdminActivity;
import rs.shop.activities.KupacActivity;
import rs.shop.activities.LoginActivity;
import rs.shop.activities.ProdavacActivity;
import rs.shop.rest.PorudzbineCall;
import rs.shop.tools.Token;

/*
 * Aktivnost pravimo tako sto napravimo klasu koja nasledjuje AppCompatActivity klasu.
 * Nasledjivanjem ove klase, dobijamo metode zivotnog ciklusa Aktivnosti.
 * */
public class MainActivity extends AppCompatActivity {

    /*
     * Unutar onCreate metode, postavljamo izgled nase aktivnosti koristeci setContentView
     * U ovoj metodi mozemo dobaviti sve view-e (widget-e, komponente interface-a).
     * Moramo voditi racuna, ovde se ne sme nalaziti kod koji ce blokirati prelazak aktivnosti
     * u naredne metode! To znaci da izvrsavanje dugackih operacija treba izbegavati ovde.
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedpreferences = getSharedPreferences("mypref",
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains("token")) {
            String token = sharedpreferences.getString("token", "");
            if (Token.getUloga(token).equals("ROLE_KUPAC")) {
                Intent intent = new Intent(this, KupacActivity.class);
                intent.putExtra("idKupca", String.valueOf(Token.getId(Token.getToken(this))));
                startActivity(intent);
            } else if (Token.getUloga(token).equals("ROLE_PRODAVAC")) {
                Intent intent = new Intent(this, ProdavacActivity.class);
                intent.putExtra("idProdavca", String.valueOf(Token.getId(Token.getToken(this))));
                startActivity(intent);
            } else if (Token.getUloga(token).equals("ROLE_ADMINISTRATOR")) {
                Intent intent = new Intent(this, AdminActivity.class);
                startActivity(intent);
            }
        }
        else{
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

    }



    @Override
    protected void onResume() {
        super.onResume();
        //Toast.makeText(this, "onResume()",Toast.LENGTH_SHORT).show();
    }

    /*
     * onPause se poziva kada je aktivnost delimicno prekrivena.
     * */
    @Override
    protected void onPause() {
        super.onPause();
        //Toast.makeText(this, "onPause()",Toast.LENGTH_SHORT).show();
    }

    /*
     * onPause se poziva kada je aktivnost u potpunosti prekrivena.
     * */
    @Override
    protected void onStop() {
        super.onStop();
        //Toast.makeText(this, "onResume()",Toast.LENGTH_SHORT).show();
    }

    /*
     * onStart se poziva kada se aktivnost pokrece, nakon nje poziva se onResume
     * i aktivnost je vidljiva..
     * */
    @Override
    protected void onStart() {
        super.onStart();
        //Toast.makeText(this, "onStart()",Toast.LENGTH_SHORT).show();
    }
}
