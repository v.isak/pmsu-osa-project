package rs.shop.rest;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import model.dto.Kupac;
import model.dto.Prodavac;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rs.shop.R;
import rs.shop.tools.Token;

public class KupciCall {

    static final String TAG = KupciCall.class.getSimpleName();
    static List<Kupac> kupci = new ArrayList<Kupac>();
    static Kupac kupac;
    static final String BASE_URL = Token.BASE_URL;
    static Retrofit retrofit = null;

    public static List<Kupac> getKupci(Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        KupciApiService kupciApiService = retrofit.create(KupciApiService.class);
        Call<List<Kupac>> call = kupciApiService.getKupci();

        call.enqueue(new Callback<List<Kupac>>() {
            @Override
            public void onResponse(Call<List<Kupac>> call, Response<List<Kupac>> response) {
                System.out.println(response.body());
                kupci.clear();
                if (response.body()!= null) {
                    kupci.addAll(response.body());
                }
                System.out.println(kupci);
            }
            @Override
            public void onFailure(Call<List<Kupac>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return kupci;

    }

    public static Kupac getKupac(Long idKupca, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        KupciApiService kupciApiService = retrofit.create(KupciApiService.class);
        Call<Kupac> call = kupciApiService.getKupac(idKupca);

        call.enqueue(new Callback<Kupac>() {
            @Override
            public void onResponse(Call<Kupac> call, Response<Kupac> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    kupac = response.body();
                    TextView ime = (TextView) activity.findViewById(R.id.unosRegIme);
                    TextView prezime = (TextView) activity.findViewById(R.id.unosRegPrezime);
                    TextView username = (TextView) activity.findViewById(R.id.unosRegUsername);
                    TextView adresa = (TextView) activity.findViewById(R.id.unosRegAdresa);

                    ime.setText(kupac.getImeKorisnika());
                    prezime.setText(kupac.getPrezimeKorisnika());
                    username.setText(kupac.getKorisnickoIme());
                    adresa.setText(kupac.getAdresa());
                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Kupac> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return kupac;

    }

    public static void setNazivKupca(Long idKupca, final Activity activity, TextView nazivKupca){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        KupciApiService kupciApiService = retrofit.create(KupciApiService.class);
        Call<Kupac> call = kupciApiService.getKupac(idKupca);

        call.enqueue(new Callback<Kupac>() {
            @Override
            public void onResponse(Call<Kupac> call, Response<Kupac> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    kupac = response.body();
                    nazivKupca.setText(kupac.getKorisnickoIme());
                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Kupac> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }

    public static Kupac getKupacByUsername(String username, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        KupciApiService kupciApiService = retrofit.create(KupciApiService.class);
        Call<Kupac> call = kupciApiService.getKupacByUsername(username);

        call.enqueue(new Callback<Kupac>() {
            @Override
            public void onResponse(Call<Kupac> call, Response<Kupac> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.isSuccessful()){
                    System.out.println("GetProdavacByUsername: SUCCESSFUL");
                    activity.finish();
                    kupac = response.body();
                }
                else{
                    System.out.println("GetProdavacByUsername: UNSUCCESSFUL");
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<Kupac> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return kupac;

    }

    public static Kupac createKupac(Kupac kupacInfo, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        KupciApiService kupciApiService = retrofit.create(KupciApiService.class);
        Call<Kupac> call = kupciApiService.createKupac(kupacInfo);

        call.enqueue(new Callback<Kupac>() {
            @Override
            public void onResponse(Call<Kupac> call, Response<Kupac> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 201){
                    activity.finish();
                    kupac = response.body();
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Kupac> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return kupac;

    }

    public static Kupac updateKupac(Kupac kupacInfo, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        KupciApiService kupciApiService = retrofit.create(KupciApiService.class);
        Call<Kupac> call = kupciApiService.updateKupac(kupacInfo.getIdKorisnika(),kupacInfo);

        call.enqueue(new Callback<Kupac>() {
            @Override
            public void onResponse(Call<Kupac> call, Response<Kupac> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    activity.finish();
                    kupac = response.body();
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Kupac> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return kupac;

    }

    public static Kupac deleteKupac(Long idKupca, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        KupciApiService kupciApiService = retrofit.create(KupciApiService.class);
        Call<ResponseBody> call = kupciApiService.deleteKupac(idKupca);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    activity.finish();
                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return kupac;

    }

}
