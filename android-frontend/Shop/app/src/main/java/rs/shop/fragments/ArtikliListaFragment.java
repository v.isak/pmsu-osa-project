package rs.shop.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import model.dto.Artikal;
import rs.shop.R;
import rs.shop.activities.ArtiklDetailActivity;
import rs.shop.activities.LoginActivity;
import rs.shop.adapters.ArtikliAdapter;
import rs.shop.adapters.ListArtikliAdapter;
import rs.shop.rest.ArtikliCall;
import rs.shop.rest.KorisnikCall;
import rs.shop.tools.FragmentTransition;
import rs.shop.tools.Token;

public class ArtikliListaFragment extends Fragment {

    public static ArtikliListaFragment newInstance() {
        return new ArtikliListaFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle data) {
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.map_layout, container, false);
        RecyclerView recyclerView = (RecyclerView)v.findViewById(R.id.recyclerView);
        Long idAkcije = Long.valueOf(getActivity().getIntent().getStringExtra("idAkcije"));
        Long idProdavca = Long.valueOf(getActivity().getIntent().getStringExtra("idProdavca"));
        ArtikliCall.getArtikliZaAkciju(idAkcije,idProdavca,getActivity(),recyclerView);

        return v;
    }


    @Override
    public void onResume() {
        super.onResume();

        // update-uje se recycler view odnosno podaci u njemu
        RecyclerView recyclerView = (RecyclerView)getView().findViewById(R.id.recyclerView);
        Long idAkcije = Long.valueOf(getActivity().getIntent().getStringExtra("idAkcije"));
        Long idProdavca = Long.valueOf(getActivity().getIntent().getStringExtra("idProdavca"));
        ArtikliCall.getArtikliZaAkciju(idAkcije,idProdavca,getActivity(),recyclerView);

    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        // ovo korostimo ako je nasa arhitekrura takva da imamo jednu aktivnost
        // i vise fragmentaa gde svaki od njih ima svoj menu unutar toolbar-a
        menu.clear();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_new){
            Intent intent = new Intent(getActivity(), ArtiklDetailActivity.class);
            startActivity(intent);
        }

        else if(id == R.id.action_akcije){
            FragmentTransition.to(AkcijeFragment.newInstance(), getActivity(), false);
        }

        else if(id == R.id.action_artikli){
            FragmentTransition.to(this, getActivity(), false);
        }

        else if(id == R.id.action_komentari){
            FragmentTransition.to(KomentariFragment.newInstance(), getActivity(), false);
        }

        else if(id == R.id.action_odjava){
            KorisnikCall.odjava(getActivity());
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }


}
