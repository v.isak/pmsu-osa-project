package rs.shop.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import model.dto.Artikal;
import model.dto.Slika;
import rs.shop.R;
import rs.shop.activities.ArtiklDetailActivity;
import rs.shop.rest.AkcijeCall;
import rs.shop.rest.SlikeCall;

/*
* Adapteri unutar Android-a sluze da prikazu unapred nedefinisanu kolicinu podataka
* pristigle sa interneta ili ucitane iz baze ili filesystem-a uredjaja.
* Da bi napravili adapter treba da napraivmo klasu, koja nasledjuje neki od postojecih adaptera.
* Za potrebe ovih vezbi koristicemo BaseAdapter koji je sposoban da kao izvor podataka iskoristi listu ili niz.
* Nasledjivanjem bilo kog adaptera, dobicemo
* nekolkko metoda koje moramo da referinisemo da bi adapter ispravno radio.
* */
public class ListArtikliKorpaAdapter extends RecyclerView.Adapter<ListArtikliKorpaAdapter.ViewHolder>{
    private List<Artikal> artikli;
    HashMap<Artikal, Integer> stavke = new HashMap<Artikal, Integer>();
    private Activity activity;

    public ListArtikliKorpaAdapter(List<Artikal> artikliZaProdavca, Activity activity) {

        HashMap<Artikal, Integer> stavke = new HashMap<Artikal, Integer>();

        // prolazenje kroz svaki od artikla jednom i izracunavanje koliko se puta nalazi u listi
        // odnosno koja je kolicina odabrana
        for (Artikal artikal : artikliZaProdavca) {
            int kolicina = 0;

            // ako se id nijednom ne poklopi znaci da se korisnik nije ubacio
            // taj artikal u korpu
            boolean pronadjen = false;
            for (String idArtikla : activity.getIntent().getStringArrayListExtra("artikliKorpa")) {
                if (artikal.getIdArtikla() == Long.valueOf(idArtikla)) {
                    kolicina += 1;
                    pronadjen = true;
                }
            }
            if (pronadjen) {
                stavke.put(artikal, kolicina);
            }
        }

        this.stavke = stavke;
        this.artikli = new ArrayList<Artikal>(stavke.keySet());

        TextView ukupnaCenaView = activity.findViewById(R.id.ukupnaCenaKorpa);
        if (stavke.size() > 0){
            /*float ukupnaCena = 0;
            for (Artikal artikal : stavke.keySet()){
                ukupnaCena += artikal.getCena()*stavke.get(artikal);
            }*/
            Long idProdavca = Long.valueOf(activity.getIntent().getStringExtra("idProdavca"));
            AkcijeCall.setUkupnaCena(activity,ukupnaCenaView, stavke, idProdavca);
        }
        else{
            ukupnaCenaView.setText("Ukupna cena: 0");
        }

        this.activity = activity;

    }



    @NonNull
    @Override
    public ListArtikliKorpaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.artikal_list_korpa, viewGroup, false);

        return new ListArtikliKorpaAdapter.ViewHolder(v);
    }


    @Override
    public int getItemCount() {
        return artikli.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView naziv;
        private final TextView opis;
        private final TextView cenaSaAkcijom;
        private final TextView cena;
        private final TextView kolicina;
        private final ImageView slika;

        ViewHolder(View v){
            super(v);
            naziv = (TextView)v.findViewById(R.id.nazivArtikalList);
            opis = (TextView)v.findViewById(R.id.opisArtikalList);
            cena = (TextView)v.findViewById(R.id.cenaArtikalList);
            cenaSaAkcijom = (TextView)v.findViewById(R.id.cenaSaAkcijomArtikalList);
            kolicina = (TextView)v.findViewById(R.id.kolicinaArtikala);
            slika = (ImageView)v.findViewById(R.id.slikaArtikalList);

        }
    }

    // prikazivanje elementa u recyclerview-u
    @Override
    public void onBindViewHolder(@NonNull ListArtikliKorpaAdapter.ViewHolder viewHolder, int position) {
        Artikal artikal = artikli.get(position);
        viewHolder.naziv.setText(artikal.getNaziv());
        viewHolder.opis.setText(artikal.getOpis());
        viewHolder.cena.setText("Pocetna cena: " + String.valueOf(artikal.getCena()) + " RSD");
        AkcijeCall.setCenaSaAkcijom(activity, viewHolder.cenaSaAkcijom, artikal, artikal.getIdProdavca());
        viewHolder.kolicina.setText(viewHolder.kolicina.getText()+String.valueOf(stavke.get(artikal)));

        // postavljanje slike ako postoji
        if (!artikal.getPutanjaSlike().equals("")) {
            Slika slika = new Slika();
            slika.setSlikaRelativePath(artikal.getPutanjaSlike());

            SlikeCall.loadSlika(slika, viewHolder.slika, null, activity);
        }

        // kad korisnik klikne na element iz liste?
        SharedPreferences sharedpreferences = activity.getSharedPreferences("mypref",
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains("token")) {
            viewHolder.naziv.setOnClickListener((view) -> {
                Context context = view.getContext();
                Intent intent = new Intent(activity, ArtiklDetailActivity.class);
                intent.putExtra("idArtikla", String.valueOf(artikal.getIdArtikla()));
                intent.putExtra("idProdavca", String.valueOf(artikal.getIdProdavca()));
                intent.putExtra("naziv", artikal.getNaziv());
                intent.putExtra("opis", artikal.getOpis());
                intent.putExtra("cena", String.valueOf(artikal.getCena()));
                intent.putExtra("putanjaSlike", artikal.getPutanjaSlike());
                context.startActivity(intent);
                activity.finish();
            });
        }

    }
}
