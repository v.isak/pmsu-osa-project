package rs.shop.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import model.dto.Porudzbina;
import rs.shop.R;
import rs.shop.activities.KomentarActivity;
import rs.shop.activities.PorudzbinaDetailActivity;
import rs.shop.rest.KupciCall;
import rs.shop.rest.PorudzbineCall;
import rs.shop.tools.Token;

/*
* Adapteri unutar Android-a sluze da prikazu unapred nedefinisanu kolicinu podataka
* pristigle sa interneta ili ucitane iz baze ili filesystem-a uredjaja.
* Da bi napravili adapter treba da napraivmo klasu, koja nasledjuje neki od postojecih adaptera.
* Za potrebe ovih vezbi koristicemo BaseAdapter koji je sposoban da kao izvor podataka iskoristi listu ili niz.
* Nasledjivanjem bilo kog adaptera, dobicemo
* nekolkko metoda koje moramo da referinisemo da bi adapter ispravno radio.
* */
public class KomentariAdapter extends RecyclerView.Adapter<KomentariAdapter.ViewHolder>{
    private Activity activity;
    List<Porudzbina> porudzbine;

    public KomentariAdapter(List<Porudzbina> komentariZaProdavca, Activity activity) {

        if (Token.getUloga(Token.getToken(activity)).equals("ROLE_KUPAC")){
            // nearhivirane porudzbine koje se prikazuju kupcima
            List<Porudzbina> nearhiviraniKomentari = new ArrayList<Porudzbina>();
            for (Porudzbina komentar : komentariZaProdavca){
                if (komentar.isArhiviraniKomentar() == false){
                    nearhiviraniKomentari.add(komentar);
                }
            }
            this.porudzbine = nearhiviraniKomentari;
        }
        else {
            this.porudzbine = komentariZaProdavca;
        }
        this.activity = activity;
    }


    @NonNull
    @Override
    public KomentariAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View v = null;
        SharedPreferences sharedpreferences = activity.getSharedPreferences("mypref",
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains("token")) {
            if (Token.getUloga(Token.getToken(activity)).equals("ROLE_KUPAC")){
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.komentar_list, viewGroup, false);
            }
            else if (Token.getUloga(Token.getToken(activity)).equals("ROLE_PRODAVAC")){
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.komentar_list_prodavac, viewGroup, false);
            }
        }

        return new KomentariAdapter.ViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return porudzbine.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView kupac;
        private final TextView komentar;
        private final TextView ocena;
        private final Button btnArhivirajKomentar;


        ViewHolder(View v){
            super(v);
            kupac = (TextView)v.findViewById(R.id.kupacKomentar);
            komentar = (TextView)v.findViewById(R.id.komentarKomentar);
            if (Token.getUloga(Token.getToken(activity)).equals("ROLE_KUPAC")) {
                ocena = (TextView) v.findViewById(R.id.ocenaKomentar);
                btnArhivirajKomentar = null;
            }
            else if (Token.getUloga(Token.getToken(activity)).equals("ROLE_PRODAVAC")) {
                ocena = null;
                btnArhivirajKomentar = (Button) v.findViewById(R.id.btnArhivirajKomentar);
            }
            else{
                ocena = null;
                btnArhivirajKomentar = null;
            }
        }
    }

    // prikazivanje elementa u recyclerview-u
    @Override
    public void onBindViewHolder(@NonNull KomentariAdapter.ViewHolder viewHolder, int position) {
        Porudzbina porudzbina = porudzbine.get(position);
        viewHolder.komentar.setText(porudzbina.getKomentar());

        // kad korisnik klikne na element iz liste?
        SharedPreferences sharedpreferences = activity.getSharedPreferences("mypref",
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains("token")) {

            if (Token.getUloga(Token.getToken(activity)).equals("ROLE_PRODAVAC")){

                if (porudzbina.isArhiviraniKomentar()){
                    viewHolder.btnArhivirajKomentar.setText("PRIKAZI");
                }

                if (porudzbina.isAnonimanKomentar()) {
                    viewHolder.kupac.setText("Anoniman");
                }
                else{
                    KupciCall.setNazivKupca(porudzbina.getIdKupca(),activity,viewHolder.kupac);
                }

                viewHolder.btnArhivirajKomentar.setOnClickListener((view) -> {
                    if (viewHolder.btnArhivirajKomentar.getText().equals("ARHIVIRAJ")){
                        Porudzbina porudzbinaNull = new Porudzbina();
                        porudzbinaNull.setArhiviraniKomentar(true);
                        PorudzbineCall.updatePorudzbina(porudzbina.getIdPorudzbine(), porudzbinaNull, activity);
                        viewHolder.btnArhivirajKomentar.setText("PRIKAZI");
                        porudzbina.setArhiviraniKomentar(true);
                    }
                    else if (viewHolder.btnArhivirajKomentar.getText().equals("PRIKAZI")){
                        Porudzbina porudzbinaNull = new Porudzbina();
                        porudzbinaNull.setArhiviraniKomentar(false);
                        PorudzbineCall.updatePorudzbina(porudzbina.getIdPorudzbine(), porudzbinaNull, activity);
                        viewHolder.btnArhivirajKomentar.setText("ARHIVIRAJ");
                        porudzbina.setArhiviraniKomentar(false);
                    }
                });
            }
            else if (Token.getUloga(Token.getToken(activity)).equals("ROLE_KUPAC")){
                viewHolder.ocena.setText(String.valueOf(porudzbina.getOcena()) + viewHolder.ocena.getText());
                // ako je komentar od ulogovanog korisnika on nece biti naznan "Anoniman"
                // za korisnika iako je postavljen kao anoniman
                if (porudzbina.getIdKupca() == Token.getId(Token.getToken(activity))
                 && !porudzbina.isAnonimanKomentar()){
                    viewHolder.kupac.setText("Vi");
                    //KupciCall.setNazivKupca(porudzbina.getIdKupca(),activity,viewHolder.kupac);
                }
                else if (porudzbina.isAnonimanKomentar()) {
                    viewHolder.kupac.setText("Anoniman");
                }
                else{
                    KupciCall.setNazivKupca(porudzbina.getIdKupca(),activity,viewHolder.kupac);
                }
            }
        }
        else{
            viewHolder.ocena.setText(String.valueOf(porudzbina.getOcena()) + viewHolder.ocena.getText());
        }
    }

}
