package rs.shop.rest;

import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import model.dto.Akcija;
import model.dto.Artikal;
import model.dto.Stavka;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rs.shop.adapters.AkcijeAdapter;
import rs.shop.adapters.ArtikliAdapter;
import rs.shop.tools.Token;

public class AkcijeCall {

    static final String TAG = AkcijeCall.class.getSimpleName();
    static List<Akcija> akcije = new ArrayList<Akcija>();
    static Akcija akcija;
    static final String BASE_URL = Token.BASE_URL;
    static Retrofit retrofit = null;

    public static List<Akcija> getAkcije(Activity activity, RecyclerView recyclerView){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        AkcijeApiService AkcijeApiService = retrofit.create(AkcijeApiService.class);
        Call<List<Akcija>> call = AkcijeApiService.getAkcije();

        call.enqueue(new Callback<List<Akcija>>() {
            @Override
            public void onResponse(Call<List<Akcija>> call, Response<List<Akcija>> response) {
                System.out.println(response.body());
                akcije.clear();
                if (response.body()!= null) {
                    akcije.addAll(response.body());
                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    recyclerView.setAdapter(new AkcijeAdapter(akcije,activity));

                }
                System.out.println(akcije);
            }
            @Override
            public void onFailure(Call<List<Akcija>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return akcije;

    }


    public static void getAkcijeZaProdavca(Long idProdavca, Activity activity, RecyclerView recyclerView){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        AkcijeApiService AkcijeApiService = retrofit.create(AkcijeApiService.class);
        Call<List<Akcija>> call = AkcijeApiService.getAkcijeZaProdavca(idProdavca);

        call.enqueue(new Callback<List<Akcija>>() {
            @Override
            public void onResponse(Call<List<Akcija>> call, Response<List<Akcija>> response) {
                System.out.println(response.body());
                akcije.clear();
                if (response.body()!= null) {
                    akcije.addAll(response.body());
                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    recyclerView.setAdapter(new AkcijeAdapter(akcije,activity));

                }
                System.out.println(akcije);
            }
            @Override
            public void onFailure(Call<List<Akcija>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }


    public static void setCenaSaAkcijom(Activity activity, TextView cenaSaAkcijomView, Artikal artikal, Long idProdavca){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        AkcijeApiService AkcijeApiService = retrofit.create(AkcijeApiService.class);
        Call<List<Akcija>> call = AkcijeApiService.getAkcijeZaProdavca(idProdavca);

        call.enqueue(new Callback<List<Akcija>>() {
            @Override
            public void onResponse(Call<List<Akcija>> call, Response<List<Akcija>> response) {
                System.out.println(response.body());
                akcije.clear();
                if (response.body()!= null) {
                    akcije.addAll(response.body());

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    double pocetnaCena = artikal.getCena();
                    for (Akcija akcija : akcije){
                        for (Long idArtikla : akcija.getArtikli()){
                            try {
                                if (idArtikla == artikal.getIdArtikla() && sdf.parse(akcija.getKrajAkcije().replace("T"," ")).after(new Date())){
                                    System.out.println("POKLOP ARTIKALA");
                                    System.out.println("Cena bez akcije" + artikal.getCena());
                                    artikal.setCena(artikal.getCena() - (pocetnaCena * (Double.valueOf(akcija.getProcenat())/100.0)));
                                    System.out.println("Cena sa akcijom" + artikal.getCena());
                                    break;
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    cenaSaAkcijomView.setText("Cena sa akcijom: " + String.valueOf(artikal.getCena()) + " RSD");
                }
                System.out.println(akcije);
            }
            @Override
            public void onFailure(Call<List<Akcija>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }

    public static void setUkupnaCena(Activity activity, TextView ukupnaCenaView, HashMap<Artikal, Integer> stavke, Long idProdavca){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        AkcijeApiService AkcijeApiService = retrofit.create(AkcijeApiService.class);
        Call<List<Akcija>> call = AkcijeApiService.getAkcijeZaProdavca(idProdavca);

        call.enqueue(new Callback<List<Akcija>>() {
            @Override
            public void onResponse(Call<List<Akcija>> call, Response<List<Akcija>> response) {
                System.out.println(response.body());
                akcije.clear();
                if (response.body()!= null) {
                    akcije.addAll(response.body());

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                    double ukupnaCena = 0;
                    for (Artikal artikal : stavke.keySet()) {
                        double pocetnaCena = artikal.getCena();
                        double cenaSaAkcijom = artikal.getCena();
                        for (Akcija akcija : akcije) {
                            for (Long idArtikla : akcija.getArtikli()) {
                                try {
                                    if (idArtikla == artikal.getIdArtikla() && sdf.parse(akcija.getKrajAkcije().replace("T", " ")).after(new Date())) {
                                        //System.out.println("POKLOP ARTIKALA");
                                        cenaSaAkcijom -= pocetnaCena * (Double.valueOf(akcija.getProcenat()) / 100.0);
                                        break;
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        ukupnaCena += cenaSaAkcijom * stavke.get(artikal);
                    }
                    ukupnaCenaView.setText("Ukupna cena: " + String.valueOf(ukupnaCena) + " RSD");
                }
                System.out.println(akcije);
            }
            @Override
            public void onFailure(Call<List<Akcija>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }

    public static Akcija createAkcija(Akcija akcijaInfo, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        AkcijeApiService AkcijeApiService = retrofit.create(AkcijeApiService.class);
        Call<Akcija> call = AkcijeApiService.createAkcija(akcijaInfo);

        call.enqueue(new Callback<Akcija>() {
            @Override
            public void onResponse(Call<Akcija> call, Response<Akcija> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 201){
                    activity.finish();
                    akcija = response.body();
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Akcija> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return akcija;

    }

    public static Akcija updateAkcija(Akcija akcijaInfo, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        AkcijeApiService AkcijeApiService = retrofit.create(AkcijeApiService.class);
        Call<Akcija> call = AkcijeApiService.updateAkcija(akcijaInfo.getIdAkcije(),akcijaInfo);

        call.enqueue(new Callback<Akcija>() {
            @Override
            public void onResponse(Call<Akcija> call, Response<Akcija> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    activity.finish();
                    akcija = response.body();
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Akcija> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return akcija;

    }

    public static Akcija deleteAkcija(Long idAkcije, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        AkcijeApiService AkcijeApiService = retrofit.create(AkcijeApiService.class);
        Call<ResponseBody> call = AkcijeApiService.deleteAkcija(idAkcije);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    activity.finish();
                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return akcija;

    }

}
