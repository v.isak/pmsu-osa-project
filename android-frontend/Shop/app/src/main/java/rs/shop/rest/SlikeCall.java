package rs.shop.rest;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.material.appbar.AppBarLayout;

import java.util.ArrayList;
import java.util.List;

import model.dto.Administrator;
import model.dto.Artikal;
import model.dto.Slika;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rs.shop.R;
import rs.shop.activities.ArtiklDetailActivity;
import rs.shop.tools.Token;

public class SlikeCall {

    static final String TAG = SlikeCall.class.getSimpleName();
    static List<Administrator> admini = new ArrayList<Administrator>();
    static Administrator admin;
    static final String BASE_URL = Token.BASE_URL;
    static Retrofit retrofit = null;



    public static void createSlika(Slika slikaValue, Artikal artikl, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        SlikeApiService SlikeApiService = retrofit.create(SlikeApiService.class);
        Call<Slika> call = SlikeApiService.createSlika(slikaValue);

        call.enqueue(new Callback<Slika>() {
            @Override
            public void onResponse(Call<Slika> call, Response<Slika> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 201){
                    Slika slika = response.body();
                    artikl.setPutanjaSlike(slika.getSlikaRelativePath());
                    ArtikliCall.createArtikal(artikl, slika, activity);
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Slika> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }


    public static void loadSlika(Slika slikaValue, ImageView slikaView, ConstraintLayout backgroundLayout, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        SlikeApiService SlikeApiService = retrofit.create(SlikeApiService.class);
        Call<Slika> call = SlikeApiService.loadSlika(slikaValue);

        call.enqueue(new Callback<Slika>() {
            @Override
            public void onResponse(Call<Slika> call, Response<Slika> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    Slika slika = response.body();

                    try {
                        byte [] encodeByte= Base64.decode(slika.getSlikaString(),Base64.DEFAULT);
                        Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
                        // provjerava se da li se radi o slici za artikl ili slici za prodavca
                        if (slikaView != null) {
                            slikaView.setImageBitmap(bitmap);
                        }
                        if (backgroundLayout != null){
                            // konvertovanje u drawable jer ConstraintLayout prihvata drawable kao background
                            BitmapDrawable bitmapDrawable = new BitmapDrawable(activity.getResources(), bitmap);
                            backgroundLayout.setBackground(bitmapDrawable);
                        }
                    } catch(Exception e) {
                        e.getMessage();
                    }

                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Slika> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }

    public static Administrator updateAdmin(Administrator adminInfo, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        AdminiApiService AdminiApiService = retrofit.create(AdminiApiService.class);
        Call<Administrator> call = AdminiApiService.updateAdmin(adminInfo.getIdKorisnika(),adminInfo);

        call.enqueue(new Callback<Administrator>() {
            @Override
            public void onResponse(Call<Administrator> call, Response<Administrator> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    activity.finish();
                    admin = response.body();
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Administrator> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return admin;

    }

}
