package rs.shop.activities;

import android.os.Bundle;
import android.view.Menu;
import android.widget.RatingBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import model.dto.Slika;
import rs.shop.R;
import rs.shop.adapters.ArtikliAdapter;
import rs.shop.fragments.ArtikliFragment;
import rs.shop.fragments.ProdavacFragment;
import rs.shop.rest.PorudzbineCall;
import rs.shop.rest.ProdavciCall;
import rs.shop.rest.SlikeCall;
import rs.shop.tools.FragmentTransition;

public class ProdavacActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Long idProdavca = Long.valueOf(getIntent().getStringExtra("idProdavca"));
        getIntent().putExtra("putanjaSlike","");
        ProdavciCall.setPodaciZaProdavca(idProdavca, ProdavacActivity.this);

        // postavljanje u ProdavciCall.setPodaciZaProdavca()

        /*RatingBar ratingBar = findViewById(R.id.ratingBar);
        ratingBar.setEnabled(false);
        if (getIntent().getStringExtra("prosecnaCena") != null) {
            float prosecnaOcena = Float.valueOf(getIntent().getStringExtra("prosecnaOcena"));
            ratingBar.setRating(prosecnaOcena);

            //getSupportActionBar().setTitle(getSupportActionBar().getTitle() + "\n" + "Prosecna ocena: " + getIntent().getStringExtra("prosecnaOcena"));
        }
        else{
            PorudzbineCall.setOcenaZaProdavca(ProdavacActivity.this, idProdavca, null, ratingBar);
        }*/
        //getSupportActionBar().setTitle("Prodavac");

        //invalidateOptionsMenu();

        //FragmentTransition.to(ArtikliFragment.newInstance(), this, true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
