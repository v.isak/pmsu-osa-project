package rs.shop.activities;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import model.dto.Akcija;
import model.dto.Prijava;
import model.dto.Registracija;
import model.enums.TipKorisnika;
import pub.devrel.easypermissions.EasyPermissions;
import rs.shop.R;
import rs.shop.fragments.ArtikliListaFragment;
import rs.shop.rest.AdminiCall;
import rs.shop.rest.AkcijeCall;
import rs.shop.rest.KorisnikCall;
import rs.shop.rest.KupciCall;
import rs.shop.rest.ProdavciCall;
import rs.shop.tools.FragmentTransition;
import rs.shop.tools.Token;

public class KorisnikDetailActivity extends AppCompatActivity {

    /// KAMERA
    private String slikaString = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_korisnik_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView adresa = (TextView) findViewById(R.id.unosRegAdresa);
        TextView adresaTextView = (TextView) findViewById(R.id.adresaTextView);
        TextView email = (TextView) findViewById(R.id.unosRegEmail);
        TextView emailTextView = (TextView) findViewById(R.id.emailTextView);
        TextView naziv = (TextView) findViewById(R.id.unosRegNaziv);
        TextView nazivTextView = (TextView) findViewById(R.id.nazivTextView);
        String uloga = Token.getUloga(Token.getToken(this));

        TextView ukloniSliku = (TextView) findViewById(R.id.ukloniSliku);
        ukloniSliku.setEnabled(false);
        ukloniSliku.setVisibility(View.INVISIBLE);

        TextView slikaResultMsg = (TextView) findViewById(R.id.slikaResultMsg);
        slikaResultMsg.setText("");

        Button photoButton = (Button) this.findViewById(R.id.btnSlika);

        Long idKorisnika = Long.valueOf(Token.getId(Token.getToken(this)));

        findViewById(R.id.unosRegUsername).setFocusable(false);

        if (uloga.equals("ROLE_KUPAC"))
        {
            email.setText("");
            naziv.setText("");
            email.setEnabled(false);
            email.setVisibility(View.GONE);
            emailTextView.setVisibility(View.GONE);
            naziv.setEnabled(false);
            naziv.setVisibility(View.GONE);
            nazivTextView.setVisibility(View.GONE);
            photoButton.setEnabled(false);
            photoButton.setVisibility(View.GONE);
            slikaResultMsg.setVisibility(View.GONE);

            KupciCall.getKupac(idKorisnika, KorisnikDetailActivity.this);
        }

        else if (uloga.equals("ROLE_PRODAVAC")){
            ProdavciCall.getProdavac(idKorisnika, KorisnikDetailActivity.this);

            ///////// KAMERA /////////
            photoButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

                    if (EasyPermissions.hasPermissions(KorisnikDetailActivity.this, galleryPermissions)) {
                        pickImage();
                    } else {
                        EasyPermissions.requestPermissions(KorisnikDetailActivity.this, "Access for storage",
                                101, galleryPermissions);
                    }
                }
            });

            // uklanjanje odabrane slike
            ukloniSliku.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    slikaString = "";
                    slikaResultMsg.setText("");
                    ukloniSliku.setEnabled(false);
                    ukloniSliku.setVisibility(View.GONE);
                }
            });

        }

        else if (uloga.equals("ROLE_ADMINISTRATOR")){
            adresa.setText("");
            email.setText("");
            naziv.setText("");
            adresa.setEnabled(false);
            adresa.setVisibility(View.GONE);
            adresaTextView.setVisibility(View.GONE);
            email.setEnabled(false);
            email.setVisibility(View.GONE);
            emailTextView.setVisibility(View.GONE);
            naziv.setEnabled(false);
            naziv.setVisibility(View.GONE);
            nazivTextView.setVisibility(View.GONE);
            photoButton.setEnabled(false);
            photoButton.setVisibility(View.GONE);
            slikaResultMsg.setVisibility(View.GONE);
            AdminiCall.getAdmin(idKorisnika, KorisnikDetailActivity.this);
        }

        // dugme za registraciju
        Button btnPotvrda = (Button) findViewById(R.id.buttonReg);
        btnPotvrda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView username = (TextView) findViewById(R.id.unosRegUsername);
                TextView password = (TextView) findViewById(R.id.unosRegPassword);

                if (password.getText().toString().equals("")){
                    Toast.makeText(KorisnikDetailActivity.this, "Morate uneti lozinku da biste izmenili podatke", Toast.LENGTH_SHORT).show();
                    return;
                }

                Prijava prijava = new Prijava(username.getText().toString(), password.getText().toString());
                KorisnikCall.izmenaKorisnika(prijava, slikaString, KorisnikDetailActivity.this);

            }
        });

    }


    /// KAMERA
    public void pickImage() {
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 2);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && resultCode == RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);

            ByteArrayOutputStream baos=new  ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
            byte [] b=baos.toByteArray();
            slikaString = Base64.encodeToString(b, Base64.DEFAULT);
            // update-ovanje poruke
            TextView slikaResultMsg = (TextView) findViewById(R.id.slikaResultMsg);
            slikaResultMsg.setText("Slika je uspesno ucitana!");
            // dodavanje dugmeta za uklanjanje selektovane slike
            TextView ukloniSliku = (TextView) findViewById(R.id.ukloniSliku);
            ukloniSliku.setEnabled(true);
            ukloniSliku.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
