package rs.shop.tools;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class Token {

    public static final String BASE_URL = "http://10.0.2.2:8080/Prodavnica/";

    public static String DecodeJWT(String token) throws UnsupportedEncodingException {
        String[] chunks = token.split("\\.");

        // Receiving side
        byte[] header = Base64.decode(chunks[0], Base64.DEFAULT);
        byte[] payload = Base64.decode(chunks[1], Base64.DEFAULT);
        String data = new String(payload, "UTF-8");

        return data;
    }

    public static String getUloga(String token){
        String tokenDecoded = "";
        try {
            tokenDecoded = Token.DecodeJWT(token);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String uloga = "";
        try {
            JSONObject jsonData = new JSONObject(tokenDecoded);
            JSONObject jsonRoleData = new JSONObject(jsonData.getString("role"));
            uloga = jsonRoleData.getString("authority");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return uloga;
    }

    public static String getUsername(String token){
        String tokenDecoded = "";
        try {
            tokenDecoded = Token.DecodeJWT(token);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String username = "";
        try {
            JSONObject jsonData = new JSONObject(tokenDecoded);
            username = jsonData.getString("sub");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return username;
    }

    public static Long getId(String token){
        String tokenDecoded = "";
        try {
            tokenDecoded = Token.DecodeJWT(token);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Long id = null;
        try {
            JSONObject jsonData = new JSONObject(tokenDecoded);
            id = Long.valueOf(jsonData.getString("id"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return id;
    }

    public static String getToken(Activity activity){

        SharedPreferences sharedpreferences = activity.getSharedPreferences("mypref",
                Context.MODE_PRIVATE);
        String token = sharedpreferences.getString("token","");
        return token;

    }

    public static OkHttpClient getTokenAuthHeader(final String token) {
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {

                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        return client;
    }

}
