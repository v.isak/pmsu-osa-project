package rs.shop.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import model.dto.Artikal;
import model.dto.Prodavac;
import model.dto.Slika;
import rs.shop.R;
import rs.shop.activities.ArtiklDetailActivity;
import rs.shop.activities.ProdavacActivity;
import rs.shop.rest.PorudzbineCall;
import rs.shop.rest.ProdavciCall;
import rs.shop.rest.SlikeCall;
import rs.shop.tools.Token;

/*
* Adapteri unutar Android-a sluze da prikazu unapred nedefinisanu kolicinu podataka
* pristigle sa interneta ili ucitane iz baze ili filesystem-a uredjaja.
* Da bi napravili adapter treba da napraivmo klasu, koja nasledjuje neki od postojecih adaptera.
* Za potrebe ovih vezbi koristicemo BaseAdapter koji je sposoban da kao izvor podataka iskoristi listu ili niz.
* Nasledjivanjem bilo kog adaptera, dobicemo
* nekolkko metoda koje moramo da referinisemo da bi adapter ispravno radio.
* */
public class ProdavciAdapter extends RecyclerView.Adapter<ProdavciAdapter.ViewHolder>{
    private Activity activity;
    List<Prodavac> prodavci;

    public ProdavciAdapter(List<Prodavac> prodavci, Activity activity) {

        // Call za dobijanje prosjecne ocijene i onda je
        // treba postavit u view za odredjenog prodavca
        /***********/
        this.prodavci = prodavci;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ProdavciAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.prodavac_list, viewGroup, false);

        return new ProdavciAdapter.ViewHolder(v);
    }


    @Override
    public int getItemCount() {
        return prodavci.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView posludeOd;
        private final TextView emailProdavca;
        private final TextView adresaProdavca;
        private final TextView naziv;
        private final TextView prosecnaOcena;
        private final ImageView slika;

        ViewHolder(View v){
            super(v);
            posludeOd = (TextView)v.findViewById(R.id.posludeOd);
            emailProdavca = (TextView)v.findViewById(R.id.emailProdavca);
            adresaProdavca = (TextView)v.findViewById(R.id.adresaProdavca);
            naziv = (TextView)v.findViewById(R.id.naziv);
            prosecnaOcena = (TextView)v.findViewById(R.id.prosecnaOcena);
            slika = (ImageView)v.findViewById(R.id.slika);
        }
    }

    // prikazivanje elementa u recyclerview-u
    @Override
    public void onBindViewHolder(@NonNull ProdavciAdapter.ViewHolder viewHolder, int position) {
        Prodavac prodavac = prodavci.get(position);
        PorudzbineCall.setOcenaZaProdavca(activity, prodavac.getIdKorisnika(), viewHolder.prosecnaOcena, null);
        viewHolder.posludeOd.setText("Posluje od: " + prodavac.getPoslujeOd().toString().substring(0,16).replace("T"," "));
        viewHolder.emailProdavca.setText("Email: "+ prodavac.getEmail());
        viewHolder.adresaProdavca.setText("Adresa: "+prodavac.getAdresa());
        viewHolder.naziv.setText(prodavac.getNaziv());

        // postavljanje slike ako postoji
        if (!prodavac.getPutanjaSlike().equals("")) {
            Slika slika = new Slika();
            slika.setSlikaRelativePath(prodavac.getPutanjaSlike());

            SlikeCall.loadSlika(slika, viewHolder.slika, null, activity);
        }


        // kad korisnik klikne na element iz liste?
        viewHolder.naziv.setOnClickListener((view) -> {
            Context context = view.getContext();
            Intent intent = new Intent(activity, ProdavacActivity.class);
            intent.putExtra("idProdavca", String.valueOf(prodavac.getIdKorisnika()));
            // za dodavanje artikla u korpu odnosno intent
            intent.putStringArrayListExtra("artikliKorpa", new ArrayList<String>());
            // treba dodati jos prosjecnu ocijenu za prosledjivanje
            /**********/
            intent.putExtra("prosecnaOcena",viewHolder.prosecnaOcena.getText().toString());
            intent.putExtra("putanjaSlike", prodavac.getPutanjaSlike());
            context.startActivity(intent);
            //activity.finish();
        });

    }

}
