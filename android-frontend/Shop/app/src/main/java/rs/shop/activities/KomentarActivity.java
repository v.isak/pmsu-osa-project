package rs.shop.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import model.dto.Artikal;
import model.dto.Porudzbina;
import rs.shop.R;
import rs.shop.rest.ArtikliCall;
import rs.shop.rest.PorudzbineCall;

public class KomentarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_komentar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView komentar = findViewById(R.id.komentarUnos);
        CheckBox anoniman = findViewById(R.id.anonimanUnos);
        RatingBar ocena = findViewById(R.id.ocenaUnos);

        // dugme za potvrdu
        Button btnOceni = (Button) findViewById(R.id.btnOceni);


        String token = rs.shop.tools.Token.getToken(KomentarActivity.this);
        String uloga = rs.shop.tools.Token.getUloga(token);

        // PRODAVAC ILI VEC OCENJENO
        if(Boolean.valueOf(getIntent().getStringExtra("ocenjenaPorudzbina")) ||
                uloga.equals("ROLE_PRODAVAC")){
            btnOceni.setEnabled(false);
            btnOceni.setVisibility(View.INVISIBLE);
        }

        // KUPAC
        else if (uloga.equals("ROLE_KUPAC")){

            btnOceni.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (komentar.getText().toString().equals("") || ocena.getRating() == 0){
                            Toast.makeText(KomentarActivity.this, "Morate popuniti sve podatke", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Porudzbina porudzbina = new Porudzbina();
                            porudzbina.setKomentar(komentar.getText().toString());
                            porudzbina.setAnonimanKomentar(anoniman.isChecked());
                            // ukoliko je ocena manja od 1 postavlja se na 1
                            porudzbina.setOcena(ocena.getRating() < 1.0 ? 1 : ocena.getRating());
                            PorudzbineCall.updatePorudzbina(Long.valueOf(getIntent().getStringExtra("idPorudzbine")), porudzbina, KomentarActivity.this);
                        }
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }
                }
            });


        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
