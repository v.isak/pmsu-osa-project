package rs.shop.rest;


import java.util.List;

import model.dto.Akcija;
import model.dto.Artikal;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AkcijeApiService {
    @GET("Akcije")
    Call<List<Akcija>> getAkcije();

    @GET("Akcije/Prodavac/{id}")
    Call<List<Akcija>> getAkcijeZaProdavca(@Path("id") Long idProdavca);

    @GET("Akcije/{id}")
    Call<Akcija> getAkcija(@Path("id") Long id);

    @POST("Akcije")
    Call<Akcija> createAkcija(@Body Akcija akcija);

    @PUT("Akcije/{id}")
    Call<Akcija> updateAkcija(@Path("id") Long id, @Body Akcija akcija);

    @DELETE("Akcije/{id}")
    Call<ResponseBody> deleteAkcija(@Path("id") Long id);
}
