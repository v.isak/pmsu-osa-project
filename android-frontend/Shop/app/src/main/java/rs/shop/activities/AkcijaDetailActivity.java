package rs.shop.activities;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import model.dto.Akcija;
import model.dto.Artikal;
import rs.shop.R;
import rs.shop.fragments.ArtikliListaFragment;
import rs.shop.rest.AkcijeCall;
import rs.shop.rest.ArtikliCall;
import rs.shop.tools.FragmentTransition;
import rs.shop.tools.Token;

public class AkcijaDetailActivity extends AppCompatActivity {

    private String uloga = "";
    private String token = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_akcija_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView pocetakAkcije = findViewById(R.id.akcijaUnosPocetak);
        TextView krajAkcije = findViewById(R.id.akcijaUnosKraj);
        TextView opis = findViewById(R.id.akcijaOpisUnos);
        TextView procenat = findViewById(R.id.akcijaUnosProcenat);



        // dugme za dodavanje
        Button btnDodajIzmeni = (Button) findViewById(R.id.btnAddAkcija);

        // dugme za brisanje
        Button btnObrisi = (Button) findViewById(R.id.btnObrisiAkcija);

        // IZMENA
        if (getIntent().getStringExtra("opis") != null) {
            getSupportActionBar().setTitle("Izmena akcije");
            pocetakAkcije.setText(getIntent().getStringExtra("pocetakAkcije").substring(0,16).replace("T", " "));
            krajAkcije.setText(getIntent().getStringExtra("krajAkcije").substring(0,16).replace("T", " "));
            opis.setText(getIntent().getStringExtra("opis"));
            procenat.setText(getIntent().getStringExtra("procenat"));
            btnDodajIzmeni.setText("IZMENI");
        }
        else{
            getSupportActionBar().setTitle("Dodavanje akcije");
            btnObrisi.setEnabled(false);
            btnObrisi.setVisibility(View.INVISIBLE);
        }

        token = Token.getToken(this);
        uloga = Token.getUloga(token);

        //
        if (uloga.equals("ROLE_PRODAVAC")){

            btnDodajIzmeni.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {


                        Akcija akcija = new Akcija();
                        akcija.setArtikli(new ArrayList<Long>());
                        // postavljanje datuma i provjera validnosti
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        try {
                            Date pocetakAkcijeDate = sdf.parse(pocetakAkcije.getText().toString());
                            Date krajAkcijeDate = sdf.parse(krajAkcije.getText().toString());

                            if (pocetakAkcijeDate.after(new Date()) && pocetakAkcijeDate.before(krajAkcijeDate) && krajAkcijeDate.after(new Date())){
                                akcija.setPocetakAkcije(pocetakAkcije.getText().toString().replace(" ", "T"));
                                akcija.setKrajAkcije(krajAkcije.getText().toString().replace(" ", "T"));
                            }
                            else{
                                Toast.makeText(AkcijaDetailActivity.this, "Unesite validne datume", Toast.LENGTH_SHORT).show();
                                return;
                            }
                        } catch (ParseException ex) {
                            Toast.makeText(AkcijaDetailActivity.this, "Unesite validan format datuma", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        akcija.setTekst(opis.getText().toString());
                        akcija.setProcenat(Integer.valueOf(procenat.getText().toString()));
                        akcija.setIdProdavca(Token.getId(Token.getToken(AkcijaDetailActivity.this)));

                        for (String idArtikla : getIntent().getStringArrayListExtra("artikli")){
                            akcija.getArtikli().add(Long.valueOf(idArtikla));
                        }

                        if (btnDodajIzmeni.getText().equals("DODAJ")){
                            AkcijeCall.createAkcija(akcija,AkcijaDetailActivity.this);
                        }

                        else if(btnDodajIzmeni.getText().equals("IZMENI")){
                            akcija.setIdAkcije(Long.valueOf(getIntent().getStringExtra("idAkcije")));
                            AkcijeCall.updateAkcija(akcija, AkcijaDetailActivity.this);
                        }
                        /*
                        else{
                            Toast.makeText(ArtiklDetailActivity.this, "Button error", Toast.LENGTH_SHORT).show();
                        }*/
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }
                }
            });


            btnObrisi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        AkcijeCall.deleteAkcija(Long.valueOf(getIntent().getStringExtra("idAkcije")),AkcijaDetailActivity.this);
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }

        // KUPAC
        else if (uloga.equals("ROLE_KUPAC")){
            getSupportActionBar().setTitle("Pregled akcije");
            //pocetakAkcije.setEnabled(false);
            pocetakAkcije.setFocusable(false);
            //opis.setEnabled(false);
            opis.setFocusable(false);
            //krajAkcije.setEnabled(false);
            krajAkcije.setFocusable(false);
            //procenat.setEnabled(false);
            procenat.setFocusable(false);
            btnDodajIzmeni.setEnabled(false);
            btnDodajIzmeni.setVisibility(View.INVISIBLE);
            btnObrisi.setEnabled(false);
            btnObrisi.setVisibility(View.INVISIBLE);
        }

        FragmentTransition.to(ArtikliListaFragment.newInstance(), this, false);



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
