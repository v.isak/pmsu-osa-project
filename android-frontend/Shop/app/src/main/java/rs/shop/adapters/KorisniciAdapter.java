package rs.shop.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import model.dto.Korisnik;
import model.dto.Prodavac;
import rs.shop.R;
import rs.shop.activities.ProdavacActivity;
import rs.shop.rest.KorisnikCall;
import rs.shop.tools.Token;

/*
* Adapteri unutar Android-a sluze da prikazu unapred nedefinisanu kolicinu podataka
* pristigle sa interneta ili ucitane iz baze ili filesystem-a uredjaja.
* Da bi napravili adapter treba da napraivmo klasu, koja nasledjuje neki od postojecih adaptera.
* Za potrebe ovih vezbi koristicemo BaseAdapter koji je sposoban da kao izvor podataka iskoristi listu ili niz.
* Nasledjivanjem bilo kog adaptera, dobicemo
* nekolkko metoda koje moramo da referinisemo da bi adapter ispravno radio.
* */


public class KorisniciAdapter extends RecyclerView.Adapter<KorisniciAdapter.ViewHolder>{
    private Activity activity;
    List<Korisnik> korisnici;

    public KorisniciAdapter(List<Korisnik> korisnici, Activity activity) {

        // uklanjanje admin koji je ulogovan iz liste da ne bi mogao sebe da blokira
        for (Korisnik korisnik : korisnici){
            if (korisnik.getIdKorisnika() == Token.getId(Token.getToken(activity))){
                korisnici.remove(korisnik);
                break;
            }
        }
        this.korisnici = korisnici;
        this.activity = activity;
    }


    @NonNull
    @Override
    public KorisniciAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.korisnik_list, viewGroup, false);

        return new KorisniciAdapter.ViewHolder(v);
    }


    @Override
    public int getItemCount() {
        return korisnici.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView ime;
        private final TextView prezime;
        private final TextView korisnickoIme;
        private final TextView button;

        ViewHolder(View v){
            super(v);
            ime = (TextView)v.findViewById(R.id.imeKorisnika);
            prezime = (TextView)v.findViewById(R.id.prezimeKorisnika);
            korisnickoIme = (TextView)v.findViewById(R.id.korisnickoIme);
            button = (Button)v.findViewById(R.id.btnBlokiraj);
        }
    }

    // prikazivanje elementa u recyclerview-u
    @Override
    public void onBindViewHolder(@NonNull KorisniciAdapter.ViewHolder viewHolder, int position) {
        Korisnik korisnik = korisnici.get(position);

        viewHolder.ime.setText("Ime: " + korisnik.getIme());
        viewHolder.prezime.setText("Prezime: " + korisnik.getPrezime());
        viewHolder.korisnickoIme.setText(korisnik.getKorisnickoIme());
        viewHolder.button.setText(korisnik.isBlokiran() ? "ODBLOKIRAJ" : "BLOKIRAJ");

        viewHolder.button.setOnClickListener((view) -> {
            if (viewHolder.button.getText().toString().equals("BLOKIRAJ")){
                KorisnikCall.blokirajKorisnika(korisnik.getIdKorisnika(),activity);

                viewHolder.button.setText("ODBLOKIRAJ");
            }
            else if (viewHolder.button.getText().toString().equals("ODBLOKIRAJ")){
                KorisnikCall.odblokirajKorisnika(korisnik.getIdKorisnika(),activity);

                viewHolder.button.setText("BLOKIRAJ");
            }
        });

    }

}
