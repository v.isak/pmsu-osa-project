package rs.shop.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import model.dto.Artikal;
import model.dto.Slika;
import pub.devrel.easypermissions.EasyPermissions;
import rs.shop.R;
import rs.shop.rest.ArtikliCall;
import rs.shop.rest.SlikeCall;
import rs.shop.tools.Token;

public class ArtiklDetailActivity extends AppCompatActivity {
    SharedPreferences sharedpreferences;
    private String uloga = "";
    private String token = "";

    /// KAMERA
    private static final int CAMERA_REQUEST = 1888;
    private ImageView imageView;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private String slikaString = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artikl_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView naziv = findViewById(R.id.artiklNazivUnos);
        TextView opis = findViewById(R.id.artiklOpisUnos);
        TextView cena = findViewById(R.id.artiklCenaUnos);
        ImageView slikaView = (ImageView)findViewById(R.id.imageView2);



        // dugme za dodavanje i izmenu
        Button btnDodajIzmeni = (Button) findViewById(R.id.buttonDodajIzmeniArtikal);

        // dugme za brisanje
        Button btnObrisi = (Button) findViewById(R.id.buttonObrisiArtikal);

        ///////// KAMERA /////////
        Button photoButton = (Button) this.findViewById(R.id.btnSlika);


        // IZMENA ARTIKLA
        if (getIntent().getStringExtra("naziv") != null) {
            getSupportActionBar().setTitle("Izmena artikla");
            naziv.setText(getIntent().getStringExtra("naziv"));
            opis.setText(getIntent().getStringExtra("opis"));
            cena.setText(getIntent().getStringExtra("cena"));
            if (!getIntent().getStringExtra("putanjaSlike").equals("")) {
                Slika slika = new Slika();
                slika.setSlikaRelativePath(getIntent().getStringExtra("putanjaSlike"));
                SlikeCall.loadSlika(slika,slikaView,null,ArtiklDetailActivity.this);
            }

            btnDodajIzmeni.setText("IZMENI");
        }
        else{
            getSupportActionBar().setTitle("Dodavanje artikla");
            btnObrisi.setEnabled(false);
            btnObrisi.setVisibility(View.INVISIBLE);
        }

        token = Token.getToken(ArtiklDetailActivity.this);
        uloga = Token.getUloga(token);


        // PRODAVAC
        if (uloga.equals("ROLE_PRODAVAC")){

            photoButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    String[] galleryPermissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

                    if (EasyPermissions.hasPermissions(ArtiklDetailActivity.this, galleryPermissions)) {
                        pickImage();
                    } else {
                        EasyPermissions.requestPermissions(ArtiklDetailActivity.this, "Access for storage",
                                101, galleryPermissions);
                    }
                }
            });

            btnDodajIzmeni.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        TextView naziv = findViewById(R.id.artiklNazivUnos);
                        TextView opis = findViewById(R.id.artiklOpisUnos);
                        TextView cena = findViewById(R.id.artiklCenaUnos);
                        Artikal artikl = new Artikal();
                        artikl.setIdProdavca(Long.valueOf(getIntent().getStringExtra("idProdavca")));
                        if (!naziv.getText().toString().equals("") || !opis.getText().toString().equals("")){
                            artikl.setNaziv(naziv.getText().toString());
                            artikl.setOpis(opis.getText().toString());
                        }
                        else{
                            Toast.makeText(ArtiklDetailActivity.this, "Unesite sve podatke", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        artikl.setCena(Double.valueOf(cena.getText().toString()));

                        Button button1 = (Button) findViewById(R.id.buttonDodajIzmeniArtikal);
                        if (button1.getText().equals("DODAJ")){
                            Slika slika = new Slika();
                            if (slikaString != null) {
                                slika.setSlikaString(slikaString);
                            }
                            else{
                                slika.setSlikaString("");
                            }
                            ArtikliCall.createArtikal(artikl, slika,ArtiklDetailActivity.this);
                        }
                        else if(button1.getText().equals("IZMENI")){
                            Slika slika = new Slika();
                            if (slikaString != null) {
                                slika.setSlikaRelativePath(getIntent().getStringExtra("putanjaSlike"));
                                slika.setSlikaString(slikaString);
                            }
                            else{
                                slika.setSlikaRelativePath(getIntent().getStringExtra("putanjaSlike"));
                                slika.setSlikaString("");
                            }
                            artikl.setIdArtikla(Long.valueOf(getIntent().getStringExtra("idArtikla")));
                            artikl.setPutanjaSlike(getIntent().getStringExtra("putanjaSlike"));
                            ArtikliCall.updateArtikal(artikl, slika, ArtiklDetailActivity.this);
                        }
                        else{
                            Toast.makeText(ArtiklDetailActivity.this, "Button error", Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch(Exception e){
                        Toast.makeText(ArtiklDetailActivity.this, "Unesite validne podatke", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            btnObrisi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        ArtikliCall.deleteArtikal(Long.valueOf(getIntent().getStringExtra("idArtikla")),ArtiklDetailActivity.this);
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
        // KUPAC
        else if (uloga.equals("ROLE_KUPAC")){
            getSupportActionBar().setTitle("Pregled artikla");
            //naziv.setEnabled(false);
            naziv.setFocusable(false);
            //opis.setEnabled(false);
            opis.setFocusable(false);
            //cena.setEnabled(false);
            cena.setFocusable(false);
            btnDodajIzmeni.setEnabled(false);
            btnDodajIzmeni.setVisibility(View.INVISIBLE);
            btnObrisi.setEnabled(false);
            btnObrisi.setVisibility(View.INVISIBLE);
            photoButton.setEnabled(false);
            photoButton.setVisibility(View.INVISIBLE);
        }

    }


    /// KAMERA
    public void pickImage() {
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 2);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && resultCode == RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImageView slika = (ImageView)findViewById(R.id.imageView2);
            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
            slika.setImageBitmap(bitmap);

            ByteArrayOutputStream baos=new  ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
            byte [] b=baos.toByteArray();
            slikaString = Base64.encodeToString(b, Base64.DEFAULT);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
