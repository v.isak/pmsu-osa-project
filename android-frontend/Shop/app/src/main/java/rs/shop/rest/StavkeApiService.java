package rs.shop.rest;


import java.util.List;

import model.dto.Akcija;
import model.dto.Stavka;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface StavkeApiService {
    @GET("Stavke")
    Call<List<Stavka>> getStavke();

    @GET("Stavke/Porudzbina/{id}")
    Call<List<Stavka>> getStavkeZaPorudzbinu(@Path("id") Long idPorudzbine);

    @GET("Stavke/{id}")
    Call<Stavka> getStavka(@Path("id") Long id);

    @POST("Stavke")
    Call<Stavka> createStavka(@Body Stavka stavka);

    @PUT("Stavke/{id}")
    Call<Stavka> updateStavka(@Path("id") Long id, @Body Stavka stavka);

    @DELETE("Stavke/{id}")
    Call<ResponseBody> deleteStavka(@Path("id") Long id);
}
