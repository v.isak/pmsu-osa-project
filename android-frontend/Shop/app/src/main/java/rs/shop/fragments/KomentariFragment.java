package rs.shop.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import rs.shop.R;
import rs.shop.activities.KorisnikDetailActivity;
import rs.shop.activities.LoginActivity;
import rs.shop.rest.KorisnikCall;
import rs.shop.rest.PorudzbineCall;
import rs.shop.tools.FragmentTransition;
import rs.shop.tools.Token;


public class KomentariFragment extends Fragment {


    public static KomentariFragment newInstance() {
        return new KomentariFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle data) {
        setHasOptionsMenu(true);

        View v = inflater.inflate(R.layout.map_layout, container, false);
        RecyclerView recyclerView = (RecyclerView)v.findViewById(R.id.recyclerView);
        Long idProdavca = Long.valueOf(getActivity().getIntent().getStringExtra("idProdavca"));
        System.out.println(idProdavca);
        PorudzbineCall.getKomentariZaProdavca(getActivity(), recyclerView, idProdavca);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        // update-uje se recycler view odnosno podaci u njemu
        RecyclerView recyclerView = (RecyclerView)getView().findViewById(R.id.recyclerView);
        Long idProdavca = Long.valueOf(getActivity().getIntent().getStringExtra("idProdavca"));
        PorudzbineCall.getKomentariZaProdavca(getActivity(), recyclerView, idProdavca);

    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        // ovo korostimo ako je nasa arhitekrura takva da imamo jednu aktivnost
        // i vise fragmentaa gde svaki od njih ima svoj menu unutar toolbar-a
        menu.clear();
        inflater.inflate(R.menu.activity_itemdetail_prodavac, menu);
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("mypref",
                Context.MODE_PRIVATE);
        menu.findItem(R.id.action_new).setVisible(false);

        menu.findItem(R.id.action_korpa).setVisible(false);
        if (sharedpreferences.contains("token")) {
            String token = sharedpreferences.getString("token", "");
            if (Token.getUloga(token).equals("ROLE_PRODAVAC")) {
                menu.findItem(R.id.action_korpa).setVisible(false);
            } else if (Token.getUloga(token).equals("ROLE_KUPAC")) {
                menu.findItem(R.id.action_new).setVisible(false);
                //menu.findItem(R.id.action_akcije).setVisible(false);
                //menu.findItem(R.id.action_artikli).setVisible(false);
                //menu.findItem(R.id.action_komentari).setVisible(false);
            }
            else if (Token.getUloga(token).equals("ROLE_ADMINISTRATOR")) {
                menu.findItem(R.id.action_korpa).setVisible(false);
            }
        }
        else{
            menu.findItem(R.id.action_new).setVisible(false);
            menu.findItem(R.id.action_akcije).setVisible(false);
            menu.findItem(R.id.action_artikli).setVisible(false);
            menu.findItem(R.id.action_komentari).setVisible(false);
            menu.findItem(R.id.action_odjava).setVisible(false);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_akcije){
            FragmentTransition.to(AkcijeFragment.newInstance(), getActivity(), false);
        }

        else if (id == R.id.action_user){
            Intent intent = new Intent(getActivity(), KorisnikDetailActivity.class);
            if (Token.getUloga(Token.getToken(getActivity())).equals("ROLE_PRODAVAC")) {
                intent.putExtra("putanjaSlike", getActivity().getIntent().getStringExtra("putanjaSlike"));
            }
            startActivity(intent);
        }

        else if(id == R.id.action_artikli){
            FragmentTransition.to(ArtikliFragment.newInstance(), getActivity(), false);
        }

        else if(id == R.id.action_komentari){
            FragmentTransition.to(this, getActivity(), false);
        }

        else if(id == R.id.action_odjava){
            KorisnikCall.odjava(getActivity());
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }


}