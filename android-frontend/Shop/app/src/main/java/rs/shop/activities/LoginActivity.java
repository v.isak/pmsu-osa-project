package rs.shop.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import model.dto.Prijava;
import model.dto.Prodavac;
import rs.shop.R;
import rs.shop.rest.KorisnikCall;
import rs.shop.rest.PorudzbineCall;
import rs.shop.rest.ProdavciCall;
import rs.shop.tools.Token;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // dugme za prijavu
        Button button1 = (Button) findViewById(R.id.button);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView username = (TextView) findViewById(R.id.editTextUsername);
                TextView password = (TextView) findViewById(R.id.editTextPass);
                Prijava prijava = new Prijava(username.getText().toString(),
                        password.getText().toString());
                KorisnikCall.prijava(prijava, LoginActivity.this);
            }
        });

        // link za registraciju
        TextView textViewReg = (TextView) findViewById(R.id.textViewReg);
        textViewReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
            }
        });

        // link za gosta
        TextView textViewGost = (TextView) findViewById(R.id.textViewGost);
        textViewGost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, KupacActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences sharedpreferences = getSharedPreferences("mypref",
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains("token")) {
            String token = sharedpreferences.getString("token", "");
            if (Token.getUloga(token).equals("ROLE_KUPAC")) {
                Intent intent = new Intent(LoginActivity.this, KupacActivity.class);
                intent.putExtra("idKupca", String.valueOf(Token.getId(Token.getToken(this))));
                startActivity(intent);
            } else if (Token.getUloga(token).equals("ROLE_PRODAVAC")) {
                Intent intent = new Intent(LoginActivity.this, ProdavacActivity.class);
                intent.putExtra("idProdavca", String.valueOf(Token.getId(Token.getToken(this))));
                startActivity(intent);
            } else if (Token.getUloga(token).equals("ROLE_ADMINISTRATOR")) {
                Intent intent = new Intent(LoginActivity.this, AdminActivity.class);
                startActivity(intent);
            }
        }
        else{
            Intent intent = new Intent(LoginActivity.this, KupacActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}