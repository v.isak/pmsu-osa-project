package rs.shop.rest;


import java.util.List;

import model.dto.Artikal;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ArtikliApiService {
    @GET("Artikli")
    Call<List<Artikal>> getArtikli();

    @GET("Artikli/Akcija/Prodavac/{id}")
    Call<List<Artikal>> getArtikliNaAkcijiZaProdavca(@Path("id") Long idAkcije);

    @GET("Artikli/Prodavac/{id}")
    Call<List<Artikal>> getArtikliZaProdavca(@Path("id") Long idProdavca);

    @POST("Artikli/ArtikliIzStavki")
    Call<List<Artikal>> getArtikliIzStavki(@Body List<Long> artikliIzStavkiIds);

    @GET("Artikli/{id}")
    Call<Artikal> getArtikal(@Path("id") Long id);

    @POST("Artikli")
    Call<Artikal> createArtikal(@Body Artikal artikal);

    @PUT("Artikli/{id}")
    Call<Artikal> updateArtikal(@Path("id") Long id, @Body Artikal artikal);

    @DELETE("Artikli/{id}")
    Call<ResponseBody> deleteArtikal(@Path("id") Long id);
}
