package rs.shop.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import model.dto.Artikal;
import model.dto.Porudzbina;
import model.dto.Prodavac;
import model.dto.Stavka;
import rs.shop.R;
import rs.shop.activities.ArtiklDetailActivity;
import rs.shop.activities.KomentarActivity;
import rs.shop.activities.PorudzbinaDetailActivity;
import rs.shop.rest.ArtikliCall;
import rs.shop.rest.PorudzbineCall;
import rs.shop.rest.ProdavciCall;
import rs.shop.rest.StavkeCall;
import rs.shop.tools.Token;

/*
* Adapteri unutar Android-a sluze da prikazu unapred nedefinisanu kolicinu podataka
* pristigle sa interneta ili ucitane iz baze ili filesystem-a uredjaja.
* Da bi napravili adapter treba da napraivmo klasu, koja nasledjuje neki od postojecih adaptera.
* Za potrebe ovih vezbi koristicemo BaseAdapter koji je sposoban da kao izvor podataka iskoristi listu ili niz.
* Nasledjivanjem bilo kog adaptera, dobicemo
* nekolkko metoda koje moramo da referinisemo da bi adapter ispravno radio.
* */
public class PorudzbinaAdapter extends RecyclerView.Adapter<PorudzbinaAdapter.ViewHolder>{
    private Activity activity;
    List<Porudzbina> porudzbine;

    public PorudzbinaAdapter(List<Porudzbina> porudzbineZaKupca, Activity activity) {

        this.porudzbine = porudzbineZaKupca;
        this.activity = activity;
    }


    @NonNull
    @Override
    public PorudzbinaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.porudzbina_list, viewGroup, false);

        return new PorudzbinaAdapter.ViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return porudzbine.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView idPorudzbine;
        private final TextView satnica;

        private final TextView dostavljeno;
        private final TextView pristiglo;
        private final TextView ocenjenoNeocenjeno;

        ViewHolder(View v){
            super(v);
            idPorudzbine = (TextView)v.findViewById(R.id.idPorudzbine);
            satnica = (TextView)v.findViewById(R.id.satnica);

            dostavljeno = (TextView)v.findViewById(R.id.dostavljeno);
            pristiglo = (Button)v.findViewById(R.id.btnPristiglo);
            ocenjenoNeocenjeno = (TextView)v.findViewById(R.id.ocenjenoNeocenjeno);

        }
    }

    // prikazivanje elementa u recyclerview-u
    @Override
    public void onBindViewHolder(@NonNull PorudzbinaAdapter.ViewHolder viewHolder, int position) {
        Porudzbina porudzbina = porudzbine.get(position);

        viewHolder.idPorudzbine.setText("ID porudzbine: " + porudzbina.getIdPorudzbine().toString());
        viewHolder.satnica.setText("Kreirana: " + porudzbina.getSatnica().replace("T", " ").substring(0,16));

        if (porudzbina.isDostavljeno()){
            viewHolder.pristiglo.setEnabled(false);
            viewHolder.pristiglo.setVisibility(View.GONE);

            viewHolder.dostavljeno.setText("Dostavljeno");
        }
        else{
            viewHolder.dostavljeno.setEnabled(false);
            viewHolder.dostavljeno.setVisibility(View.GONE);

            viewHolder.pristiglo.setOnClickListener((view) -> {
                Porudzbina porudzbinaNull = new Porudzbina();
                porudzbinaNull.setDostavljeno(true);
                PorudzbineCall.updatePorudzbina(porudzbina.getIdPorudzbine(),porudzbinaNull,activity);

                porudzbina.setDostavljeno(true);
                // iskljucivanje buttona i ukljucivanje prikaza "Dostavljeno"
                viewHolder.pristiglo.setEnabled(false);
                viewHolder.pristiglo.setVisibility(View.GONE);

                viewHolder.dostavljeno.setText("Dostavljeno");
                viewHolder.dostavljeno.setEnabled(true);
                viewHolder.dostavljeno.setVisibility(View.VISIBLE);
            });
        }

        if (porudzbina.getOcena() != 0){
            viewHolder.ocenjenoNeocenjeno.setText("Ocenjeno");
        }
        else{
            viewHolder.ocenjenoNeocenjeno.setText("Neocenjeno");
        }

        // kad korisnik klikne na element iz liste?
        SharedPreferences sharedpreferences = activity.getSharedPreferences("mypref",
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains("token")) {
            viewHolder.idPorudzbine.setOnClickListener((view) -> {
                Context context = view.getContext();
                Intent intent = new Intent(activity, PorudzbinaDetailActivity.class);
                intent.putExtra("idPorudzbine", String.valueOf(porudzbina.getIdPorudzbine()));
                intent.putExtra("ocenjenaPorudzbina", String.valueOf(porudzbina.getOcena() == 0 ? false : true));
                intent.putExtra("dostavljenaPorudzbina", String.valueOf(porudzbina.isDostavljeno()));
                intent.putExtra("vremePorudzbine", porudzbina.getSatnica());
                intent.putExtra("cenaPorudzbine", String.valueOf(porudzbina.getCenaPorudzbine()));
                context.startActivity(intent);
                //activity.finish();
            });
        }

    }

}
