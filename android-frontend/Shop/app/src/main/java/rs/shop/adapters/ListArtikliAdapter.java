package rs.shop.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import model.dto.Artikal;
import model.dto.Slika;
import rs.shop.R;
import rs.shop.activities.ArtiklDetailActivity;
import rs.shop.rest.SlikeCall;
import rs.shop.tools.Token;

/*
* Adapteri unutar Android-a sluze da prikazu unapred nedefinisanu kolicinu podataka
* pristigle sa interneta ili ucitane iz baze ili filesystem-a uredjaja.
* Da bi napravili adapter treba da napraivmo klasu, koja nasledjuje neki od postojecih adaptera.
* Za potrebe ovih vezbi koristicemo BaseAdapter koji je sposoban da kao izvor podataka iskoristi listu ili niz.
* Nasledjivanjem bilo kog adaptera, dobicemo
* nekolkko metoda koje moramo da referinisemo da bi adapter ispravno radio.
* */
public class ListArtikliAdapter extends RecyclerView.Adapter<ListArtikliAdapter.ViewHolder>{
    private List<Artikal> artikli;
    private Activity activity;

    public ListArtikliAdapter(List<Artikal> artikliZaAkcije, Activity activity) {

        this.artikli = artikliZaAkcije;
        this.activity = activity;

    }

    @NonNull
    @Override
    public ListArtikliAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.artikal_list_check, viewGroup, false);

        return new ListArtikliAdapter.ViewHolder(v);
    }


    @Override
    public int getItemCount() {
        return artikli.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView naziv;
        private final TextView opis;
        private final TextView cena;
        private final CheckBox selektovanArtikal;
        private final ImageView slika;

        ViewHolder(View v){
            super(v);
            naziv = (TextView)v.findViewById(R.id.nazivArtikalList);
            opis = (TextView)v.findViewById(R.id.opisArtikalList);
            cena = (TextView)v.findViewById(R.id.cenaArtikalList);
            selektovanArtikal = (CheckBox)v.findViewById(R.id.cbArtikalList);
            slika = (ImageView)v.findViewById(R.id.slikaArtikalList);

        }
    }

    // prikazivanje elementa u recyclerview-u
    @Override
    public void onBindViewHolder(@NonNull ListArtikliAdapter.ViewHolder viewHolder, int position) {
        Artikal artikal = artikli.get(position);
        viewHolder.naziv.setText(artikal.getNaziv());
        viewHolder.opis.setText(artikal.getOpis());
        viewHolder.cena.setText(String.valueOf(artikal.getCena()) + " RSD");

        ArrayList<String> artikli = activity.getIntent().getStringArrayListExtra("artikli");
        for (String idArtikla : artikli){
            if (Long.valueOf(idArtikla) == artikal.getIdArtikla()){
                viewHolder.selektovanArtikal.setChecked(true);
                break;
            }
        }
        //
        // postavljanje slike ako postoji
        if (!artikal.getPutanjaSlike().equals("")) {
            Slika slika = new Slika();
            slika.setSlikaRelativePath(artikal.getPutanjaSlike());

            SlikeCall.loadSlika(slika, viewHolder.slika, null, activity);
        }

        // kad korisnik klikne na element iz liste?
        SharedPreferences sharedpreferences = activity.getSharedPreferences("mypref",
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains("token")) {
            viewHolder.naziv.setOnClickListener((view) -> {
                Context context = view.getContext();
                Intent intent = new Intent(activity, ArtiklDetailActivity.class);
                intent.putExtra("idArtikla", String.valueOf(artikal.getIdArtikla()));
                intent.putExtra("idProdavca", String.valueOf(artikal.getIdProdavca()));
                intent.putExtra("naziv", artikal.getNaziv());
                intent.putExtra("opis", artikal.getOpis());
                intent.putExtra("cena", String.valueOf(artikal.getCena()));
                intent.putExtra("putanjaSlike", artikal.getPutanjaSlike());
                context.startActivity(intent);
                activity.finish();
            });

            if (Token.getUloga(Token.getToken(activity)).equals("ROLE_PRODAVAC")) {
                viewHolder.selektovanArtikal.setOnClickListener((view) -> {
                    // ako je korisnik selektovao artikal
                    if (viewHolder.selektovanArtikal.isChecked()) {
                        artikli.add(String.valueOf(artikal.getIdArtikla()));
                    }
                    // ako je korisnik deselktovao artikal
                    else {
                        for (String idArtikla : artikli) {
                            if (Long.valueOf(idArtikla) == artikal.getIdArtikla()) {
                                artikli.remove(idArtikla);
                                break;
                            }
                        }
                    }
                    activity.getIntent().putStringArrayListExtra("artikli", artikli);
                    System.out.println(activity.getIntent().getStringArrayListExtra("artikli"));
                });
            }
            else if (Token.getUloga(Token.getToken(activity)).equals("ROLE_KUPAC")){
                viewHolder.selektovanArtikal.setEnabled(false);
            }
        }

    }
}
