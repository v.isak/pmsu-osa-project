package rs.shop.rest;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import model.dto.Administrator;
import model.dto.Korisnik;
import model.dto.Artikal;
import model.dto.Kupac;
import model.dto.Prijava;
import model.dto.Prodavac;
import model.dto.Registracija;
import model.dto.Slika;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rs.shop.R;
import rs.shop.activities.LoginActivity;
import rs.shop.adapters.ArtikliAdapter;
import rs.shop.adapters.KorisniciAdapter;
import rs.shop.tools.Token;

public class KorisnikCall {

    static final String TAG = KorisnikCall.class.getSimpleName();
    static final String BASE_URL = Token.BASE_URL;
    static Retrofit retrofit = null;
    SharedPreferences sharedpreferences;

    public static void getKorisnici(Activity activity, RecyclerView recyclerView){
        retrofit = new Retrofit.Builder()
                .client(rs.shop.tools.Token.getTokenAuthHeader(rs.shop.tools.Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        KorisnikApiService korisnikApiService = retrofit.create(KorisnikApiService.class);
        Call<List<Korisnik>> call = korisnikApiService.getKorisnici();

        call.enqueue(new Callback<List<Korisnik>>() {
            @Override
            public void onResponse(Call<List<Korisnik>> call, Response<List<Korisnik>> response) {
                System.out.println(response.body());
                if (response.body()!= null) {
                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    recyclerView.setAdapter(new KorisniciAdapter(response.body(),activity));
                }
            }
            @Override
            public void onFailure(Call<List<Korisnik>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });


    }

    public static void blokirajKorisnika(Long idKorisnika, final Activity activity) {
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        KorisnikApiService korisnikApiService = retrofit.create(KorisnikApiService.class);
        Call<Void> call = korisnikApiService.blokirajKorisnika(idKorisnika);

        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                System.out.println(response.body());
                if (response.code() == 200) {
                }
                else{
                    Toast.makeText(activity, "Neuspesna odjava", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                Toast.makeText(activity, "Greska prilikom slanja zahteva", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void odblokirajKorisnika(Long idKorisnika, final Activity activity) {
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        KorisnikApiService korisnikApiService = retrofit.create(KorisnikApiService.class);
        Call<Void> call = korisnikApiService.odblokirajKorisnika(idKorisnika);

        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                System.out.println(response.body());
                if (response.code() == 200) {
                }
                else{
                    Toast.makeText(activity, "Neuspesna odjava", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                Toast.makeText(activity, "Greska prilikom slanja zahteva", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void registracija(Registracija registracija, final Activity activity){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        KorisnikApiService korisnikApiService = retrofit.create(KorisnikApiService.class);
        Call<Void> call = korisnikApiService.registration(registracija);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 201) {
                    /*
                    SharedPreferences sharedpreferences = activity.getSharedPreferences(mypreference,
                            Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(Token, response.body());
                    editor.commit();*/
                    activity.finish();
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }


    public static void prijava(Prijava prijava, final Activity activity) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        KorisnikApiService korisnikApiService = retrofit.create(KorisnikApiService.class);
        Call<String> call = korisnikApiService.login(prijava);
        final String[] token = {""};
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                System.out.println(response.body());
                if (response.isSuccessful()) {
                    SharedPreferences sharedpreferences = activity.getSharedPreferences("mypref",
                            Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("token", response.body().toString());
                    editor.commit();
                    activity.finish();
                }
                else{
                    Toast.makeText(activity, "Neuspesna prijava", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable throwable) {
                Toast.makeText(activity, throwable.toString(), Toast.LENGTH_SHORT).show();
                Toast.makeText(activity, "Greska prilikom slanja podataka", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public static void izmenaKorisnika(Prijava prijava, String slikaString, final Activity activity) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        KorisnikApiService korisnikApiService = retrofit.create(KorisnikApiService.class);
        Call<String> call = korisnikApiService.login(prijava);
        final String[] token = {""};
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                System.out.println(response.body());
                if (response.isSuccessful()) {
                    TextView ime = (TextView) activity.findViewById(R.id.unosRegIme);
                    TextView prezime = (TextView) activity.findViewById(R.id.unosRegPrezime);
                    TextView username = (TextView) activity.findViewById(R.id.unosRegUsername);
                    TextView adresa = (TextView) activity.findViewById(R.id.unosRegAdresa);
                    TextView email = (TextView) activity.findViewById(R.id.unosRegEmail);
                    TextView naziv = (TextView) activity.findViewById(R.id.unosRegNaziv);
                    TextView newPassword = (TextView) activity.findViewById(R.id.unosRegNewPassword);

                    Long idKorisnika = Long.valueOf(Token.getId(Token.getToken(activity)));
                    String uloga = Token.getUloga(Token.getToken(activity));
                    if (uloga.equals("ROLE_KUPAC")){
                        Kupac kupac = new Kupac();
                        kupac.setIdKorisnika(idKorisnika);
                        kupac.setImeKorisnika(ime.getText().toString());
                        kupac.setPrezimeKorisnika(prezime.getText().toString());
                        kupac.setKorisnickoIme(username.getText().toString());
                        kupac.setAdresa(adresa.getText().toString());
                        if (newPassword.getText().toString().length() != 0) {
                            kupac.setLozinkaKorisnika(newPassword.getText().toString());
                        }

                        KupciCall.updateKupac(kupac,activity);
                    }
                    else if (uloga.equals("ROLE_PRODAVAC")) {
                        Prodavac prodavac = new Prodavac();
                        prodavac.setIdKorisnika(idKorisnika);
                        prodavac.setImeKorisnika(ime.getText().toString());
                        prodavac.setPrezimeKorisnika(prezime.getText().toString());
                        prodavac.setKorisnickoIme(username.getText().toString());
                        prodavac.setAdresa(adresa.getText().toString());
                        prodavac.setEmail(email.getText().toString());
                        prodavac.setNaziv(naziv.getText().toString());
                        System.out.println("NEW PASSWORD: "+ newPassword.getText().toString().length());
                        if (newPassword.getText().toString().length() != 0) {
                            prodavac.setLozinkaKorisnika(newPassword.getText().toString());
                        }

                        Slika slika = new Slika();
                        if (slikaString != null) {
                            slika.setSlikaRelativePath(activity.getIntent().getStringExtra("putanjaSlike"));
                            slika.setSlikaString(slikaString);
                        }
                        else{
                            slika.setSlikaRelativePath(activity.getIntent().getStringExtra("putanjaSlike"));
                            slika.setSlikaString("");
                        }
                        prodavac.setPutanjaSlike(activity.getIntent().getStringExtra("putanjaSlike"));
                        ProdavciCall.updateProdavac(prodavac,slika,activity);
                    }
                    else if (uloga.equals("ROLE_ADMINISTRATOR")){
                        Administrator admin = new Administrator();
                        admin.setIdKorisnika(idKorisnika);
                        admin.setImeKorisnika(ime.getText().toString());
                        admin.setPrezimeKorisnika(prezime.getText().toString());
                        admin.setKorisnickoIme(username.getText().toString());
                        if (newPassword.getText().toString().length() != 0) {
                            admin.setLozinkaKorisnika(newPassword.getText().toString());
                        }

                        AdminiCall.updateAdmin(admin,activity);
                    }
                    activity.finish();
                }
                else{
                    Toast.makeText(activity, "Lozinka koju ste uneli nije ispravna", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable throwable) {
                Toast.makeText(activity, throwable.toString(), Toast.LENGTH_SHORT).show();
                Toast.makeText(activity, "Greska prilikom slanja podataka", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public static void odjava(final Activity activity) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        KorisnikApiService korisnikApiService = retrofit.create(KorisnikApiService.class);
        Call<Void> call = korisnikApiService.logout();

        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                System.out.println(response.body());
                if (response.code() == 200) {
                    SharedPreferences sharedpreferences = activity.getSharedPreferences("mypref",
                            Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.remove("token");
                    editor.commit();
                    activity.finishAffinity();
                }
                else{
                    Toast.makeText(activity, "Neuspesna odjava", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                Toast.makeText(activity, "Greska prilikom slanja zahteva", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
