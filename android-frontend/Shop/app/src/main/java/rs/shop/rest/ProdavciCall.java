package rs.shop.rest;


import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import model.dto.Artikal;
import model.dto.Prodavac;
import model.dto.Slika;
import model.dto.Stavka;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rs.shop.R;
import rs.shop.activities.ProdavacActivity;
import rs.shop.adapters.ArtikliAdapter;
import rs.shop.adapters.ListArtikliPorudzbineAdapter;
import rs.shop.adapters.PorudzbinaAdapter;
import rs.shop.adapters.ProdavciAdapter;
import rs.shop.fragments.ArtikliFragment;
import rs.shop.tools.FragmentTransition;
import rs.shop.tools.Token;

public class ProdavciCall {

    static final String TAG = ProdavciCall.class.getSimpleName();
    static List<Prodavac> prodavci = new ArrayList<Prodavac>();
    static Prodavac prodavac;
    static final String BASE_URL = Token.BASE_URL;
    static Retrofit retrofit = null;

    public static List<Prodavac> getProdavci(Activity activity, RecyclerView recyclerView){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ProdavciApiService prodavciApiService = retrofit.create(ProdavciApiService.class);
        Call<List<Prodavac>> call = prodavciApiService.getProdavci();

        call.enqueue(new Callback<List<Prodavac>>() {
            @Override
            public void onResponse(Call<List<Prodavac>> call, Response<List<Prodavac>> response) {
                System.out.println(response.body());
                prodavci.clear();
                if (response.body()!= null) {
                    prodavci.addAll(response.body());
                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    recyclerView.setAdapter(new ProdavciAdapter(prodavci,activity));

                }
                System.out.println(prodavci);
            }
            @Override
            public void onFailure(Call<List<Prodavac>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return prodavci;

    }

    public static void setProdavacNaziv(Activity activity, Long idPorudzbine, TextView nazivProdavca){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ProdavciApiService prodavciApiService = retrofit.create(ProdavciApiService.class);
        Call<Prodavac> call = prodavciApiService.getProdavacZaPorudzbinu(idPorudzbine);

        call.enqueue(new Callback<Prodavac>() {
            @Override
            public void onResponse(Call<Prodavac> call, Response<Prodavac> response) {
                System.out.println(response.body());
                if (response.body()!= null) {
                    nazivProdavca.setText("Prodavac: "+response.body().getNaziv());
                }
            }
            @Override
            public void onFailure(Call<Prodavac> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }

    public static Prodavac getProdavac(Long idProdavca, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ProdavciApiService prodavciApiService = retrofit.create(ProdavciApiService.class);
        Call<Prodavac> call = prodavciApiService.getProdavac(idProdavca);

        call.enqueue(new Callback<Prodavac>() {
            @Override
            public void onResponse(Call<Prodavac> call, Response<Prodavac> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    prodavac = response.body();

                    TextView ime = (TextView) activity.findViewById(R.id.unosRegIme);
                    TextView prezime = (TextView) activity.findViewById(R.id.unosRegPrezime);
                    TextView username = (TextView) activity.findViewById(R.id.unosRegUsername);
                    TextView adresa = (TextView) activity.findViewById(R.id.unosRegAdresa);
                    TextView email = (TextView) activity.findViewById(R.id.unosRegEmail);
                    TextView naziv = (TextView) activity.findViewById(R.id.unosRegNaziv);

                    ime.setText(prodavac.getImeKorisnika());
                    prezime.setText(prodavac.getPrezimeKorisnika());
                    username.setText(prodavac.getKorisnickoIme());
                    adresa.setText(prodavac.getAdresa());
                    email.setText(prodavac.getEmail());
                    naziv.setText(prodavac.getNaziv());
                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Prodavac> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return prodavac;

    }

    public static void setPodaciZaProdavca(Long idProdavca, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ProdavciApiService prodavciApiService = retrofit.create(ProdavciApiService.class);
        Call<Prodavac> call = prodavciApiService.getProdavac(idProdavca);

        call.enqueue(new Callback<Prodavac>() {
            @Override
            public void onResponse(Call<Prodavac> call, Response<Prodavac> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    prodavac = response.body();

                    // postavljanje slike ako postoji
                    if (!prodavac.getPutanjaSlike().equals("")) {
                        // postavljanje content view na onaj koji odgovara za sliku
                        activity.setContentView(R.layout.activity_prodavac_sa_slikom);
                        Toolbar toolbar = activity.findViewById(R.id.toolbar);
                        ((AppCompatActivity)activity).setSupportActionBar(toolbar);
                        // postavljanje putanje da bi se mogla koristiti KorisnikDetailActivity
                        activity.getIntent().putExtra("putanjaSlike",prodavac.getPutanjaSlike());
                        // instanciranje appBarLayout-a da bi se u njega ucitala slika
                        Slika slika = new Slika();
                        slika.setSlikaRelativePath(prodavac.getPutanjaSlike());
                        ConstraintLayout backgroundLayout = activity.findViewById(R.id.backgroundLayout);
                        SlikeCall.loadSlika(slika, null, backgroundLayout, activity);
                    }
                    else {
                        activity.setContentView(R.layout.activity_prodavac);
                        Toolbar toolbar = activity.findViewById(R.id.toolbar);
                        ((AppCompatActivity)activity).setSupportActionBar(toolbar);
                    }

                    RatingBar ratingBar = activity.findViewById(R.id.ratingBar);
                    ratingBar.setEnabled(false);
                    if (activity.getIntent().getStringExtra("prosecnaCena") != null) {
                        float prosecnaOcena = Float.valueOf(activity.getIntent().getStringExtra("prosecnaOcena"));
                        ratingBar.setRating(prosecnaOcena);

                        //getSupportActionBar().setTitle(getSupportActionBar().getTitle() + "\n" + "Prosecna ocena: " + getIntent().getStringExtra("prosecnaOcena"));
                    }
                    else {
                        PorudzbineCall.setOcenaZaProdavca(activity, idProdavca, null, ratingBar);
                    }
                    ((AppCompatActivity) activity).getSupportActionBar().setTitle(response.body().getNaziv());

                    activity.invalidateOptionsMenu();

                    FragmentTransition.to(ArtikliFragment.newInstance(), (FragmentActivity) activity, true);

                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Prodavac> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }

    public static Prodavac getProdavacByUsername(String username, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ProdavciApiService prodavciApiService = retrofit.create(ProdavciApiService.class);
        Call<Prodavac> call = prodavciApiService.getProdavacByUsername(username);

        call.enqueue(new Callback<Prodavac>() {
            @Override
            public void onResponse(Call<Prodavac> call, Response<Prodavac> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.isSuccessful()){
                    System.out.println("GetProdavacByUsername: SUCCESSFUL");
                    activity.finish();
                    prodavac = response.body();
                }
                else{
                    System.out.println("GetProdavacByUsername: UNSUCCESSFUL");
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<Prodavac> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return prodavac;

    }

    public static Prodavac createProdavac(Prodavac prodavacInfo, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ProdavciApiService prodavciApiService = retrofit.create(ProdavciApiService.class);
        Call<Prodavac> call = prodavciApiService.createProdavac(prodavacInfo);

        call.enqueue(new Callback<Prodavac>() {
            @Override
            public void onResponse(Call<Prodavac> call, Response<Prodavac> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 201){
                    activity.finish();
                    prodavac = response.body();
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Prodavac> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return prodavac;

    }

    public static void updateProdavac(Prodavac prodavacInfo, Slika slikaValue, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        SlikeApiService SlikeApiService = retrofit.create(SlikeApiService.class);
        Call<Slika> slikaCall = SlikeApiService.updateSlika(slikaValue);

        slikaCall.enqueue(new Callback<Slika>() {
            @Override
            public void onResponse(Call<Slika> call, Response<Slika> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){

                    prodavacInfo.setPutanjaSlike(response.body().getSlikaRelativePath());

                    ProdavciApiService prodavciApiService = retrofit.create(ProdavciApiService.class);
                    Call<Prodavac> prodavacCall = prodavciApiService.updateProdavac(prodavacInfo.getIdKorisnika(),prodavacInfo);

                    prodavacCall.enqueue(new Callback<Prodavac>() {
                        @Override
                        public void onResponse(Call<Prodavac> call, Response<Prodavac> response) {
                            System.out.println(response.body());
                            System.out.println(response.code());
                            if (response.code() == 200){
                                // unistavanje svih aktivnosti nakon izmene prodavca
                                // da bi se slika opet ucitala kako treba zajedno sa
                                // ostalim podacima
                                Intent intent = new Intent(activity, ProdavacActivity.class);
                                intent.putExtra("idProdavca",String.valueOf(prodavacInfo.getIdKorisnika()));
                                activity.startActivity(intent);
                                activity.finishAffinity();
                                prodavac = response.body();
                            }
                            else{
                                Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<Prodavac> call, Throwable throwable) {
                            Log.e(TAG, throwable.toString());
                        }
                    });
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Slika> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });
    }

    public static Prodavac deleteProdavac(Long idProdavca, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ProdavciApiService prodavciApiService = retrofit.create(ProdavciApiService.class);
        Call<ResponseBody> call = prodavciApiService.deleteProdavac(idProdavca);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    activity.finish();
                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return prodavac;

    }

}
