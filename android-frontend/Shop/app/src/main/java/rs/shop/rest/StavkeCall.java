package rs.shop.rest;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import model.dto.Akcija;
import model.dto.Artikal;
import model.dto.Stavka;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rs.shop.adapters.ListArtikliAdapter;
import rs.shop.adapters.ListArtikliKorpaAdapter;
import rs.shop.adapters.ListArtikliPorudzbineAdapter;
import rs.shop.tools.Token;

public class StavkeCall {

    static final String TAG = StavkeCall.class.getSimpleName();
    static List<Stavka> stavke = new ArrayList<Stavka>();
    static List<Artikal> artikli = new ArrayList<Artikal>();
    static Stavka Stavka;
    static final String BASE_URL = Token.BASE_URL;
    static Retrofit retrofit = null;

    // dobijanje artikala stavki za porudzbinu a ne samih stavki
    public static List<Artikal> getStavkeZaPorudzbinu(Activity activity, Long idPorudzbine, RecyclerView recyclerView){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        StavkeApiService StavkeApiService = retrofit.create(StavkeApiService.class);
        Call<List<Stavka>> call = StavkeApiService.getStavkeZaPorudzbinu(idPorudzbine);

        call.enqueue(new Callback<List<Stavka>>() {
            @Override
            public void onResponse(Call<List<Stavka>> call, Response<List<Stavka>> response) {
                System.out.println(response.body());
                stavke.clear();
                if (response.body()!= null) {
                    stavke.addAll(response.body());

                    List<Long> artikliIzStavki = new ArrayList<Long>();
                    for (Stavka stavka: stavke){
                        artikliIzStavki.add(stavka.getIdArtikla());
                    }

                    ArtikliApiService artikliApiService = retrofit.create(ArtikliApiService.class);
                    Call<List<Artikal>> callArtikli = artikliApiService.getArtikliIzStavki(artikliIzStavki);

                    callArtikli.enqueue(new Callback<List<Artikal>>() {
                        @Override
                        public void onResponse(Call<List<Artikal>> call, Response<List<Artikal>> response) {
                            System.out.println(response.body());
                            artikli.clear();
                            if (response.isSuccessful()) {
                                artikli.addAll(response.body());
                                recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                                recyclerView.setAdapter(new ListArtikliPorudzbineAdapter(stavke,artikli,activity));

                            }
                            System.out.println(artikli);
                        }
                        @Override
                        public void onFailure(Call<List<Artikal>> call, Throwable throwable) {
                            Log.e(TAG, throwable.toString());
                        }
                    });
                        }
                    }
            @Override
            public void onFailure(Call<List<Stavka>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return null;

    }


    public static Stavka createStavka(Stavka StavkaInfo, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        StavkeApiService StavkeApiService = retrofit.create(StavkeApiService.class);
        Call<Stavka> call = StavkeApiService.createStavka(StavkaInfo);

        call.enqueue(new Callback<Stavka>() {
            @Override
            public void onResponse(Call<Stavka> call, Response<Stavka> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 201){
                    activity.finish();
                    Stavka = response.body();
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Stavka> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return Stavka;

    }

    public static Stavka updateStavka(Stavka StavkaInfo, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        StavkeApiService StavkeApiService = retrofit.create(StavkeApiService.class);
        Call<Stavka> call = StavkeApiService.updateStavka(StavkaInfo.getIdArtikla(),StavkaInfo);

        call.enqueue(new Callback<Stavka>() {
            @Override
            public void onResponse(Call<Stavka> call, Response<Stavka> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    activity.finish();
                    Stavka = response.body();
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Stavka> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return Stavka;

    }

    public static Stavka deleteStavka(Long idStavke, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        StavkeApiService StavkeApiService = retrofit.create(StavkeApiService.class);
        Call<ResponseBody> call = StavkeApiService.deleteStavka(idStavke);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    activity.finish();
                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return Stavka;

    }

}
