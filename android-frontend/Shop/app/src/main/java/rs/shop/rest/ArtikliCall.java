package rs.shop.rest;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import model.dto.Administrator;
import model.dto.Artikal;
import model.dto.Slika;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rs.shop.adapters.ArtikliAdapter;
import rs.shop.adapters.ListArtikliAdapter;
import rs.shop.adapters.ListArtikliKorpaAdapter;
import rs.shop.tools.Token;

public class ArtikliCall {

    static final String TAG = ArtikliCall.class.getSimpleName();
    static List<Artikal> artikli = new ArrayList<Artikal>();
    static Artikal artikal;
    static final String BASE_URL = Token.BASE_URL;
    static Retrofit retrofit = null;

    public static List<Artikal> getArtikli(Activity activity, RecyclerView recyclerView){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ArtikliApiService artikliApiService = retrofit.create(ArtikliApiService.class);
        Call<List<Artikal>> call = artikliApiService.getArtikli();

        call.enqueue(new Callback<List<Artikal>>() {
            @Override
            public void onResponse(Call<List<Artikal>> call, Response<List<Artikal>> response) {
                System.out.println(response.body());
                artikli.clear();
                if (response.body()!= null) {
                    artikli.addAll(response.body());
                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    recyclerView.setAdapter(new ArtikliAdapter(artikli,activity));

                }
                System.out.println(artikli);
            }
            @Override
            public void onFailure(Call<List<Artikal>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return artikli;

    }

    public static void getArtikliZaProdavca(Long idProdavca, Activity activity, RecyclerView recyclerView){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ArtikliApiService artikliApiService = retrofit.create(ArtikliApiService.class);
        Call<List<Artikal>> call = artikliApiService.getArtikliZaProdavca(idProdavca);

        call.enqueue(new Callback<List<Artikal>>() {
            @Override
            public void onResponse(Call<List<Artikal>> call, Response<List<Artikal>> response) {
                System.out.println(response.body());
                artikli.clear();
                if (response.body()!= null) {
                    artikli.addAll(response.body());
                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    recyclerView.setAdapter(new ArtikliAdapter(artikli,activity));

                }
                System.out.println(artikli);
            }
            @Override
            public void onFailure(Call<List<Artikal>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }


    public static List<Artikal> getArtikliZaKorpu(Long idProdavca, Activity activity, RecyclerView recyclerView){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ArtikliApiService artikliApiService = retrofit.create(ArtikliApiService.class);
        Call<List<Artikal>> call = artikliApiService.getArtikliZaProdavca(idProdavca);

        call.enqueue(new Callback<List<Artikal>>() {
            @Override
            public void onResponse(Call<List<Artikal>> call, Response<List<Artikal>> response) {
                System.out.println(response.body());
                artikli.clear();
                if (response.body()!= null) {
                    artikli.addAll(response.body());
                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    recyclerView.setAdapter(new ListArtikliKorpaAdapter(artikli,activity));

                }
                System.out.println(artikli);
            }
            @Override
            public void onFailure(Call<List<Artikal>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return artikli;

    }

    public static void getArtikliZaAkciju(Long idAkcije, Long idProdavca, Activity activity, RecyclerView recyclerView){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        String uloga = Token.getUloga(Token.getToken(activity));
        if (uloga.equals("ROLE_KUPAC")) {
            ArtikliApiService artikliApiService = retrofit.create(ArtikliApiService.class);
            Call<List<Artikal>> call = artikliApiService.getArtikliNaAkcijiZaProdavca(idAkcije);

            call.enqueue(new Callback<List<Artikal>>() {
                @Override
                public void onResponse(Call<List<Artikal>> call, Response<List<Artikal>> response) {
                    System.out.println(response.body());
                    artikli.clear();
                    if (response.body() != null) {
                        artikli.addAll(response.body());
                        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                        recyclerView.setAdapter(new ListArtikliAdapter(artikli, activity));

                    }
                    System.out.println(artikli);
                }

                @Override
                public void onFailure(Call<List<Artikal>> call, Throwable throwable) {
                    Log.e(TAG, throwable.toString());
                }
            });
        }

        else if (uloga.equals("ROLE_PRODAVAC")){
            ArtikliApiService artikliApiService = retrofit.create(ArtikliApiService.class);
            Call<List<Artikal>> call = artikliApiService.getArtikliZaProdavca(idProdavca);

            call.enqueue(new Callback<List<Artikal>>() {
                @Override
                public void onResponse(Call<List<Artikal>> call, Response<List<Artikal>> response) {
                    System.out.println(response.body());
                    artikli.clear();
                    if (response.body() != null) {
                        artikli.addAll(response.body());
                        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                        recyclerView.setAdapter(new ListArtikliAdapter(artikli, activity));

                    }
                    System.out.println(artikli);
                }

                @Override
                public void onFailure(Call<List<Artikal>> call, Throwable throwable) {
                    Log.e(TAG, throwable.toString());
                }
            });
        }

    }


    public static Artikal getArtikal(Long idArtikla, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ArtikliApiService ArtikliApiService = retrofit.create(ArtikliApiService.class);
        Call<Artikal> call = ArtikliApiService.getArtikal(idArtikla);

        call.enqueue(new Callback<Artikal>() {
            @Override
            public void onResponse(Call<Artikal> call, Response<Artikal> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    activity.finish();
                    artikal = response.body();
                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Artikal> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return artikal;

    }

    public static Artikal createArtikal(Artikal artikalInfo, Slika slikaValue, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        SlikeApiService SlikeApiService = retrofit.create(SlikeApiService.class);
        Call<Slika> slikaCall = SlikeApiService.createSlika(slikaValue);

        slikaCall.enqueue(new Callback<Slika>() {
            @Override
            public void onResponse(Call<Slika> call, Response<Slika> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 201){
                    System.out.println(response.body().getSlikaRelativePath());
                    artikalInfo.setPutanjaSlike(response.body().getSlikaRelativePath());

                    ArtikliApiService artikliApiService = retrofit.create(ArtikliApiService.class);
                    Call<Artikal> artikalCall = artikliApiService.createArtikal(artikalInfo);

                    artikalCall.enqueue(new Callback<Artikal>() {
                        @Override
                        public void onResponse(Call<Artikal> call, Response<Artikal> response) {
                            System.out.println(response.body());
                            System.out.println(response.code());
                            if (response.code() == 201){
                                activity.finish();
                                artikal = response.body();
                            }
                            else{
                                Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<Artikal> call, Throwable throwable) {
                            Log.e(TAG, throwable.toString());
                        }
                    });
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Slika> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });



        return artikal;

    }

    public static Artikal updateArtikal(Artikal artikalInfo, Slika slikaValue, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        SlikeApiService SlikeApiService = retrofit.create(SlikeApiService.class);
        Call<Slika> slikaCall = SlikeApiService.updateSlika(slikaValue);

        slikaCall.enqueue(new Callback<Slika>() {
            @Override
            public void onResponse(Call<Slika> call, Response<Slika> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    artikalInfo.setPutanjaSlike(response.body().getSlikaRelativePath());

                    ArtikliApiService artikliApiService = retrofit.create(ArtikliApiService.class);
                    Call<Artikal> artikalCall = artikliApiService.updateArtikal(artikalInfo.getIdArtikla(),artikalInfo);

                    artikalCall.enqueue(new Callback<Artikal>() {
                        @Override
                        public void onResponse(Call<Artikal> call, Response<Artikal> response) {
                            System.out.println(response.body());
                            System.out.println(response.code());
                            if (response.code() == 200){
                                activity.finish();
                                artikal = response.body();
                            }
                            else{
                                Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<Artikal> call, Throwable throwable) {
                            Log.e(TAG, throwable.toString());
                        }
                    });
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Slika> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return artikal;

    }

    public static Artikal deleteArtikal(Long idArtikla, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ArtikliApiService artikliApiService = retrofit.create(ArtikliApiService.class);
        Call<ResponseBody> call = artikliApiService.deleteArtikal(idArtikla);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    activity.finish();
                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return artikal;

    }

}
