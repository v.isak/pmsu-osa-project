package rs.shop.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.database.Observable;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import model.dto.Artikal;
import model.dto.Slika;
import rs.shop.R;
import rs.shop.activities.ArtiklDetailActivity;
import rs.shop.rest.ArtikliCall;
import rs.shop.rest.ProdavciCall;
import rs.shop.rest.SlikeCall;
import rs.shop.tools.Token;

/*
* Adapteri unutar Android-a sluze da prikazu unapred nedefinisanu kolicinu podataka
* pristigle sa interneta ili ucitane iz baze ili filesystem-a uredjaja.
* Da bi napravili adapter treba da napraivmo klasu, koja nasledjuje neki od postojecih adaptera.
* Za potrebe ovih vezbi koristicemo BaseAdapter koji je sposoban da kao izvor podataka iskoristi listu ili niz.
* Nasledjivanjem bilo kog adaptera, dobicemo
* nekolkko metoda koje moramo da referinisemo da bi adapter ispravno radio.
* */
public class ArtikliAdapter extends RecyclerView.Adapter<ArtikliAdapter.ViewHolder> {

    private List<Artikal> artikli;
    private Activity activity;

    public ArtikliAdapter(List<Artikal> artikliZaProdavca, Activity activity) {

        this.artikli = artikliZaProdavca;
        this.activity = activity;

    }

    @NonNull
    @Override
    public ArtikliAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.artikal_list, viewGroup, false);

        return new ViewHolder(v);
    }


    @Override
    public int getItemCount() {
        return artikli.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView naziv;
        private final TextView opis;
        private final TextView cena;
        private final ImageView slika;
        private final Button dodajArtikal;
        private final TextView kolicina;

        ViewHolder(View v){
            super(v);
            naziv = (TextView)v.findViewById(R.id.nazivArtikla);
            opis = (TextView)v.findViewById(R.id.opisArtikla);
            cena = (TextView)v.findViewById(R.id.cenaArtikla);
            slika = (ImageView)v.findViewById(R.id.slikaArtikla);
            dodajArtikal = (Button)v.findViewById(R.id.btnDodajArtikal);
            kolicina = (TextView)v.findViewById(R.id.kolicinaArtikala);

            SharedPreferences sharedpreferences = activity.getSharedPreferences("mypref",
                    Context.MODE_PRIVATE);
            if (sharedpreferences.contains("token")) {
                String token = sharedpreferences.getString("token", "");
                if (Token.getUloga(token).equals("ROLE_PRODAVAC")) {
                    dodajArtikal.setEnabled(false);
                    dodajArtikal.setVisibility(View.GONE);

                    kolicina.setEnabled(false);
                    kolicina.setVisibility(View.GONE);
                }
            }
            else{
                dodajArtikal.setEnabled(false);
                dodajArtikal.setVisibility(View.GONE);

                kolicina.setEnabled(false);
                kolicina.setVisibility(View.GONE);
            }
        }
    }

    // prikazivanje elementa u recyclerview-u
    @Override
    public void onBindViewHolder(@NonNull ArtikliAdapter.ViewHolder viewHolder, int position) {
        Artikal artikal = artikli.get(position);
        viewHolder.naziv.setText(artikal.getNaziv());
        viewHolder.opis.setText(artikal.getOpis());
        viewHolder.cena.setText(String.valueOf(artikal.getCena()) + " RSD");

        // postavljanje slike ako postoji
        if (!artikal.getPutanjaSlike().equals("")) {
            Slika slika = new Slika();
            slika.setSlikaRelativePath(artikal.getPutanjaSlike());

            SlikeCall.loadSlika(slika, viewHolder.slika, null, activity);
        }

        // kad korisnik klikne na element iz liste?
        SharedPreferences sharedpreferences = activity.getSharedPreferences("mypref",
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains("token")) {
            viewHolder.naziv.setOnClickListener((view) -> {
                Context context = view.getContext();
                Intent intent = new Intent(activity, ArtiklDetailActivity.class);
                intent.putExtra("idArtikla", String.valueOf(artikal.getIdArtikla()));
                intent.putExtra("idProdavca", String.valueOf(artikal.getIdProdavca()));
                intent.putExtra("naziv", artikal.getNaziv());
                intent.putExtra("opis", artikal.getOpis());
                intent.putExtra("cena", String.valueOf(artikal.getCena()));
                intent.putExtra("putanjaSlike", artikal.getPutanjaSlike());
                context.startActivity(intent);
            });

            if (Token.getUloga(Token.getToken(activity)).equals("ROLE_KUPAC")){
                viewHolder.dodajArtikal.setOnClickListener((view) -> {
                    if (viewHolder.kolicina.equals("")){
                        Toast.makeText(activity, "Unesite kolicinu", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    try {
                        System.out.println(viewHolder.kolicina.getText().toString());
                        Integer kolicina = Integer.valueOf(viewHolder.kolicina.getText().toString());
                        ArrayList<String> artikliKorpa = activity.getIntent().getStringArrayListExtra("artikliKorpa");

                        for (Integer i = 0; i < kolicina; i++){
                            artikliKorpa.add(String.valueOf(artikal.getIdArtikla()));
                        }

                        activity.getIntent().putStringArrayListExtra("artikliKorpa",artikliKorpa);
                        viewHolder.kolicina.setText("");
                        Toast.makeText(activity, "Artikal je uspesno dodat u korpu", Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                        System.out.println(e);
                        Toast.makeText(activity, "Unesite validnu kolicinu", Toast.LENGTH_SHORT).show();
                    }
                    System.out.println(activity.getIntent().getStringArrayListExtra("artikliKorpa"));
                });
            }
        }

    }


}
