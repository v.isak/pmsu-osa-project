package rs.shop.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import model.dto.Prijava;
import model.dto.Registracija;
import model.enums.TipKorisnika;
import rs.shop.R;
import rs.shop.rest.KorisnikCall;

public class RegistrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioGroup.check(R.id.rbProdavac);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                TextView email = (TextView) findViewById(R.id.unosRegEmail);
                TextView naziv = (TextView) findViewById(R.id.unosRegNaziv);
                if (checkedRadioButton.getText().toString().equals("KUPAC"))
                {
                    // Changes the textview's text to "Checked: example radiobutton text"
                    email.setText("");
                    naziv.setText("");
                    email.setVisibility(View.INVISIBLE);
                    naziv.setVisibility(View.INVISIBLE);
                }
                else{
                    email.setVisibility(View.VISIBLE);
                    naziv.setVisibility(View.VISIBLE);
                }
            }
        });

        // dugme za registraciju
        Button buttonReg = (Button) findViewById(R.id.buttonReg);
        buttonReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView ime = (TextView) findViewById(R.id.unosRegIme);
                TextView prezime = (TextView) findViewById(R.id.unosRegPrezime);
                TextView username = (TextView) findViewById(R.id.unosRegUsername);
                TextView password = (TextView) findViewById(R.id.unosRegPassword);
                TextView adresa = (TextView) findViewById(R.id.unosRegAdresa);
                TextView email = (TextView) findViewById(R.id.unosRegEmail);
                TextView naziv = (TextView) findViewById(R.id.unosRegNaziv);

                RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
                int selectedId = radioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton) findViewById(selectedId);


                Registracija registracija = new Registracija();
                registracija.setIme(ime.getText().toString());
                registracija.setPrezime(prezime.getText().toString());
                registracija.setUsername(username.getText().toString());
                registracija.setPassword(password.getText().toString());
                registracija.setAdresa(adresa.getText().toString());
                registracija.setEmail(email.getText().toString());
                registracija.setNaziv(naziv.getText().toString());

                if (radioButton != null) {
                    TipKorisnika uloga = TipKorisnika.valueOf(radioButton.getText().toString());
                    registracija.setTipKorisnika(uloga);
                }



                KorisnikCall.registracija(registracija, RegistrationActivity.this);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}