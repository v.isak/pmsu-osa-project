package rs.shop.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import model.dto.Artikal;
import model.dto.Slika;
import model.dto.Stavka;
import rs.shop.R;
import rs.shop.activities.ArtiklDetailActivity;
import rs.shop.rest.SlikeCall;

/*
* Adapteri unutar Android-a sluze da prikazu unapred nedefinisanu kolicinu podataka
* pristigle sa interneta ili ucitane iz baze ili filesystem-a uredjaja.
* Da bi napravili adapter treba da napraivmo klasu, koja nasledjuje neki od postojecih adaptera.
* Za potrebe ovih vezbi koristicemo BaseAdapter koji je sposoban da kao izvor podataka iskoristi listu ili niz.
* Nasledjivanjem bilo kog adaptera, dobicemo
* nekolkko metoda koje moramo da referinisemo da bi adapter ispravno radio.
* */
public class ListArtikliPorudzbineAdapter extends RecyclerView.Adapter<ListArtikliPorudzbineAdapter.ViewHolder>{
    private List<Artikal> artikli;
    HashMap<Artikal, Integer> stavke = new HashMap<Artikal, Integer>();
    private Activity activity;

    public ListArtikliPorudzbineAdapter(List<Stavka> stavkeZaPorudzbinu, List<Artikal> artikliIzStavki, Activity activity) {

        HashMap<Artikal, Integer> stavke = new HashMap<Artikal, Integer>();

        // prolazenje kroz svaki od artikla jednom i izracunavanje koliko se puta nalazi u listi
        // odnosno koja je kolicina odabrana
        for (Artikal artikal : new HashSet<>(artikliIzStavki)){
            int kolicina = 1;
            for (Stavka stavka : stavkeZaPorudzbinu){
                if (artikal.getIdArtikla() == stavka.getIdArtikla()){
                    kolicina = stavka.getKolicina();
                    stavke.put(artikal,kolicina);
                    break;
                }
            }
        }

        this.stavke = stavke;
        this.artikli = new ArrayList<Artikal>(stavke.keySet());
        this.activity = activity;

    }

    @NonNull
    @Override
    public ListArtikliPorudzbineAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.artikal_list_porudzbina, viewGroup, false);

        return new ListArtikliPorudzbineAdapter.ViewHolder(v);
    }


    @Override
    public int getItemCount() {
        return artikli.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView naziv;
        private final TextView opis;
        private final TextView cena;
        private final TextView kolicina;
        private final ImageView slika;

        ViewHolder(View v){
            super(v);
            naziv = (TextView)v.findViewById(R.id.nazivArtikalList);
            opis = (TextView)v.findViewById(R.id.opisArtikalList);
            cena = (TextView)v.findViewById(R.id.cenaArtikalList);
            kolicina = (TextView)v.findViewById(R.id.kolicinaArtikala);
            slika = (ImageView)v.findViewById(R.id.slikaArtikalList);

        }
    }

    // prikazivanje elementa u recyclerview-u
    @Override
    public void onBindViewHolder(@NonNull ListArtikliPorudzbineAdapter.ViewHolder viewHolder, int position) {
        Artikal artikal = artikli.get(position);
        viewHolder.naziv.setText(artikal.getNaziv());
        viewHolder.opis.setText(artikal.getOpis());
        viewHolder.cena.setText(String.valueOf(artikal.getCena()) + " RSD");
        viewHolder.kolicina.setText(viewHolder.kolicina.getText()+String.valueOf(stavke.get(artikal)));

        // postavljanje slike ako postoji
        if (!artikal.getPutanjaSlike().equals("")) {
            Slika slika = new Slika();
            slika.setSlikaRelativePath(artikal.getPutanjaSlike());

            SlikeCall.loadSlika(slika, viewHolder.slika, null, activity);
        }

        // kad korisnik klikne na element iz liste?
        SharedPreferences sharedpreferences = activity.getSharedPreferences("mypref",
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains("token")) {
            viewHolder.naziv.setOnClickListener((view) -> {
                Context context = view.getContext();
                Intent intent = new Intent(activity, ArtiklDetailActivity.class);
                intent.putExtra("idArtikla", String.valueOf(artikal.getIdArtikla()));
                intent.putExtra("idProdavca", String.valueOf(artikal.getIdProdavca()));
                intent.putExtra("naziv", artikal.getNaziv());
                intent.putExtra("opis", artikal.getOpis());
                intent.putExtra("cena", String.valueOf(artikal.getCena()));
                intent.putExtra("putanjaSlike", artikal.getPutanjaSlike());
                context.startActivity(intent);
                activity.finish();
            });
        }

    }
}
