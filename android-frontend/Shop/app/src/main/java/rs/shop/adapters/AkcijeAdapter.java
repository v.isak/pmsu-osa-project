package rs.shop.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.dto.Akcija;
import model.dto.Artikal;
import model.dto.Prodavac;
import rs.shop.R;
import rs.shop.activities.AkcijaDetailActivity;
import rs.shop.activities.ArtiklDetailActivity;
import rs.shop.rest.AkcijeCall;
import rs.shop.rest.PorudzbineCall;
import rs.shop.rest.ProdavciCall;
import rs.shop.tools.Token;

/*
* Adapteri unutar Android-a sluze da prikazu unapred nedefinisanu kolicinu podataka
* pristigle sa interneta ili ucitane iz baze ili filesystem-a uredjaja.
* Da bi napravili adapter treba da napraivmo klasu, koja nasledjuje neki od postojecih adaptera.
* Za potrebe ovih vezbi koristicemo BaseAdapter koji je sposoban da kao izvor podataka iskoristi listu ili niz.
* Nasledjivanjem bilo kog adaptera, dobicemo
* nekolkko metoda koje moramo da referinisemo da bi adapter ispravno radio.
* */
public class AkcijeAdapter extends RecyclerView.Adapter<AkcijeAdapter.ViewHolder>{
    private List<Akcija> akcije;
    private Activity activity;


    public AkcijeAdapter(List<Akcija> akcijeZaProdavca, Activity activity) {

        List<Akcija> akcije = new ArrayList<Akcija>();

        // filtriranje akcija koje su vec zavrsene
        for (Akcija Akcija : akcijeZaProdavca){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            try {
                Date krajAkcije = sdf.parse(Akcija.getKrajAkcije().substring(0,16).replace("T"," "));
                if (krajAkcije.after(new Date())){
                    akcije.add(Akcija);
                }
            } catch (ParseException ex) {
                System.out.println(ex);
            }
        }

        this.akcije = akcije;
        this.activity = activity;
    }

    @NonNull
    @Override
    public AkcijeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.akcija_list, viewGroup, false);

        return new AkcijeAdapter.ViewHolder(v);
    }


    @Override
    public int getItemCount() {
        return akcije.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView opis;
        private final TextView procenat;
        private final TextView odKad;
        private final TextView doKad;

        ViewHolder(View v){
            super(v);
            opis = (TextView)v.findViewById(R.id.opis);
            procenat = (TextView)v.findViewById(R.id.procenat);
            odKad = (TextView)v.findViewById(R.id.odKad);
            doKad = (TextView)v.findViewById(R.id.doKad);
        }
    }

    // prikazivanje elementa u recyclerview-u
    @Override
    public void onBindViewHolder(@NonNull AkcijeAdapter.ViewHolder viewHolder, int position) {
        Akcija akcija = akcije.get(position);
        viewHolder.opis.setText(akcija.getTekst());
        viewHolder.procenat.setText(String.valueOf(akcija.getProcenat())+"%");
        viewHolder.odKad.setText("Akcija pocinje: " + akcija.getPocetakAkcije().toString().substring(0,16).replace("T"," "));
        viewHolder.doKad.setText("Akcija se zavrsava: " + akcija.getKrajAkcije().toString().substring(0,16).replace("T"," "));

        // kad korisnik klikne na element iz liste?
        SharedPreferences sharedpreferences = activity.getSharedPreferences("mypref",
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains("token")) {
            viewHolder.opis.setOnClickListener((view) -> {

                ArrayList<String> artikli = new ArrayList<String>();
                for (Long idArtikla : akcija.getArtikli()){
                    artikli.add(String.valueOf(idArtikla));
                }

                Context context = view.getContext();
                Intent intent = new Intent(activity, AkcijaDetailActivity.class);
                intent.putExtra("idAkcije", String.valueOf(akcija.getIdAkcije()));
                intent.putExtra("idProdavca", activity.getIntent().getStringExtra("idProdavca"));
                intent.putExtra("procenat", String.valueOf(akcija.getProcenat()));
                intent.putExtra("pocetakAkcije", akcija.getPocetakAkcije().toString().replace("T", " "));
                intent.putExtra("krajAkcije", akcija.getKrajAkcije().toString().replace("T", " "));
                intent.putExtra("opis", akcija.getTekst());
                intent.putStringArrayListExtra("artikli", artikli);
                context.startActivity(intent);
                //activity.finish();
            });
        }

    }

}
