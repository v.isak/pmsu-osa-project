package rs.shop.rest;

import android.app.Activity;
import android.util.Log;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import model.dto.Artikal;
import model.dto.Porudzbina;
import model.dto.Stavka;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rs.shop.adapters.ArtikliAdapter;
import rs.shop.adapters.KomentariAdapter;
import rs.shop.adapters.ListArtikliPorudzbineAdapter;
import rs.shop.adapters.PorudzbinaAdapter;
import rs.shop.tools.Token;

public class PorudzbineCall {

    static final String TAG = PorudzbineCall.class.getSimpleName();
    static List<Porudzbina> porudzbine = new ArrayList<Porudzbina>();
    static Porudzbina porudzbina;
    static final String BASE_URL = Token.BASE_URL;
    static Retrofit retrofit = null;

    public static List<Porudzbina> getPorudzbine(Activity activity, RecyclerView recyclerView){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        PorudzbineApiService PorudzbineApiService = retrofit.create(PorudzbineApiService.class);
        Call<List<Porudzbina>> call = PorudzbineApiService.getPorudzbine();

        call.enqueue(new Callback<List<Porudzbina>>() {
            @Override
            public void onResponse(Call<List<Porudzbina>> call, Response<List<Porudzbina>> response) {
                System.out.println(response.body());
                porudzbine.clear();
                if (response.body()!= null) {
                    porudzbine.addAll(response.body());
                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    recyclerView.setAdapter(new PorudzbinaAdapter(porudzbine,activity));

                }
                System.out.println(porudzbine);
            }
            @Override
            public void onFailure(Call<List<Porudzbina>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return porudzbine;

    }


    public static void getPorudzbineZaKupca(Long idKupca, Activity activity, RecyclerView recyclerView){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        PorudzbineApiService PorudzbineApiService = retrofit.create(PorudzbineApiService.class);
        Call<List<Porudzbina>> call = PorudzbineApiService.getPorudzbineZaKupca(idKupca);

        call.enqueue(new Callback<List<Porudzbina>>() {
            @Override
            public void onResponse(Call<List<Porudzbina>> call, Response<List<Porudzbina>> response) {
                System.out.println(response.body());
                porudzbine.clear();
                if (response.body()!= null) {
                    porudzbine.addAll(response.body());
                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    recyclerView.setAdapter(new PorudzbinaAdapter(porudzbine,activity));

                }
                System.out.println(porudzbine);
            }
            @Override
            public void onFailure(Call<List<Porudzbina>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }

    public static void getKomentariZaProdavca(Activity activity, RecyclerView recyclerView, Long idProdavca){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PorudzbineApiService PorudzbineApiService = retrofit.create(PorudzbineApiService.class);
        Call<List<Porudzbina>> call = PorudzbineApiService.getKomentariZaProdavca(idProdavca);

        call.enqueue(new Callback<List<Porudzbina>>() {
            @Override
            public void onResponse(Call<List<Porudzbina>> call, Response<List<Porudzbina>> response) {
                System.out.println(response.body());
                porudzbine.clear();
                if (response.body()!= null) {
                    porudzbine.addAll(response.body());

                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    recyclerView.setAdapter(new KomentariAdapter(porudzbine, activity));
                }
                System.out.println(porudzbine);
            }
            @Override
            public void onFailure(Call<List<Porudzbina>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }

    public static void setOcenaZaProdavca(Activity activity, Long idProdavca, TextView prosecnaOcenaView,
                                          RatingBar ratingBar){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PorudzbineApiService PorudzbineApiService = retrofit.create(PorudzbineApiService.class);
        Call<List<Porudzbina>> call = PorudzbineApiService.getKomentariZaProdavca(idProdavca);

        call.enqueue(new Callback<List<Porudzbina>>() {
            @Override
            public void onResponse(Call<List<Porudzbina>> call, Response<List<Porudzbina>> response) {
                System.out.println(response.body());
                porudzbine.clear();
                if (response.body()!= null) {
                    porudzbine.addAll(response.body());

                    System.out.println("Porudzbine: " + porudzbine);
                    float prosecnaOcena = 0;
                    int brojOcena = 0;
                    for (Porudzbina porudzbina : porudzbine){
                        if (porudzbina.getOcena() != 0) {
                            brojOcena += 1;
                            prosecnaOcena += porudzbina.getOcena();
                        }
                    }
                    prosecnaOcena = prosecnaOcena / brojOcena;
                    if (prosecnaOcenaView != null) {
                        if (String.valueOf(prosecnaOcena).length() > 3){
                            prosecnaOcenaView.setText(String.valueOf(prosecnaOcena).substring(0,3) + "/5");
                        }
                        else {
                            prosecnaOcenaView.setText(String.valueOf(prosecnaOcena) + "/5");
                        }
                    }
                    else if (ratingBar != null){
                        ratingBar.setRating(prosecnaOcena);
                        //actionBar.setTitle(actionBar.getTitle() + "\n" + "Prosecna ocena: " + String.valueOf(prosecnaOcena) + "/5");
                    }
                    activity.getIntent().putExtra("prosecnaOcena", String.valueOf(prosecnaOcena));

                }
                System.out.println(porudzbine);
            }
            @Override
            public void onFailure(Call<List<Porudzbina>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }


    public static Porudzbina getPorudzbina(Long idPorudzbine, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        PorudzbineApiService PorudzbineApiService = retrofit.create(PorudzbineApiService.class);
        Call<Porudzbina> call = PorudzbineApiService.getPorudzbina(idPorudzbine);

        call.enqueue(new Callback<Porudzbina>() {
            @Override
            public void onResponse(Call<Porudzbina> call, Response<Porudzbina> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    activity.finish();
                    porudzbina = response.body();
                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Porudzbina> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return porudzbina;

    }


    public static Porudzbina createPorudzbina(Porudzbina porudzbinaInfo, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        PorudzbineApiService PorudzbineApiService = retrofit.create(PorudzbineApiService.class);
        Call<Porudzbina> call = PorudzbineApiService.createPorudzbina(porudzbinaInfo);

        call.enqueue(new Callback<Porudzbina>() {
            @Override
            public void onResponse(Call<Porudzbina> call, Response<Porudzbina> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 201){
                    activity.finish();
                    porudzbina = response.body();

                    // kreiranje stavki nakon porudzbine
                    ArrayList<Stavka> stavke = new ArrayList<Stavka>();
                    ArrayList<String> artikliKorpa = activity.getIntent().getStringArrayListExtra("artikliKorpa");

                    System.out.println(artikliKorpa);
                    for (String idArtikla : new HashSet<>(artikliKorpa)){
                        Stavka stavka = new Stavka();
                        stavka.setIdPorudzbine(porudzbina.getIdPorudzbine());
                        stavka.setIdArtikla(Long.valueOf(idArtikla));
                        stavka.setKolicina(0);

                        for (String idArtikla1 : artikliKorpa){
                            if (Long.valueOf(idArtikla) == Long.valueOf(idArtikla1)){
                                stavka.setKolicina(stavka.getKolicina()+1);
                            }
                        }
                        stavke.add(stavka);
                    }
                    // kreiranje stavki na back-endu
                    for (Stavka stavka : stavke){
                        StavkeCall.createStavka(stavka,activity);
                    }

                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Porudzbina> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return porudzbina;

    }

    public static Porudzbina updatePorudzbina(Long idPorudzbine, Porudzbina porudzbinaInfo,final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        PorudzbineApiService PorudzbineApiService = retrofit.create(PorudzbineApiService.class);

        Call<Porudzbina> callGet = PorudzbineApiService.getPorudzbina(idPorudzbine);

        callGet.enqueue(new Callback<Porudzbina>() {
            @Override
            public void onResponse(Call<Porudzbina> call, Response<Porudzbina> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){

                    porudzbina = response.body();

                    if (porudzbinaInfo.getKomentar() != null){
                        porudzbina.setKomentar(porudzbinaInfo.getKomentar());
                    }
                    // postavljanje anonimnog komentara kod ocene jer se postavlja samo kad se ocenjuje
                    if (porudzbinaInfo.getOcena() != 0){
                        porudzbina.setAnonimanKomentar(porudzbinaInfo.isAnonimanKomentar());
                        porudzbina.setOcena(porudzbinaInfo.getOcena());
                    }
                    if (porudzbinaInfo.isDostavljeno()){
                        porudzbina.setDostavljeno(true);
                    }

                    // ako su ocena, komentar i dostavljeno neprosledjeni znaci da se radi o arhiviranju
                    // ili dearhiviranju
                    if (porudzbinaInfo.getOcena() == 0 && porudzbinaInfo.getKomentar() == null && porudzbinaInfo.isDostavljeno() == false){
                        porudzbina.setArhiviraniKomentar(porudzbinaInfo.isArhiviraniKomentar());
                    }

                    Call<Porudzbina> callUpdate = PorudzbineApiService.updatePorudzbina(porudzbina.getIdPorudzbine(),porudzbina);

                    callUpdate.enqueue(new Callback<Porudzbina>() {
                        @Override
                        public void onResponse(Call<Porudzbina> call, Response<Porudzbina> response) {
                            System.out.println(response.body());
                            System.out.println(response.code());
                            if (response.code() == 200){
                                // ako su ocena i komentar neprosledjeni znaci da se radi o arhiviranju
                                // ili dearhiviranju
                                if (porudzbinaInfo.getOcena() != 0 && porudzbinaInfo.getKomentar() != null) {
                                    activity.finish();
                                }
                                porudzbina = response.body();
                            }
                            else{
                                Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<Porudzbina> call, Throwable throwable) {
                            Log.e(TAG, throwable.toString());
                        }
                    });



                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Porudzbina> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });



        return porudzbina;

    }

    public static Porudzbina deletePorudzbina(Long idPorudzbine, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        PorudzbineApiService PorudzbineApiService = retrofit.create(PorudzbineApiService.class);
        Call<ResponseBody> call = PorudzbineApiService.deletePorudzbina(idPorudzbine);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    activity.finish();
                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return porudzbina;

    }

}
