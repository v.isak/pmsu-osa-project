package rs.shop.rest;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import model.dto.Administrator;
import model.dto.Kupac;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rs.shop.R;
import rs.shop.tools.Token;

public class AdminiCall {

    static final String TAG = AdminiCall.class.getSimpleName();
    static List<Administrator> admini = new ArrayList<Administrator>();
    static Administrator admin;
    static final String BASE_URL = Token.BASE_URL;
    static Retrofit retrofit = null;

    public static List<Administrator> getAdmini(Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        AdminiApiService adminiApiService = retrofit.create(AdminiApiService.class);
        Call<List<Administrator>> call = adminiApiService.getAdmini();

        call.enqueue(new Callback<List<Administrator>>() {
            @Override
            public void onResponse(Call<List<Administrator>> call, Response<List<Administrator>> response) {
                System.out.println(response.body());
                admini.clear();
                if (response.body()!= null) {
                    admini.addAll(response.body());
                }
                System.out.println(admini);
            }
            @Override
            public void onFailure(Call<List<Administrator>> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return admini;

    }

    public static Administrator getAdmin(Long idAdmina, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        AdminiApiService AdminiApiService = retrofit.create(AdminiApiService.class);
        Call<Administrator> call = AdminiApiService.getAdmin(idAdmina);

        call.enqueue(new Callback<Administrator>() {
            @Override
            public void onResponse(Call<Administrator> call, Response<Administrator> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    admin = response.body();

                    TextView ime = (TextView) activity.findViewById(R.id.unosRegIme);
                    TextView prezime = (TextView) activity.findViewById(R.id.unosRegPrezime);
                    TextView username = (TextView) activity.findViewById(R.id.unosRegUsername);

                    ime.setText(admin.getImeKorisnika());
                    prezime.setText(admin.getPrezimeKorisnika());
                    username.setText(admin.getKorisnickoIme());
                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Administrator> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return admin;

    }

    public static Administrator getAdminByUsername(String username, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        AdminiApiService AdminiApiService = retrofit.create(AdminiApiService.class);
        Call<Administrator> call = AdminiApiService.getAdminByUsername(username);

        call.enqueue(new Callback<Administrator>() {
            @Override
            public void onResponse(Call<Administrator> call, Response<Administrator> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.isSuccessful()){
                    System.out.println("GetProdavacByUsername: SUCCESSFUL");
                    activity.finish();
                    admin = response.body();
                }
                else{
                    System.out.println("GetProdavacByUsername: UNSUCCESSFUL");
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<Administrator> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return admin;

    }

    public static Administrator createAdmin(Administrator adminInfo, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        AdminiApiService AdminiApiService = retrofit.create(AdminiApiService.class);
        Call<Administrator> call = AdminiApiService.createAdmin(adminInfo);

        call.enqueue(new Callback<Administrator>() {
            @Override
            public void onResponse(Call<Administrator> call, Response<Administrator> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 201){
                    activity.finish();
                    admin = response.body();
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Administrator> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return admin;

    }

    public static Administrator updateAdmin(Administrator adminInfo, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        AdminiApiService AdminiApiService = retrofit.create(AdminiApiService.class);
        Call<Administrator> call = AdminiApiService.updateAdmin(adminInfo.getIdKorisnika(),adminInfo);

        call.enqueue(new Callback<Administrator>() {
            @Override
            public void onResponse(Call<Administrator> call, Response<Administrator> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    activity.finish();
                    admin = response.body();
                }
                else{
                    Toast.makeText(activity, "Losi podaci", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Administrator> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return admin;

    }

    public static Administrator deleteAdmin(Long idAdmina, final Activity activity){
        retrofit = new Retrofit.Builder()
                .client(Token.getTokenAuthHeader(Token.getToken(activity)))
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        AdminiApiService adminiApiService = retrofit.create(AdminiApiService.class);
        Call<ResponseBody> call = adminiApiService.deleteAdmin(idAdmina);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                System.out.println(response.body());
                System.out.println(response.code());
                if (response.code() == 200){
                    activity.finish();
                }
                else{
                    Toast.makeText(activity, "Los zahtev", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

        return admin;

    }

}
