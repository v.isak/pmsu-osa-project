package rs.shop.rest;

import java.util.List;

import model.dto.Artikal;
import model.dto.Korisnik;
import model.dto.Prijava;
import model.dto.Registracija;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface KorisnikApiService {

    @GET("Blokiraj/{id}")
    Call<Void> blokirajKorisnika(@Path("id") Long id);

    @GET("Odblokiraj/{id}")
    Call<Void> odblokirajKorisnika(@Path("id") Long id);

    @GET("Korisnici")
    Call<List<Korisnik>> getKorisnici();

    @POST("Prijava")
    Call<String> login(@Body Prijava prijava);

    @POST("Odjava")
    Call<Void> logout();

    @POST("Registracija")
    Call<Void> registration(@Body Registracija registracija);

}
