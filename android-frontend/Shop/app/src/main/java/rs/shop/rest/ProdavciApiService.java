package rs.shop.rest;

import java.util.List;

import model.dto.Prodavac;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ProdavciApiService {

    @GET("Prodavci")
    Call<List<Prodavac>> getProdavci();

    @GET("Prodavci/{id}")
    Call<Prodavac> getProdavac(@Path("id") Long id);

    @GET("Prodavci/Porudzbina/{id}")
    Call<Prodavac> getProdavacZaPorudzbinu(@Path("id") Long idPorudzbine);

    @GET("Prodavci/KorisnickoIme/{username}")
    Call<Prodavac> getProdavacByUsername(@Path("username") String username);

    @POST("Prodavci")
    Call<Prodavac> createProdavac(@Body Prodavac prodavac);

    @PUT("Prodavci/{id}")
    Call<Prodavac> updateProdavac(@Path("id") Long id, @Body Prodavac prodavac);

    @DELETE("Prodavci/{id}")
    Call<ResponseBody> deleteProdavac(@Path("id") Long id);

}
