package rs.shop.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import model.dto.Porudzbina;
import model.dto.Stavka;
import rs.shop.R;
import rs.shop.fragments.ArtikliFragment;
import rs.shop.fragments.KorpaFragment;
import rs.shop.rest.AkcijeCall;
import rs.shop.rest.PorudzbineCall;
import rs.shop.tools.FragmentTransition;
import rs.shop.tools.Token;

public class KorpaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_korpa);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Korpa");
        invalidateOptionsMenu();

        Button btnPoruci = findViewById(R.id.btnPoruciKorpa);

        btnPoruci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    Porudzbina porudzbina = new Porudzbina();
                    porudzbina.setIdKupca(Token.getId(Token.getToken(KorpaActivity.this)));

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    porudzbina.setSatnica(sdf.format(new Date()).replace(" ","T"));
                    TextView ukupnaCenaView = findViewById(R.id.ukupnaCenaKorpa);

                    String stringCena = ukupnaCenaView.getText().toString().split(" ")[2];
                    System.out.println(stringCena);
                    porudzbina.setCenaPorudzbine(Float.valueOf(stringCena));
                    PorudzbineCall.createPorudzbina(porudzbina,KorpaActivity.this);

                    Toast.makeText(KorpaActivity.this, "Porudzbina je uspesno kreirana", Toast.LENGTH_SHORT).show();

                    // odradjeno u createPorudzbina u PorudzbineCall
                    /*ArrayList<Stavka> stavke = new ArrayList<Stavka>();
                    ArrayList<String> artikliKorpa = getIntent().getStringArrayListExtra("artikliKorpa");

                    for (String idArtikla : new HashSet<>(artikliKorpa)){
                        Stavka stavka = new Stavka();
                        stavka.setIdArtikla(Long.valueOf(idArtikla));
                        stavka.setKolicina(0);

                        for (String idArtikla1 : artikliKorpa){
                            if (idArtikla == idArtikla1){
                                stavka.setKolicina(stavka.getKolicina()+1);
                            }
                        }
                        stavke.add(stavka);
                    }*/


                    //AkcijeCall.deleteAkcija(Long.valueOf(getIntent().getStringExtra("idAkcije")),AkcijaDetailActivity.this);
                }

                catch(Exception e){
                    e.printStackTrace();
                }
            }
        });

        FragmentTransition.to(KorpaFragment.newInstance(), this, true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
