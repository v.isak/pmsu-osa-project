package rs.shop.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import model.dto.Prijava;
import rs.shop.R;
import rs.shop.fragments.ArtikliListaFragment;
import rs.shop.fragments.ArtikliListaPorudzbinaFragment;
import rs.shop.rest.KorisnikCall;
import rs.shop.rest.ProdavciCall;
import rs.shop.tools.FragmentTransition;
import rs.shop.tools.Token;

public class PorudzbinaDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_porudzbina_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView idPorudzbine = findViewById(R.id.idPorudzbineDetail);
        TextView vremePorudzbine = findViewById(R.id.vremePorudzbineDetail);
        TextView nazivProdavca = findViewById(R.id.nazivProdavcaPorudzbinaDetail);
        TextView cenaPorudzbine = findViewById(R.id.cenaPorudzbine);

        idPorudzbine.setText(idPorudzbine.getText() + getIntent().getStringExtra("idPorudzbine"));
        vremePorudzbine.setText(vremePorudzbine.getText() + "\n" +getIntent().getStringExtra("vremePorudzbine").replace("T", " "));
        ProdavciCall.setProdavacNaziv(PorudzbinaDetailActivity.this, Long.valueOf(getIntent().getStringExtra("idPorudzbine")), nazivProdavca);
        cenaPorudzbine.setText(cenaPorudzbine.getText() + getIntent().getStringExtra("cenaPorudzbine") + " RSD");

        // dugme za dodavanje
        Button button = (Button) findViewById(R.id.btnOstaviKomentar);

        String token = rs.shop.tools.Token.getToken(PorudzbinaDetailActivity.this);
        String uloga = rs.shop.tools.Token.getUloga(token);

        // PRODAVAC ILI VEC OCENJENO
        if(Boolean.valueOf(getIntent().getStringExtra("ocenjenaPorudzbina")) ||
                uloga.equals("ROLE_PRODAVAC") || Boolean.valueOf(getIntent().getStringExtra("dostavljenaPorudzbina")) == false){
            button.setEnabled(false);
            button.setVisibility(View.INVISIBLE);
        }

        // listener
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PorudzbinaDetailActivity.this, KomentarActivity.class);
                intent.putExtra("idPorudzbine", getIntent().getStringExtra("idPorudzbine"));
                startActivity(intent);
                finish();
            }
        });

        FragmentTransition.to(ArtikliListaPorudzbinaFragment.newInstance(), this, false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
