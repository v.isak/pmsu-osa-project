package rs.shop.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import model.dto.Prodavac;
import rs.shop.R;
import rs.shop.activities.KorisnikDetailActivity;
import rs.shop.activities.LoginActivity;
import rs.shop.activities.ProdavacActivity;
import rs.shop.adapters.ProdavciAdapter;
import rs.shop.rest.ArtikliCall;
import rs.shop.rest.KorisnikCall;
import rs.shop.rest.ProdavciCall;
import rs.shop.tools.FragmentTransition;
import rs.shop.tools.Token;


public class KupacFragment extends Fragment {

	public static KupacFragment newInstance() {
        return new KupacFragment();
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle data) {
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.map_layout, container, false);
        RecyclerView recyclerView = (RecyclerView)v.findViewById(R.id.recyclerView);
        ProdavciCall.getProdavci(getActivity(),recyclerView);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        // update-uje se recycler view odnosno podaci u njemu
        RecyclerView recyclerView = (RecyclerView)getView().findViewById(R.id.recyclerView);
        ProdavciCall.getProdavci(getActivity(),recyclerView);

    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        // ovo korostimo ako je nasa arhitekrura takva da imamo jednu aktivnost
        // i vise fragmentaa gde svaki od njih ima svoj menu unutar toolbar-a
        menu.clear();
        inflater.inflate(R.menu.activity_itemdetail_kupac, menu);
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("mypref",
                Context.MODE_PRIVATE);
        if (!sharedpreferences.contains("token")) {
            menu.findItem(R.id.action_prodavci).setEnabled(false);
            menu.findItem(R.id.action_porudzbine).setEnabled(false);
            menu.findItem(R.id.action_odjava).setEnabled(false);

            menu.findItem(R.id.action_prodavci).setVisible(false);
            menu.findItem(R.id.action_porudzbine).setVisible(false);
            menu.findItem(R.id.action_odjava).setVisible(false);
        }
    }

    /*
    * Da bi znali na koju ikonicu je korisnik kliknuo
    * treba da iskoristimo jedinstveni identifikator
    * za svaki element u listi. Metoda onOptionsItemSelected
    * ce vratiti MenuItem na koji je korisnik kliknuo. Ako znamo
    * da ce on vratiti i id, tacno znamo na koji element je korisnik
    * kliknuo, i shodno tome reagujemo.
    * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_porudzbine){
            FragmentTransition.to(PorudzbineFragment.newInstance(), getActivity(), true);
        }


        else if(id == R.id.action_odjava){
            KorisnikCall.odjava(getActivity());
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
        }

        else if (id == R.id.action_user){
            Intent intent = new Intent(getActivity(), KorisnikDetailActivity.class);
            if (Token.getUloga(Token.getToken(getActivity())).equals("ROLE_PRODAVAC")) {
                intent.putExtra("putanjaSlike", getActivity().getIntent().getStringExtra("putanjaSlike"));
            }
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }


}