package rs.shop.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import rs.shop.R;
import rs.shop.fragments.KorisniciFragment;
import rs.shop.tools.FragmentTransition;


public class AdminActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Administrator");
        invalidateOptionsMenu();

        FragmentTransition.to(KorisniciFragment.newInstance(), this, true);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}