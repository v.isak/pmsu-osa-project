package model.enums;

public enum TipKorisnika {
	KUPAC,
	PRODAVAC,
	ADMINISTRATOR
}
