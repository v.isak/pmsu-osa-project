package model.dto;

import java.io.Serializable;

public class Stavka implements Serializable {

	private Long idStavke;
	private Long idArtikla;
	private Long idPorudzbine;
	private int kolicina;
	
	
	
	
	
	public Stavka() {
		super();
	}

	public Stavka(Long idStavke, Long idArtikla, Long idPorudzbine, int kolicina) {
		super();
		this.idStavke = idStavke;
		this.idArtikla = idArtikla;
		this.idPorudzbine = idPorudzbine;
		this.kolicina = kolicina;
	}

	
	
	public Long getIdStavke() {
		return idStavke;
	}

	public void setIdStavke(Long idStavke) {
		this.idStavke = idStavke;
	}

	public Long getIdArtikla() {
		return idArtikla;
	}

	public void setIdArtikla(Long idArtikla) {
		this.idArtikla = idArtikla;
	}

	public Long getIdPorudzbine() {
		return idPorudzbine;
	}

	public void setIdPorudzbine(Long idPorudzbine) {
		this.idPorudzbine = idPorudzbine;
	}

	public int getKolicina() {
		return kolicina;
	}

	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}
	
	
	
}
