package model.dto;

import java.io.Serializable;

import model.enums.TipKorisnika;

public class Prodavac implements Serializable {

    private Long idKorisnika;
    private String imeKorisnika;
    private String prezimeKorisnika;
    private String korisnickoIme;
    private String lozinkaKorisnika;
    private boolean blokiran;
    private TipKorisnika tipKorisnika;
    private String poslujeOd;
    private String email;
    private String adresa;
    private String naziv;
    private String putanjaSlike;


    public Prodavac() {
        super();
    }

    public Prodavac(Long idKorisnika, String imeKorisnika, String prezimeKorisnika, String korisnickoIme,
                               String lozinkaKorisnika, boolean blokiran, TipKorisnika tipKorisnika, String poslujeOd, String email,
                               String adresa, String naziv, String putanjaSlike) {
        super();
        this.idKorisnika = idKorisnika;
        this.imeKorisnika = imeKorisnika;
        this.prezimeKorisnika = prezimeKorisnika;
        this.korisnickoIme = korisnickoIme;
        this.lozinkaKorisnika = lozinkaKorisnika;
        this.blokiran = blokiran;
        this.tipKorisnika = tipKorisnika;
        this.poslujeOd = poslujeOd;
        this.email = email;
        this.adresa = adresa;
        this.naziv = naziv;
        this.putanjaSlike = putanjaSlike;
    }




    public Long getIdKorisnika() {
        return idKorisnika;
    }

    public void setIdKorisnika(Long idKorisnika) {
        this.idKorisnika = idKorisnika;
    }

    public String getImeKorisnika() {
        return imeKorisnika;
    }

    public void setImeKorisnika(String imeKorisnika) {
        this.imeKorisnika = imeKorisnika;
    }

    public String getPrezimeKorisnika() {
        return prezimeKorisnika;
    }

    public void setPrezimeKorisnika(String prezimeKorisnika) {
        this.prezimeKorisnika = prezimeKorisnika;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getLozinkaKorisnika() {
        return lozinkaKorisnika;
    }

    public void setLozinkaKorisnika(String lozinkaKorisnika) {
        this.lozinkaKorisnika = lozinkaKorisnika;
    }

    public boolean isBlokiran() {
        return blokiran;
    }

    public void setBlokiran(boolean blokiran) {
        this.blokiran = blokiran;
    }

    public TipKorisnika getTipKorisnika() {
        return tipKorisnika;
    }

    public void setTipKorisnika(TipKorisnika tipKorisnika) {
        this.tipKorisnika = tipKorisnika;
    }

    public String getPoslujeOd() {
        return poslujeOd;
    }

    public void setPoslujeOd(String poslujeOd) {
        this.poslujeOd = poslujeOd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getPutanjaSlike() {
        return putanjaSlike;
    }

    public void setPutanjaSlike(String putanjaSlike) {
        this.putanjaSlike = putanjaSlike;
    }
}
