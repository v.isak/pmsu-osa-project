package model.dto;

import java.io.Serializable;

import model.enums.TipKorisnika;


public class Administrator implements Serializable{
	
	private Long idKorisnika;
	private String imeKorisnika;
    private String prezimeKorisnika;
    private String korisnickoIme;
    private String lozinkaKorisnika;
    private boolean blokiran;
    private TipKorisnika tipKorisnika;
    
    
    
	public Administrator() {
		super();
	}



	public Administrator(String imeKorisnika, String prezimeKorisnika, String korisnickoIme,
			String lozinkaKorisnika, boolean blokiran, TipKorisnika tipKorisnika) {
		super();
		this.imeKorisnika = imeKorisnika;
		this.prezimeKorisnika = prezimeKorisnika;
		this.korisnickoIme = korisnickoIme;
		this.lozinkaKorisnika = lozinkaKorisnika;
		this.blokiran = blokiran;
		this.tipKorisnika = tipKorisnika;
	}


	
	

	public Long getIdKorisnika() {
		return idKorisnika;
	}



	public void setIdKorisnika(Long idKorisnika) {
		this.idKorisnika = idKorisnika;
	}



	public String getImeKorisnika() {
		return imeKorisnika;
	}



	public void setImeKorisnika(String imeKorisnika) {
		this.imeKorisnika = imeKorisnika;
	}



	public String getPrezimeKorisnika() {
		return prezimeKorisnika;
	}



	public void setPrezimeKorisnika(String prezimeKorisnika) {
		this.prezimeKorisnika = prezimeKorisnika;
	}



	public String getKorisnickoIme() {
		return korisnickoIme;
	}



	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}



	public String getLozinkaKorisnika() {
		return lozinkaKorisnika;
	}



	public void setLozinkaKorisnika(String lozinkaKorisnika) {
		this.lozinkaKorisnika = lozinkaKorisnika;
	}



	public boolean isBlokiran() {
		return blokiran;
	}



	public void setBlokiran(boolean blokiran) {
		this.blokiran = blokiran;
	}



	public TipKorisnika getTipKorisnika() {
		return tipKorisnika;
	}



	public void setTipKorisnika(TipKorisnika tipKorisnika) {
		this.tipKorisnika = tipKorisnika;
	}
		
    

}
