package model.dto;

import java.io.Serializable;

public class Slika implements Serializable {

	private String slikaString;
	private String slikaRelativePath;
	
	
	public Slika() {
		super();
	}
	
	public Slika(String slikaString) {
		super();
		this.slikaString = slikaString;
	}
	
	public Slika(String slikaString, String slikaRelativePath) {
		super();
		this.slikaString = slikaString;
		this.slikaRelativePath = slikaRelativePath;
	}
	
	
	public String getSlikaString() {
		return slikaString;
	}
	public void setSlikaString(String slikaString) {
		this.slikaString = slikaString;
	}
	public String getSlikaRelativePath() {
		return slikaRelativePath;
	}
	public void setSlikaRelativePath(String slikaRelativePath) {
		this.slikaRelativePath = slikaRelativePath;
	}
	
}
