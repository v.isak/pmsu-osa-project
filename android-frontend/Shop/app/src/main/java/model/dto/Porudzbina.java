package model.dto;

import java.io.Serializable;


public class Porudzbina implements Serializable {

	private Long idPorudzbine;
	private Long idKupca;
	private String satnica;
	private boolean dostavljeno;
	private float ocena;
	private String komentar;
	private boolean anonimanKomentar;
	private boolean arhiviraniKomentar;
	private float cenaPorudzbine;
	
	
	public Porudzbina() {
		super();
	}

	public Porudzbina(Long idPorudzbine, Long idKupca, String satnica, boolean dostavljeno, float ocena,
			String komentar, boolean anonimanKomentar, boolean arhiviraniKomentar, float cenaPorudzbine) {
		super();
		this.idPorudzbine = idPorudzbine;
		this.idKupca = idKupca;
		this.satnica = satnica;
		this.dostavljeno = dostavljeno;
		this.ocena = ocena;
		this.komentar = komentar;
		this.anonimanKomentar = anonimanKomentar;
		this.arhiviraniKomentar = arhiviraniKomentar;
		this.cenaPorudzbine = cenaPorudzbine;
	}

	public float getCenaPorudzbine() {
		return cenaPorudzbine;
	}

	public void setCenaPorudzbine(float cenaPorudzbine) {
		this.cenaPorudzbine = cenaPorudzbine;
	}

	public Long getIdPorudzbine() {
		return idPorudzbine;
	}

	public void setIdPorudzbine(Long idPorudzbine) {
		this.idPorudzbine = idPorudzbine;
	}

	public Long getIdKupca() {
		return idKupca;
	}

	public void setIdKupca(Long idKupca) {
		this.idKupca = idKupca;
	}

	public String getSatnica() {
		return satnica;
	}

	public void setSatnica(String satnica) {
		this.satnica = satnica;
	}

	public boolean isDostavljeno() {
		return dostavljeno;
	}

	public void setDostavljeno(boolean dostavljeno) {
		this.dostavljeno = dostavljeno;
	}

	public float getOcena() {
		return ocena;
	}

	public void setOcena(float ocena) {
		this.ocena = ocena;
	}

	public String getKomentar() {
		return komentar;
	}

	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}

	public boolean isAnonimanKomentar() {
		return anonimanKomentar;
	}

	public void setAnonimanKomentar(boolean anonimanKomentar) {
		this.anonimanKomentar = anonimanKomentar;
	}

	public boolean isArhiviraniKomentar() {
		return arhiviraniKomentar;
	}

	public void setArhiviraniKomentar(boolean arhiviraniKomentar) {
		this.arhiviraniKomentar = arhiviraniKomentar;
	}
	
	
}
