package model.dto;

import com.google.gson.annotations.SerializedName;

import model.enums.TipKorisnika;

public class Registracija {
    @SerializedName("ime")
    private String ime;
    @SerializedName("prezime")
    private String prezime;
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;
    @SerializedName("adresa")
    private String adresa;
    @SerializedName("email")
    private String email;
    @SerializedName("naziv")
    private String naziv;
    @SerializedName("tipKorisnika")
    private TipKorisnika tipKorisnika;


    public Registracija() {
    }

    public Registracija(String ime, String prezime, String username,
                        String password, String adresa, String email,
                        String naziv, TipKorisnika tipKorisnika) {
        this.ime = ime;
        this.prezime = prezime;
        this.username = username;
        this.password = password;
        this.adresa = adresa;
        this.email = email;
        this.naziv = naziv;
        this.tipKorisnika = tipKorisnika;
    }


    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public TipKorisnika getTipKorisnika() {
        return tipKorisnika;
    }

    public void setTipKorisnika(TipKorisnika tipKorisnika) {
        this.tipKorisnika = tipKorisnika;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
