package com.projekat.pmsu_osa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PmsuOsaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PmsuOsaApplication.class, args);
	}

}
