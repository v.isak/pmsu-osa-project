package com.projekat.pmsu_osa.enums;

public enum TipKorisnika {
	KUPAC,
	PRODAVAC,
	ADMINISTRATOR
}
