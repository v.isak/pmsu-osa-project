package com.projekat.pmsu_osa.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projekat.pmsu_osa.models.dto.AdministratorBackendDTO;
import com.projekat.pmsu_osa.models.dto.AdministratorFrontendDTO;
import com.projekat.pmsu_osa.models.dto.AkcijaFrontendDTO;
import com.projekat.pmsu_osa.models.dto.ArtiklBackendDTO;
import com.projekat.pmsu_osa.models.dto.ArtiklFrontendDTO;
import com.projekat.pmsu_osa.models.entity.Administrator;
import com.projekat.pmsu_osa.models.entity.Akcija;
import com.projekat.pmsu_osa.models.entity.Artikl;
import com.projekat.pmsu_osa.models.entity.Korisnik;
import com.projekat.pmsu_osa.security.TokenUtils;
import com.projekat.pmsu_osa.services.IAdministratorService;
import com.projekat.pmsu_osa.services.IAkcijaService;
import com.projekat.pmsu_osa.services.IArtiklService;
import com.projekat.pmsu_osa.services.IProdavacService;
import com.projekat.pmsu_osa.services.implementation.UserDetailsServiceImpl;

@CrossOrigin
@RestController
@RequestMapping(value = "/Artikli")
public class ArtiklController {
	
	@Autowired
	private TokenUtils tokenUtils;
	
	@Autowired
    private IAkcijaService akcijeService;

	@Autowired
    private IArtiklService artikliService;
	
	@Autowired
	private IProdavacService prodavacService;
    
	@PreAuthorize("hasAnyRole('ADMINISTRATOR')")
    @GetMapping
    public ResponseEntity<List<ArtiklFrontendDTO>> allArtikli(){
		
		
		System.out.println("pogodak");
        List<Artikl> artikli = artikliService.findAll();
        List<ArtiklFrontendDTO> artikliFrontendDTO = new ArrayList<ArtiklFrontendDTO>();
        
        for (Artikl artikl: artikli) {
        	ArtiklFrontendDTO artiklFrontendDTO = new ArtiklFrontendDTO(artikl);
        	artikliFrontendDTO.add(artiklFrontendDTO);
        }
        return new ResponseEntity<List<ArtiklFrontendDTO>>(artikliFrontendDTO,HttpStatus.OK);
    }
	
	@PreAuthorize("hasAnyRole('KUPAC','ADMINISTRATOR','PRODAVAC','ANONYMOUS')")
    @GetMapping(value = "/Prodavac/{id}")
    public ResponseEntity<List<ArtiklFrontendDTO>> artikliZaProdavca(@PathVariable(name = "id") Long idProdavca){
		System.out.println("pogodak");
        List<Artikl> artikli = artikliService.findAll();
        List<ArtiklFrontendDTO> artikliFrontendDTO = new ArrayList<ArtiklFrontendDTO>();
        
        for (Artikl artikl: artikli) {
        	if (artikl.getProdavac().getIdKorisnika() == idProdavca) {
	        	ArtiklFrontendDTO artiklFrontendDTO = new ArtiklFrontendDTO(artikl);
	        	artikliFrontendDTO.add(artiklFrontendDTO);
        	}
        }
        return new ResponseEntity<List<ArtiklFrontendDTO>>(artikliFrontendDTO,HttpStatus.OK);
    }
	
	
	@PreAuthorize("hasAnyRole('KUPAC','ADMINISTRATOR')")
    @GetMapping(value = "/Akcija/Prodavac/{id}")
    public ResponseEntity<List<ArtiklFrontendDTO>> artikliNaAkcijiZaProdavca(@PathVariable(name = "id") Long idAkcije){

		
		List<Akcija> akcije =  akcijeService.findAll();
		Akcija akcija = new Akcija();
        
		// pronalazenje akcije
        for (Akcija akcijaIter: akcije) {
        	if (akcijaIter.getIdAkcije() == idAkcije) {
        		akcija = akcijaIter;
        		break;
        	}
        }
		
        List<Artikl> artikli = artikliService.findAll();
        List<ArtiklFrontendDTO> artikliFrontendDTO = new ArrayList<ArtiklFrontendDTO>();
        // filtriranje artikala tako da se vracaju samo oni koji su na akciji
        for (Artikl artikl: artikli) {
			for (Long idArtiklaIzAkcije : akcija.getArtikli()) {
				if (artikl.getIdArtikla() == idArtiklaIzAkcije) {
					ArtiklFrontendDTO artiklFrontendDTO = new ArtiklFrontendDTO(artikl);
		        	artikliFrontendDTO.add(artiklFrontendDTO);
		        	break;
				}
			}
		        	
        }
        
        return new ResponseEntity<List<ArtiklFrontendDTO>>(artikliFrontendDTO,HttpStatus.OK);
    }
	
	@PreAuthorize("hasAnyRole('KUPAC')")
    @PostMapping(value = "/ArtikliIzStavki")
    public ResponseEntity<List<ArtiklFrontendDTO>> artikliIzStavki(@RequestBody List<Long> artikliIzStavkiIds){
		System.out.println("pogodak");
        List<Artikl> artikli = artikliService.findAll();
        List<ArtiklFrontendDTO> artikliFrontendDTO = new ArrayList<ArtiklFrontendDTO>();
        
        for (Artikl artikl: artikli) {
        	if (artikliIzStavkiIds.contains(artikl.getIdArtikla())){
	        	ArtiklFrontendDTO artiklFrontendDTO = new ArtiklFrontendDTO(artikl);
	        	artikliFrontendDTO.add(artiklFrontendDTO);
        	}
        }
        return new ResponseEntity<List<ArtiklFrontendDTO>>(artikliFrontendDTO,HttpStatus.OK);
    }

	@PreAuthorize("hasAnyRole('KUPAC','PRODAVAC')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<ArtiklFrontendDTO> findArtikl(@PathVariable(name = "id") Long id){

        Artikl artikl = artikliService.findOne(id);
        if(artikl == null){
            return new ResponseEntity<ArtiklFrontendDTO>(HttpStatus.NOT_FOUND);
        }
        ArtiklFrontendDTO artiklFrontendDTO = new ArtiklFrontendDTO(artikl);
        
        return new ResponseEntity<ArtiklFrontendDTO>(artiklFrontendDTO, HttpStatus.OK);
    }

	@PreAuthorize("hasAnyRole('PRODAVAC')")
    @PostMapping(consumes = "application/json")
    public ResponseEntity<ArtiklFrontendDTO> saveArtikl(HttpServletRequest request, @RequestBody ArtiklBackendDTO artiklInfo){
		
    	Artikl artikl = new Artikl();
    	
    	if (artiklInfo.getIdProdavca() != null && prodavacService.findOne(artiklInfo.getIdProdavca()) != null) {
    		artikl.setProdavac(prodavacService.findOne(artiklInfo.getIdProdavca()));
    	}
    	else {
    		return new ResponseEntity<ArtiklFrontendDTO>(HttpStatus.BAD_REQUEST);
    	}
    	artikl.setNaziv(artiklInfo.getNaziv());
    	artikl.setOpis(artiklInfo.getOpis());
    	artikl.setCena(artiklInfo.getCena());
    	artikl.setPutanjaSlike(artiklInfo.getPutanjaSlike());
    	
    	artikl = artikliService.save(artikl);
       	ArtiklFrontendDTO artiklFrontendDTO = new ArtiklFrontendDTO(artikl);
        return new ResponseEntity<ArtiklFrontendDTO>(artiklFrontendDTO, HttpStatus.CREATED);
    }

	@PreAuthorize("hasAnyRole('PRODAVAC')")
    @PutMapping(value = "/{id}", consumes = "application/json")
    public  ResponseEntity<ArtiklFrontendDTO> updateArtikl(HttpServletRequest request, @PathVariable(name = "id") Long id,@RequestBody ArtiklBackendDTO artiklInfo){
		
		Artikl artikl = artikliService.findOne(id);
		if(artikl == null){
            return new ResponseEntity<ArtiklFrontendDTO>(HttpStatus.BAD_REQUEST);
        }
		
		// provjera da korisnik kome pripada artikl ga izmenjuje
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		String token = httpServletRequest.getHeader("Authorization");
		if (token != null) {
			if(token.startsWith("Bearer ")) {
				token = token.substring(7);
			}
		}
		
		Long idKorisnika = tokenUtils.getIdFromToken(token);
		System.out.println(idKorisnika);
		if (idKorisnika != null) {			
			if (idKorisnika != artikl.getProdavac().getIdKorisnika()) {
				return new ResponseEntity<ArtiklFrontendDTO>(HttpStatus.FORBIDDEN);
			}
		}
		
		else {
			return new ResponseEntity<ArtiklFrontendDTO>(HttpStatus.UNAUTHORIZED);
		}
		
    	
        

        if (artiklInfo.getIdProdavca() != null && prodavacService.findOne(artiklInfo.getIdProdavca()) != null) {
    		artikl.setProdavac(prodavacService.findOne(artiklInfo.getIdProdavca()));
    	}
    	else {
    		return new ResponseEntity<ArtiklFrontendDTO>(HttpStatus.BAD_REQUEST);
    	}
        artikl.setNaziv(artiklInfo.getNaziv());
    	artikl.setOpis(artiklInfo.getOpis());
    	artikl.setCena(artiklInfo.getCena());
    	artikl.setPutanjaSlike(artiklInfo.getPutanjaSlike());
        
    	artikl = artikliService.save(artikl);
        ArtiklFrontendDTO artiklFrontendDTO = new ArtiklFrontendDTO(artikl);

        return new ResponseEntity<ArtiklFrontendDTO>(artiklFrontendDTO, HttpStatus.OK);
    }
	
	
	@PreAuthorize("hasAnyRole('PRODAVAC')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteArtikl(HttpServletRequest request, @PathVariable(name = "id") Long id){

        Artikl artikl = artikliService.findOne(id);
        if(artikl == null){
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        
        
        // provjera da korisnik kome pripada artikl ga izmenjuje
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		String token = httpServletRequest.getHeader("Authorization");
		if (token != null) {
			if(token.startsWith("Bearer ")) {
				token = token.substring(7);
			}
		}
		
		Long idKorisnika = tokenUtils.getIdFromToken(token);
		System.out.println(idKorisnika);
		if (idKorisnika != null) {			
			if (idKorisnika != artikl.getProdavac().getIdKorisnika()) {
				return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
			}
		}
		
		else {
			return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
		}

        
        artikliService.remove(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
}
