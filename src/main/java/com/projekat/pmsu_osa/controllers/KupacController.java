package com.projekat.pmsu_osa.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projekat.pmsu_osa.models.dto.AdministratorBackendDTO;
import com.projekat.pmsu_osa.models.dto.AdministratorFrontendDTO;
import com.projekat.pmsu_osa.models.dto.KupacBackendDTO;
import com.projekat.pmsu_osa.models.dto.KupacFrontendDTO;
import com.projekat.pmsu_osa.models.entity.Administrator;
import com.projekat.pmsu_osa.models.entity.Kupac;
import com.projekat.pmsu_osa.services.IAdministratorService;
import com.projekat.pmsu_osa.services.IKupacService;
import com.projekat.pmsu_osa.services.IPorudzbinaService;

@CrossOrigin
@RestController
@RequestMapping(value = "/Kupci")
public class KupacController {

	@Autowired
    private IKupacService kupacService;
    
	@Autowired
	private IPorudzbinaService porudzbinaService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

    @GetMapping
    public ResponseEntity<List<KupacFrontendDTO>> allKupci(){

        List<Kupac> kupci =  kupacService.findAll();
        List<KupacFrontendDTO> kupciFrontendDTO = new ArrayList<KupacFrontendDTO>();
        
        for (Kupac kupac: kupci) {
        	KupacFrontendDTO kupacFrontendDTO = new KupacFrontendDTO(kupac);
        	kupciFrontendDTO.add(kupacFrontendDTO);
        }
        return new ResponseEntity<List<KupacFrontendDTO>>(kupciFrontendDTO,HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<KupacFrontendDTO> findKupac(@PathVariable(name = "id") Long id){

        Kupac kupac = kupacService.findOne(id);
        if(kupac == null){
            return new ResponseEntity<KupacFrontendDTO>(HttpStatus.BAD_REQUEST);
        }
        KupacFrontendDTO kupacFrontendDTO = new KupacFrontendDTO(kupac);
        
        return new ResponseEntity<KupacFrontendDTO>(kupacFrontendDTO, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<KupacFrontendDTO> saveKupac(@RequestBody KupacBackendDTO kupacInfo){
        
    	Kupac kupac = new Kupac();
    	kupac.setIme(kupacInfo.getImeKorisnika());
    	kupac.setPrezime(kupacInfo.getPrezimeKorisnika());
    	kupac.setKorisnickoIme(kupacInfo.getKorisnickoIme());
    	kupac.setLozinka(kupacInfo.getLozinkaKorisnika());
    	kupac.setBlokiran(kupacInfo.isBlokiran());
    	kupac.setTipKorisnika(kupacInfo.getTipKorisnika());
    	kupac.setAdresa(kupacInfo.getAdresa());
    	/*if (kupacInfo.getListaPorudzbina() != null) {
	    	for (Long idPorudzbine : kupacInfo.getListaPorudzbina()) {
	    		kupac.getListaPorudzbina().add(porudzbinaService.findOne(idPorudzbine));
	    	}
    	}*/
    	
    	kupac = kupacService.save(kupac);
    	KupacFrontendDTO kupacFrontendDTO = new KupacFrontendDTO(kupac);
        return new ResponseEntity<KupacFrontendDTO>(kupacFrontendDTO, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    public  ResponseEntity<KupacFrontendDTO> updateKupac(@PathVariable(name = "id") Long id,@RequestBody KupacBackendDTO kupacInfo){
    	Kupac kupac = kupacService.findOne(id);
        if(kupac == null){
            return new ResponseEntity<KupacFrontendDTO>(HttpStatus.BAD_REQUEST);
        }

        kupac.setIme(kupacInfo.getImeKorisnika());
    	kupac.setPrezime(kupacInfo.getPrezimeKorisnika());
    	kupac.setKorisnickoIme(kupacInfo.getKorisnickoIme());
    	if (kupacInfo.getLozinkaKorisnika() != null) {
    		kupac.setLozinka(passwordEncoder.encode(kupacInfo.getLozinkaKorisnika()));
    	}
    	
    	kupac.setBlokiran(kupacInfo.isBlokiran());
    	//kupac.setTipKorisnika(kupacInfo.getTipKorisnika());
    	kupac.setAdresa(kupacInfo.getAdresa());
    	/*if (kupacInfo.getListaPorudzbina() != null) {
	    	for (Long idPorudzbine : kupacInfo.getListaPorudzbina()) {
	    		kupac.getListaPorudzbina().add(porudzbinaService.findOne(idPorudzbine));
	    	}
    	}*/
    	
    	kupac = kupacService.save(kupac);
    	KupacFrontendDTO kupacFrontendDTO = new KupacFrontendDTO(kupac);
        

        return new ResponseEntity<KupacFrontendDTO>(kupacFrontendDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteKupac(@PathVariable(name = "id") Long id){

    	Kupac kupac = kupacService.findOne(id);
        if(kupac == null){
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        kupacService.remove(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
}
