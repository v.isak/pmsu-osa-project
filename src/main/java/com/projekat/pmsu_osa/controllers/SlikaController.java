package com.projekat.pmsu_osa.controllers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.projekat.pmsu_osa.enums.TipKorisnika;
import com.projekat.pmsu_osa.models.dto.AdministratorFrontendDTO;
import com.projekat.pmsu_osa.models.dto.ArtiklFrontendDTO;
import com.projekat.pmsu_osa.models.dto.KorisnikFrontendDTO;
import com.projekat.pmsu_osa.models.dto.PrijavaDTO;
import com.projekat.pmsu_osa.models.dto.RegistracijaDTO;
import com.projekat.pmsu_osa.models.dto.SlikaDTO;
import com.projekat.pmsu_osa.models.entity.Administrator;
import com.projekat.pmsu_osa.models.entity.Artikl;
import com.projekat.pmsu_osa.models.entity.Korisnik;
import com.projekat.pmsu_osa.models.entity.Kupac;
import com.projekat.pmsu_osa.models.entity.Prodavac;
import com.projekat.pmsu_osa.security.TokenUtils;
import com.projekat.pmsu_osa.services.IAdministratorService;
import com.projekat.pmsu_osa.services.IKupacService;
import com.projekat.pmsu_osa.services.IProdavacService;
import com.projekat.pmsu_osa.services.implementation.UserDetailsServiceImpl;


@CrossOrigin
@RestController
@RequestMapping(value="/Slike")
public class SlikaController {

	@Autowired
	ServletContext context;
	
	private Long id = 1L;
	
	@PreAuthorize("hasAnyRole('PRODAVAC')")
	@PostMapping(consumes = "application/json", produces = "application/json", value="/Sacuvaj")
	public ResponseEntity<SlikaDTO> createSlika(@RequestBody SlikaDTO slikaValue) throws IOException{
		// dobijanje relativne putanje do slike foldera
		String slikePath = new File("slike").getAbsolutePath();
		System.out.println(slikePath);
		
		File slika = new File(slikePath, String.valueOf(id));
		while (slika.exists()){
			id += 1;
			// kreiranja fajla sa imenom enkodirane slike
			slika = new File(slikePath, String.valueOf(id));
		}
		slika.createNewFile();
		
	    BufferedWriter writer = new BufferedWriter(new FileWriter(slika.getAbsolutePath(), false));
	    writer.append(slikaValue.getSlikaString());
	    writer.close();
		
	    SlikaDTO slikaPath = new SlikaDTO();
	    slikaPath.setSlikaRelativePath("slike\\"+id);
	    System.out.println(slikaPath.getSlikaRelativePath());
	    return new ResponseEntity<SlikaDTO>(slikaPath, HttpStatus.CREATED);
	}
	
	
	@PreAuthorize("hasAnyRole('PRODAVAC')")
	@PostMapping(consumes = "application/json", produces = "application/json", value="/Izmeni")
	public ResponseEntity<SlikaDTO> updateSlika(@RequestBody SlikaDTO slikaValue) throws IOException{
		
		if (slikaValue.getSlikaString().length() == 0) {
			SlikaDTO slikaPath = new SlikaDTO();
		    slikaPath.setSlikaRelativePath(slikaValue.getSlikaRelativePath());
		    return new ResponseEntity<SlikaDTO>(slikaPath,HttpStatus.OK);
		}
		
		// dobijanje relativne putanje do slike foldera
		File slika = new File(slikaValue.getSlikaRelativePath());

		SlikaDTO slikaPath = new SlikaDTO();
		
		if (!slikaValue.getSlikaRelativePath().startsWith("slike\\")) {
			id += 1;
			slika = new File("slike\\"+String.valueOf(id));
			// provjera kada se resetuje aplikacija tj. id pa
			// moramo ponovo da dodjemo do novog id-a
			while (slika.exists()){
				id += 1;
				// kreiranja fajla sa imenom enkodirane slike
				slika = new File("slike\\"+String.valueOf(id));
			}
			slika.createNewFile();
			
			slikaPath.setSlikaRelativePath("slike\\"+id);
		}
		else {
			// ako nekim slucajem ne postoji fajl odnosno ako je obrisan
			// sa backend-a ali ne i iz baze
			if (!slika.exists()) {
				slika.createNewFile();
			}
			slikaPath.setSlikaRelativePath(slikaValue.getSlikaRelativePath());
		}
		
		System.out.println(slika.getAbsolutePath());
		BufferedWriter writer = new BufferedWriter(new FileWriter(slika.getAbsolutePath(), false));
	    writer.append(slikaValue.getSlikaString());
	    writer.close();
		
	    
	    
	    
	    return new ResponseEntity<SlikaDTO>(slikaPath,HttpStatus.OK);
	}
	
	
	
	@PreAuthorize("hasAnyRole('KUPAC','PRODAVAC','ADMINISTRATOR','ANONYMOUS')")
	@PostMapping(consumes = "application/json", produces = "application/json", value="/Preuzmi")
	public ResponseEntity<SlikaDTO> loadSlika(@RequestBody SlikaDTO slikaValue) throws IOException{

		// dobijanje absolutne putanje slike preko relativne
		String slikaPath = new File(slikaValue.getSlikaRelativePath()).getAbsolutePath();
		
		String slikaString = new String(Files.readAllBytes(Paths.get(slikaPath)), StandardCharsets.UTF_8);
		
		SlikaDTO slikaZaFront = new SlikaDTO();
		slikaZaFront.setSlikaString(slikaString);
	    return new ResponseEntity<SlikaDTO>(slikaZaFront, HttpStatus.OK);
	}
	
	
}