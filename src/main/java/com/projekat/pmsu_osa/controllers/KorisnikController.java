package com.projekat.pmsu_osa.controllers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projekat.pmsu_osa.enums.TipKorisnika;
import com.projekat.pmsu_osa.models.dto.AdministratorFrontendDTO;
import com.projekat.pmsu_osa.models.dto.ArtiklFrontendDTO;
import com.projekat.pmsu_osa.models.dto.KorisnikFrontendDTO;
import com.projekat.pmsu_osa.models.dto.PrijavaDTO;
import com.projekat.pmsu_osa.models.dto.RegistracijaDTO;
import com.projekat.pmsu_osa.models.entity.Administrator;
import com.projekat.pmsu_osa.models.entity.Artikl;
import com.projekat.pmsu_osa.models.entity.Korisnik;
import com.projekat.pmsu_osa.models.entity.Kupac;
import com.projekat.pmsu_osa.models.entity.Prodavac;
import com.projekat.pmsu_osa.security.TokenUtils;
import com.projekat.pmsu_osa.services.IAdministratorService;
import com.projekat.pmsu_osa.services.IKupacService;
import com.projekat.pmsu_osa.services.IProdavacService;
import com.projekat.pmsu_osa.services.implementation.UserDetailsServiceImpl;

@CrossOrigin
@RestController
@RequestMapping(value="/")
public class KorisnikController {
	
	@Autowired
    IAdministratorService adminService;
	
	@Autowired
    IProdavacService prodavacService;
	
	@Autowired
    IKupacService kupacService;
	
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	TokenUtils tokenUtils;
	
	@Autowired
	UserDetailsServiceImpl userDetailsService;
	
	
	@PreAuthorize("hasAnyRole('ADMINISTRATOR')")
    @GetMapping("/Korisnici")
    public ResponseEntity<List<KorisnikFrontendDTO>> allKorisnici(){
		System.out.println("POGODJENI KORISNICI");
		List<Korisnik> korisnici = new ArrayList<Korisnik>();
		for (Administrator admin : adminService.findAll()) {
			korisnici.add((Korisnik) admin);
		}
		for (Prodavac prodavac : prodavacService.findAll()) {
			korisnici.add((Korisnik) prodavac);
		}
		for (Kupac kupac : kupacService.findAll()) {
			korisnici.add((Korisnik) kupac);
		}

        List<KorisnikFrontendDTO> korisniciFrontendDTO = new ArrayList<KorisnikFrontendDTO>();
        
        for (Korisnik korisnik: korisnici) {
        	KorisnikFrontendDTO korisnikFrontendDTO = new KorisnikFrontendDTO(korisnik);
        	korisniciFrontendDTO.add(korisnikFrontendDTO);
        }
        return new ResponseEntity<List<KorisnikFrontendDTO>>(korisniciFrontendDTO,HttpStatus.OK);
    }
	
	
	
	@PreAuthorize("hasAnyRole('ADMINISTRATOR')")
	@GetMapping(value = "/Blokiraj/{id}")
    public ResponseEntity<Void> blokirajKorisnika(@PathVariable(name = "id") long idKorisnika){
		
		for (Administrator admin : adminService.findAll()) {
			if (admin.getIdKorisnika() == idKorisnika) {
				admin.setBlokiran(true);
				adminService.save(admin);
			}
		}
		for (Prodavac prodavac : prodavacService.findAll()) {
			if (prodavac.getIdKorisnika() == idKorisnika) {
				prodavac.setBlokiran(true);
				prodavacService.save(prodavac);
			}
		}
		for (Kupac kupac : kupacService.findAll()) {
			if (kupac.getIdKorisnika() == idKorisnika) {
				kupac.setBlokiran(true);
				kupacService.save(kupac);
			}
		}

        return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
	@PreAuthorize("hasAnyRole('ADMINISTRATOR')")
    @GetMapping(value = "/Odblokiraj/{id}")
    public ResponseEntity<Void> odblokirajKorisnika(@PathVariable(name = "id") long idKorisnika){

		for (Administrator admin : adminService.findAll()) {
			if (admin.getIdKorisnika() == idKorisnika) {
				admin.setBlokiran(false);
				adminService.save(admin);
			}
		}
		for (Prodavac prodavac : prodavacService.findAll()) {
			if (prodavac.getIdKorisnika() == idKorisnika) {
				prodavac.setBlokiran(false);
				prodavacService.save(prodavac);
			}
		}
		for (Kupac kupac : kupacService.findAll()) {
			if (kupac.getIdKorisnika() == idKorisnika) {
				kupac.setBlokiran(false);
				kupacService.save(kupac);
			}
		}

        return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
	@PostMapping(consumes = "application/json", value="/Prijava")
	public ResponseEntity<String> login(@RequestBody PrijavaDTO prijavaDTO){
		System.out.println(prijavaDTO.getUsername() + prijavaDTO.getPassword());
		UsernamePasswordAuthenticationToken authenticationToken = 
				new UsernamePasswordAuthenticationToken(prijavaDTO.getUsername(), prijavaDTO.getPassword());
		
		
		Authentication authentication = authenticationManager.authenticate(authenticationToken);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		System.out.println(SecurityContextHolder.getContext().getAuthentication());
		try {
			// kroz loadUserByUsername se provjerava i da li je korisnik blokiran,
			// ako jeste izbacice gresku i nece se vratit korisniku token
			UserDetails userDetails = userDetailsService.loadUserByUsername(prijavaDTO.getUsername());
			return ResponseEntity.ok(tokenUtils.generateToken(userDetails,userDetailsService));
		}
		
		// neuspesna prijava
		catch(UsernameNotFoundException e) {
			return ResponseEntity.notFound().build();
		}

	}

	@PostMapping(value="/Odjava")
	public ResponseEntity<Void> logout(){
		SecurityContextHolder.getContext().setAuthentication(null);
	
		return ResponseEntity.ok(null);

	}

	@PostMapping(consumes = "application/json", value="/Registracija")
	public ResponseEntity<Void> registration(@RequestBody RegistracijaDTO registracijaDTO){
		
		if (registracijaDTO.getTipKorisnika() == TipKorisnika.KUPAC) {
			Kupac kupac = new Kupac();
			kupac.setTipKorisnika(TipKorisnika.KUPAC);
			kupac.setIme(registracijaDTO.getIme());
			kupac.setPrezime(registracijaDTO.getPrezime());
			kupac.setKorisnickoIme(registracijaDTO.getUsername());
			kupac.setLozinka(registracijaDTO.getPassword());
			kupac.setAdresa(registracijaDTO.getAdresa());
			
			kupac = kupacService.save(kupac);
			
	        return new ResponseEntity<Void>(HttpStatus.CREATED);

		}
		else if (registracijaDTO.getTipKorisnika() == TipKorisnika.PRODAVAC) {
			Prodavac prodavac = new Prodavac();
			prodavac.setTipKorisnika(TipKorisnika.PRODAVAC);
			prodavac.setPoslujeOd(LocalDateTime.now());
			prodavac.setIme(registracijaDTO.getIme());
			prodavac.setPrezime(registracijaDTO.getPrezime());
			prodavac.setKorisnickoIme(registracijaDTO.getUsername());
			prodavac.setLozinka(registracijaDTO.getPassword());
			prodavac.setAdresa(registracijaDTO.getAdresa());
			prodavac.setEmail(registracijaDTO.getEmail());
			prodavac.setNaziv(registracijaDTO.getNaziv());
			prodavac.setPutanjaSlike("");
			
			prodavac = prodavacService.save(prodavac);
			
	        return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		else {
	        return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
	}
	
	
}