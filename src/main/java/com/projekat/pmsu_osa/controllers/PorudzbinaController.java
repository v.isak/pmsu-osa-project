package com.projekat.pmsu_osa.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projekat.pmsu_osa.models.dto.AdministratorBackendDTO;
import com.projekat.pmsu_osa.models.dto.AdministratorFrontendDTO;
import com.projekat.pmsu_osa.models.dto.ArtiklFrontendDTO;
import com.projekat.pmsu_osa.models.dto.PorudzbinaBackendDTO;
import com.projekat.pmsu_osa.models.dto.PorudzbinaFrontendDTO;
import com.projekat.pmsu_osa.models.entity.Administrator;
import com.projekat.pmsu_osa.models.entity.Artikl;
import com.projekat.pmsu_osa.models.entity.Porudzbina;
import com.projekat.pmsu_osa.models.entity.Stavka;
import com.projekat.pmsu_osa.security.TokenUtils;
import com.projekat.pmsu_osa.services.IAdministratorService;
import com.projekat.pmsu_osa.services.IArtiklService;
import com.projekat.pmsu_osa.services.IKupacService;
import com.projekat.pmsu_osa.services.IPorudzbinaService;
import com.projekat.pmsu_osa.services.IProdavacService;
import com.projekat.pmsu_osa.services.IStavkaService;

@CrossOrigin
@RestController
@RequestMapping(value = "/Porudzbine")
public class PorudzbinaController {
	
	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
    private IArtiklService artikliService;
	
	@Autowired
    private IPorudzbinaService porudzbineService;
	
	@Autowired
	private IKupacService kupacService;
	
	@Autowired
	private IStavkaService stavkaService;
    

    @PreAuthorize("hasAnyRole('ADMINISTRATOR')")
    @GetMapping
    public ResponseEntity<List<PorudzbinaFrontendDTO>> allPorudzbine(){

        List<Porudzbina> porudzbine =  porudzbineService.findAll();
        List<PorudzbinaFrontendDTO> porudzbineFrontendDTO = new ArrayList<PorudzbinaFrontendDTO>();
        
        for (Porudzbina porudzbina: porudzbine) {
        	PorudzbinaFrontendDTO porudzbinaFrontendDTO = new PorudzbinaFrontendDTO(porudzbina);
        	System.out.println(porudzbinaFrontendDTO.getSatnica());
        	porudzbineFrontendDTO.add(porudzbinaFrontendDTO);
        }
        return new ResponseEntity<List<PorudzbinaFrontendDTO>>(porudzbineFrontendDTO,HttpStatus.OK);
    }
    
    @PreAuthorize("hasAnyRole('KUPAC')")
    @GetMapping(value = "/Kupac/{id}")
    public ResponseEntity<List<PorudzbinaFrontendDTO>> porudzbineZaKupca(HttpServletRequest request, @PathVariable(name = "id") Long idKupca){
    	
    	// provjera da korisnik pristupa samo svojim porudzbinama
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		String token = httpServletRequest.getHeader("Authorization");
		if (token != null) {
			if(token.startsWith("Bearer ")) {
				token = token.substring(7);
			}
		}
		
		Long idKorisnika = tokenUtils.getIdFromToken(token);
		System.out.println(idKorisnika);
		if (idKorisnika != null) {			
			if (idKorisnika != idKupca) {
				return new ResponseEntity<List<PorudzbinaFrontendDTO>>(HttpStatus.FORBIDDEN);
			}
		}
		
		else {
			return new ResponseEntity<List<PorudzbinaFrontendDTO>>(HttpStatus.UNAUTHORIZED);
		}
		//

        List<Porudzbina> porudzbine =  porudzbineService.findAll();
        List<PorudzbinaFrontendDTO> porudzbineFrontendDTO = new ArrayList<PorudzbinaFrontendDTO>();
        
        for (Porudzbina porudzbina: porudzbine) {
        	if (porudzbina.getKupac().getIdKorisnika() == idKupca) {
	        	PorudzbinaFrontendDTO porudzbinaFrontendDTO = new PorudzbinaFrontendDTO(porudzbina);
	        	System.out.println(porudzbinaFrontendDTO.getSatnica());
	        	porudzbineFrontendDTO.add(porudzbinaFrontendDTO);
        	}
        }
        return new ResponseEntity<List<PorudzbinaFrontendDTO>>(porudzbineFrontendDTO,HttpStatus.OK);
    }
    
    
    
    @PreAuthorize("hasAnyRole('KUPAC','PRODAVAC','ANONYMOUS')")
    @GetMapping(value = "/Komentari/Prodavac/{id}")
    public ResponseEntity<List<PorudzbinaFrontendDTO>> komentariZaProdavca(@PathVariable(name = "id") Long idProdavca){

    	List<Artikl> allArtikli = artikliService.findAll();
    	List<Artikl> artikliZaProdavca = new ArrayList<Artikl>();
    	
    	// filtriranje artikla od prodavca
    	for (Artikl artikl : allArtikli) {
    		if (artikl.getProdavac().getIdKorisnika() == idProdavca) {
    			artikliZaProdavca.add(artikl);
    		}
    	}
    	
    	// filtriranje stavki od prodavca uz pomoc dobijenih artikala
    	List<Stavka> allStavke = stavkaService.findAll();
    	List<Stavka> stavkeOdProdavca = new ArrayList<Stavka>();
    	
    	for (Stavka stavka : allStavke) {
    		for (Artikl artikl : artikliZaProdavca) {
    			if (stavka.getArtikal().getIdArtikla() == artikl.getIdArtikla()) {
    				stavkeOdProdavca.add(stavka);
    				break;
    			}
    		}
    	}
    	
    	// filtriranje porudzbina na osnovu dobijenih prethodnih podataka
    	// da bi kasnije iz njih izvukao komentare
    	List<Porudzbina> allPorudzbine = porudzbineService.findAll();
    	List<Porudzbina> komentariZaProdavca = new ArrayList<Porudzbina>();
    	
    	for (Porudzbina porudzbina : allPorudzbine) {
    		for (Stavka stavka : stavkeOdProdavca) {
    			if (porudzbina.getIdPorudzbine() == stavka.getPorudzbina().getIdPorudzbine()) {
    				komentariZaProdavca.add(porudzbina);
    				break;
    			}
    		}
    	}
    	
        List<PorudzbinaFrontendDTO> porudzbineFrontendDTO = new ArrayList<PorudzbinaFrontendDTO>();
        for (Porudzbina porudzbina: komentariZaProdavca) {
        	// zadnje filtriranje da se uklone porudzbine(komentari) koji nisu ocenjeni
        	if (porudzbina.getOcena() != 0) {
	        	PorudzbinaFrontendDTO porudzbinaFrontendDTO = new PorudzbinaFrontendDTO(porudzbina);
	        	System.out.println(porudzbinaFrontendDTO.getSatnica());
	        	porudzbineFrontendDTO.add(porudzbinaFrontendDTO);
        	}
        }
        System.out.println(porudzbineFrontendDTO);
        return new ResponseEntity<List<PorudzbinaFrontendDTO>>(porudzbineFrontendDTO,HttpStatus.OK);
    }
    
    
    @PreAuthorize("hasAnyRole('KUPAC','PRODAVAC')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<PorudzbinaFrontendDTO> findPorudzbina(@PathVariable(name = "id") Long id){

    	Porudzbina porudzbina = porudzbineService.findOne(id);
        if(porudzbina == null){
            return new ResponseEntity<PorudzbinaFrontendDTO>(HttpStatus.BAD_REQUEST);
        }
        PorudzbinaFrontendDTO porudzbinaFrontendDTO = new PorudzbinaFrontendDTO(porudzbina);
        
        return new ResponseEntity<PorudzbinaFrontendDTO>(porudzbinaFrontendDTO, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('KUPAC')")
    @PostMapping(consumes = "application/json")
    public ResponseEntity<PorudzbinaFrontendDTO> savePorudzbina(@RequestBody PorudzbinaBackendDTO porudzbinaInfo){
        
    	Porudzbina porudzbina = new Porudzbina();
    	
    	if (porudzbinaInfo.getIdKupca()!= null && kupacService.findOne(porudzbinaInfo.getIdKupca()) != null) {
    		porudzbina.setKupac(kupacService.findOne(porudzbinaInfo.getIdKupca()));
    	}
    	else {
    		return new ResponseEntity<PorudzbinaFrontendDTO>(HttpStatus.BAD_REQUEST);
    	}
    	porudzbina.setSatnica(porudzbinaInfo.getSatnica());
    	porudzbina.setDostavljeno(porudzbinaInfo.isDostavljeno());
    	porudzbina.setOcena(porudzbinaInfo.getOcena());
    	porudzbina.setKomentar(porudzbinaInfo.getKomentar());
    	porudzbina.setAnonimanKomentar(porudzbinaInfo.isAnonimanKomentar());
    	porudzbina.setArhiviraniKomentar(porudzbinaInfo.isArhiviraniKomentar());
    	porudzbina.setCenaPorudzbine(porudzbinaInfo.getCenaPorudzbine());
    	
    	porudzbina = porudzbineService.save(porudzbina);
    	PorudzbinaFrontendDTO porudzbinaFrontendDTO = new PorudzbinaFrontendDTO(porudzbina);
    	
        return new ResponseEntity<PorudzbinaFrontendDTO>(porudzbinaFrontendDTO, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyRole('KUPAC','ADMINISTRATOR')")
    @PutMapping(value = "/{id}", consumes = "application/json")
    public  ResponseEntity<PorudzbinaFrontendDTO> updatePorudzbina(@PathVariable(name = "id") Long id,@RequestBody PorudzbinaBackendDTO porudzbinaInfo){
    	Porudzbina porudzbina = porudzbineService.findOne(id);
        if(porudzbina == null){
            return new ResponseEntity<PorudzbinaFrontendDTO>(HttpStatus.BAD_REQUEST);
        }

        if (porudzbinaInfo.getIdKupca()!= null && kupacService.findOne(porudzbinaInfo.getIdKupca()) != null) {
    		porudzbina.setKupac(kupacService.findOne(porudzbinaInfo.getIdKupca()));
    	}
    	else {
    		return new ResponseEntity<PorudzbinaFrontendDTO>(HttpStatus.BAD_REQUEST);
    	}
    	porudzbina.setSatnica(porudzbinaInfo.getSatnica());
    	porudzbina.setDostavljeno(porudzbinaInfo.isDostavljeno());
    	porudzbina.setOcena(porudzbinaInfo.getOcena());
    	porudzbina.setKomentar(porudzbinaInfo.getKomentar());
    	porudzbina.setAnonimanKomentar(porudzbinaInfo.isAnonimanKomentar());
    	porudzbina.setArhiviraniKomentar(porudzbinaInfo.isArhiviraniKomentar());
    	porudzbina.setCenaPorudzbine(porudzbinaInfo.getCenaPorudzbine());
    	
    	porudzbina = porudzbineService.save(porudzbina);
    	PorudzbinaFrontendDTO porudzbinaFrontendDTO = new PorudzbinaFrontendDTO(porudzbina);
    	
        return new ResponseEntity<PorudzbinaFrontendDTO>(porudzbinaFrontendDTO, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ADMINISTRATOR')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deletePorudzbina(@PathVariable(name = "id") Long id){

    	Porudzbina porudzbina = porudzbineService.findOne(id);
        if(porudzbina == null){
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        porudzbineService.remove(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
}
