package com.projekat.pmsu_osa.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projekat.pmsu_osa.models.dto.AdministratorBackendDTO;
import com.projekat.pmsu_osa.models.dto.AdministratorFrontendDTO;
import com.projekat.pmsu_osa.models.dto.ArtiklFrontendDTO;
import com.projekat.pmsu_osa.models.dto.StavkaBackendDTO;
import com.projekat.pmsu_osa.models.dto.StavkaFrontendDTO;
import com.projekat.pmsu_osa.models.entity.Administrator;
import com.projekat.pmsu_osa.models.entity.Stavka;
import com.projekat.pmsu_osa.security.TokenUtils;
import com.projekat.pmsu_osa.services.IAdministratorService;
import com.projekat.pmsu_osa.services.IArtiklService;
import com.projekat.pmsu_osa.services.IPorudzbinaService;
import com.projekat.pmsu_osa.services.IStavkaService;

@CrossOrigin
@RestController
@RequestMapping(value = "/Stavke")
public class StavkaController {

	@Autowired
	private TokenUtils tokenUtils;
	
	@Autowired
    private IStavkaService stavkeService;
    
	@Autowired
	private IArtiklService artiklService;
	
	@Autowired
	private IPorudzbinaService porudzbinaService;

    @GetMapping
    public ResponseEntity<List<StavkaFrontendDTO>> allStavke(){

        List<Stavka> stavke =  stavkeService.findAll();
        List<StavkaFrontendDTO> stavkeFrontendDTO = new ArrayList<StavkaFrontendDTO>();
        
        for (Stavka stavka: stavke) {
        	StavkaFrontendDTO stavkaFrontendDTO = new StavkaFrontendDTO(stavka);
        	stavkeFrontendDTO.add(stavkaFrontendDTO);
        }
        return new ResponseEntity<List<StavkaFrontendDTO>>(stavkeFrontendDTO,HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('KUPAC','ADMINISTRATOR','PRODAVAC')")
    @GetMapping(value = "/Porudzbina/{id}")
    public ResponseEntity<List<StavkaFrontendDTO>> stavkeZaPorudzbinu(HttpServletRequest request, @PathVariable(name = "id") Long idPorudzbine){

    	if (idPorudzbine == null) {
			return new ResponseEntity<List<StavkaFrontendDTO>>(HttpStatus.BAD_REQUEST);
    	}
    	
    	// provjera da korisnik koji trazi stavke za porudzbinu jeste
    	// onaj koji je kreirao
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		String token = httpServletRequest.getHeader("Authorization");
		if (token != null) {
			if(token.startsWith("Bearer ")) {
				token = token.substring(7);
			}
		}
		
		Long idKorisnika = tokenUtils.getIdFromToken(token);
		System.out.println(idKorisnika);
		if (idKorisnika != null) {			
			if (idKorisnika != porudzbinaService.findOne(idPorudzbine).getKupac().getIdKorisnika()) {
				return new ResponseEntity<List<StavkaFrontendDTO>>(HttpStatus.FORBIDDEN);
			}
		}
		
		else {
			return new ResponseEntity<List<StavkaFrontendDTO>>(HttpStatus.UNAUTHORIZED);
		}
    	
    	
        List<Stavka> stavke =  stavkeService.findAll();
        List<StavkaFrontendDTO> stavkeFrontendDTO = new ArrayList<StavkaFrontendDTO>();
        
        for (Stavka stavka: stavke) {
        	if (stavka.getPorudzbina().getIdPorudzbine() == idPorudzbine) {
	        	StavkaFrontendDTO stavkaFrontendDTO = new StavkaFrontendDTO(stavka);
	        	stavkeFrontendDTO.add(stavkaFrontendDTO);
        	}
        }
        return new ResponseEntity<List<StavkaFrontendDTO>>(stavkeFrontendDTO,HttpStatus.OK);
    }

    
    @GetMapping(value = "/{id}")
    public ResponseEntity<StavkaFrontendDTO> findStavka(@PathVariable(name = "id") Long id){

        Stavka stavka = stavkeService.findOne(id);
        if(stavka == null){
            return new ResponseEntity<StavkaFrontendDTO>(HttpStatus.BAD_REQUEST);
        }
        StavkaFrontendDTO stavkaFrontendDTO = new StavkaFrontendDTO(stavka);
        
        return new ResponseEntity<StavkaFrontendDTO>(stavkaFrontendDTO, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('KUPAC','ADMINISTRATOR')")
    @PostMapping(consumes = "application/json")
    public ResponseEntity<StavkaFrontendDTO> saveStavka(@RequestBody StavkaBackendDTO stavkaInfo){
        
    	Stavka stavka = new Stavka();
    	
    	if (stavkaInfo.getIdArtikla() != null && artiklService.findOne(stavkaInfo.getIdArtikla()) != null) {
    		stavka.setArtikal(artiklService.findOne(stavkaInfo.getIdArtikla()));
    	}
    	if (stavkaInfo.getIdPorudzbine() != null && porudzbinaService.findOne(stavkaInfo.getIdPorudzbine()) != null) {
    		stavka.setPorudzbina(porudzbinaService.findOne(stavkaInfo.getIdPorudzbine()));
    	}
    	stavka.setKolicina(stavkaInfo.getKolicina());
    	
    	stavka = stavkeService.save(stavka);
    	StavkaFrontendDTO stavkaFrontendDTO = new StavkaFrontendDTO(stavka);
    	
        return new ResponseEntity<StavkaFrontendDTO>(stavkaFrontendDTO, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyRole('ADMINISTRATOR')")
    @PutMapping(value = "/{id}", consumes = "application/json")
    public  ResponseEntity<StavkaFrontendDTO> updateStavka(@PathVariable(name = "id") Long id,@RequestBody StavkaBackendDTO stavkaInfo){
    	Stavka stavka = stavkeService.findOne(id);
        if(stavka == null){
            return new ResponseEntity<StavkaFrontendDTO>(HttpStatus.BAD_REQUEST);
        }
        
        if (stavkaInfo.getIdArtikla() != null && artiklService.findOne(stavkaInfo.getIdArtikla()) != null) {
    		stavka.setArtikal(artiklService.findOne(stavkaInfo.getIdArtikla()));
    	}
    	if (stavkaInfo.getIdPorudzbine() != null && porudzbinaService.findOne(stavkaInfo.getIdPorudzbine()) != null) {
    		stavka.setPorudzbina(porudzbinaService.findOne(stavkaInfo.getIdPorudzbine()));
    	}
    	stavka.setKolicina(stavkaInfo.getKolicina());
    	
    	stavka = stavkeService.save(stavka);
    	StavkaFrontendDTO stavkaFrontendDTO = new StavkaFrontendDTO(stavka);
    	
        return new ResponseEntity<StavkaFrontendDTO>(stavkaFrontendDTO, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ADMINISTRATOR')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteAdministrator(@PathVariable(name = "id") Long id){

    	Stavka stavka = stavkeService.findOne(id);
        if(stavka == null){
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        stavkeService.remove(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
}
