package com.projekat.pmsu_osa.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projekat.pmsu_osa.models.dto.AdministratorBackendDTO;
import com.projekat.pmsu_osa.models.dto.AdministratorFrontendDTO;
import com.projekat.pmsu_osa.models.entity.Administrator;
import com.projekat.pmsu_osa.services.IAdministratorService;


@CrossOrigin
@RestController
@RequestMapping(value = "/Admini")
public class AdministratorController {

	@Autowired
    private IAdministratorService administratorService;
    
	@Autowired
	private PasswordEncoder passwordEncoder;

    @GetMapping
    public ResponseEntity<List<AdministratorFrontendDTO>> allAdministrators(){

        List<Administrator> administrators =  administratorService.findAll();
        List<AdministratorFrontendDTO> adminiFrontendDTO = new ArrayList<AdministratorFrontendDTO>();
        
        for (Administrator admin: administrators) {
        	AdministratorFrontendDTO adminFrontendDTO = new AdministratorFrontendDTO(admin);
        	adminiFrontendDTO.add(adminFrontendDTO);
        }
        return new ResponseEntity<List<AdministratorFrontendDTO>>(adminiFrontendDTO,HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AdministratorFrontendDTO> findAdministrator(@PathVariable(name = "id") Long id){

        Administrator admin = administratorService.findOne(id);
        if(admin == null){
            return new ResponseEntity<AdministratorFrontendDTO>(HttpStatus.BAD_REQUEST);
        }
        AdministratorFrontendDTO adminFrontDTO = new AdministratorFrontendDTO(admin);
        
        return new ResponseEntity<AdministratorFrontendDTO>(adminFrontDTO, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<AdministratorFrontendDTO> saveAdministrator(@RequestBody AdministratorBackendDTO adminInfo){
        
    	Administrator admin = new Administrator();
    	admin.setIme(adminInfo.getImeKorisnika());
    	admin.setPrezime(adminInfo.getPrezimeKorisnika());
    	admin.setKorisnickoIme(adminInfo.getKorisnickoIme());
    	admin.setLozinka(adminInfo.getLozinkaKorisnika());
    	admin.setBlokiran(adminInfo.isBlokiran());
    	admin.setTipKorisnika(adminInfo.getTipKorisnika());
    	
    	admin = administratorService.save(admin);
    	AdministratorFrontendDTO administratorFrontendDTO = new AdministratorFrontendDTO(admin);
    	
        return new ResponseEntity<AdministratorFrontendDTO>(administratorFrontendDTO, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    public  ResponseEntity<AdministratorFrontendDTO> updateAdministrator(@PathVariable(name = "id") Long id,@RequestBody AdministratorBackendDTO adminInfo){
        Administrator admin = administratorService.findOne(id);
        if(admin == null){
            return new ResponseEntity<AdministratorFrontendDTO>(HttpStatus.BAD_REQUEST);
        }

        admin.setIme(adminInfo.getImeKorisnika());
    	admin.setPrezime(adminInfo.getPrezimeKorisnika());
    	admin.setKorisnickoIme(adminInfo.getKorisnickoIme());
    	if (adminInfo.getLozinkaKorisnika() != null) {
    		admin.setLozinka(passwordEncoder.encode(adminInfo.getLozinkaKorisnika()));
    	}
    	
    	admin.setBlokiran(adminInfo.isBlokiran());
    	//admin.setTipKorisnika(adminInfo.getTipKorisnika());

        admin = administratorService.save(admin);
    	AdministratorFrontendDTO administratorFrontendDTO = new AdministratorFrontendDTO(admin);


        return new ResponseEntity<AdministratorFrontendDTO>(administratorFrontendDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteAdministrator(@PathVariable(name = "id") Long id){

        Administrator administrator = administratorService.findOne(id);
        if(administrator == null){
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        administratorService.remove(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
}
