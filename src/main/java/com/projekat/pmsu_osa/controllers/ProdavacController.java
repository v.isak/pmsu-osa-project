package com.projekat.pmsu_osa.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projekat.pmsu_osa.models.dto.AdministratorBackendDTO;
import com.projekat.pmsu_osa.models.dto.AdministratorFrontendDTO;
import com.projekat.pmsu_osa.models.dto.ProdavacBackendDTO;
import com.projekat.pmsu_osa.models.dto.ProdavacFrontendDTO;
import com.projekat.pmsu_osa.models.entity.Administrator;
import com.projekat.pmsu_osa.models.entity.Porudzbina;
import com.projekat.pmsu_osa.models.entity.Prodavac;
import com.projekat.pmsu_osa.models.entity.Stavka;
import com.projekat.pmsu_osa.services.IAdministratorService;
import com.projekat.pmsu_osa.services.IAkcijaService;
import com.projekat.pmsu_osa.services.IArtiklService;
import com.projekat.pmsu_osa.services.IPorudzbinaService;
import com.projekat.pmsu_osa.services.IProdavacService;
import com.projekat.pmsu_osa.services.IStavkaService;

@CrossOrigin
@RestController
@RequestMapping(value = "/Prodavci")
public class ProdavacController {
	
	@Autowired
    private IStavkaService stavkeService;
	
	@Autowired
    private IPorudzbinaService porudzbineService;
	
	@Autowired
    private IProdavacService prodavciService;
    
	@Autowired 
	private IArtiklService artiklService;

	@Autowired
	private IAkcijaService akcijaService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
    @GetMapping
    public ResponseEntity<List<ProdavacFrontendDTO>> allProdavci(){

        List<Prodavac> prodavci =  prodavciService.findAll();
        List<ProdavacFrontendDTO> prodavciFrontendDTO = new ArrayList<ProdavacFrontendDTO>();
        
        for (Prodavac prodavac: prodavci) {
        	ProdavacFrontendDTO prodavacFrontendDTO = new ProdavacFrontendDTO(prodavac);
        	prodavciFrontendDTO.add(prodavacFrontendDTO);
        }
        return new ResponseEntity<List<ProdavacFrontendDTO>>(prodavciFrontendDTO,HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('KUPAC','ADMINISTRATOR','PRODAVAC','ANONYMOUS')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<ProdavacFrontendDTO> findProdavac(@PathVariable(name = "id") Long id){

    	Prodavac prodavac = prodavciService.findOne(id);
        if(prodavac == null){
            return new ResponseEntity<ProdavacFrontendDTO>(HttpStatus.BAD_REQUEST);
        }
        ProdavacFrontendDTO prodavacFrontendDTO = new ProdavacFrontendDTO(prodavac);
        
        return new ResponseEntity<ProdavacFrontendDTO>(prodavacFrontendDTO, HttpStatus.OK);
    }
    
    @GetMapping(value = "/Porudzbina/{id}")
    public ResponseEntity<ProdavacFrontendDTO> getProdavacZaPorudzbinu(@PathVariable(name = "id") Long idPorudzbine){
    	
    	List<Stavka> stavke = stavkeService.findAll();
    	
    	Prodavac prodavac = null;
    	for (Stavka stavka : stavke) {
    		if (stavka.getPorudzbina().getIdPorudzbine() == idPorudzbine) {
    			prodavac = stavka.getArtikal().getProdavac();
    		}
    	}
    	
        if(prodavac == null){
            return new ResponseEntity<ProdavacFrontendDTO>(HttpStatus.BAD_REQUEST);
        }
        
        ProdavacFrontendDTO prodavacFrontendDTO = new ProdavacFrontendDTO(prodavac);
        
        return new ResponseEntity<ProdavacFrontendDTO>(prodavacFrontendDTO, HttpStatus.OK);
    }
    
    
    @GetMapping(value = "/KorisnickoIme/{username}")
    public ResponseEntity<ProdavacFrontendDTO> findProdavac(@PathVariable(name = "username") String username){

    	Prodavac prodavac = prodavciService.findByUsername(username);
    	System.out.println("PRONADJEN PRODAVAC : " + prodavac);
        if(prodavac == null){
            return new ResponseEntity<ProdavacFrontendDTO>(HttpStatus.BAD_REQUEST);
        }
        ProdavacFrontendDTO prodavacFrontendDTO = new ProdavacFrontendDTO(prodavac);
        
        return new ResponseEntity<ProdavacFrontendDTO>(prodavacFrontendDTO, HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<ProdavacFrontendDTO> saveProdavac(@RequestBody ProdavacBackendDTO prodavacInfo){
        
    	Prodavac prodavac = new Prodavac();
    	
    	prodavac.setIme(prodavacInfo.getImeKorisnika());
    	prodavac.setPrezime(prodavacInfo.getPrezimeKorisnika());
    	prodavac.setKorisnickoIme(prodavacInfo.getKorisnickoIme());
    	prodavac.setLozinka(prodavacInfo.getLozinkaKorisnika());
    	prodavac.setBlokiran(prodavacInfo.isBlokiran());
    	prodavac.setTipKorisnika(prodavacInfo.getTipKorisnika());
    	prodavac.setAdresa(prodavacInfo.getAdresa());
    	prodavac.setPoslujeOd(prodavacInfo.getPoslujeOd());
    	prodavac.setEmail(prodavacInfo.getEmail());
    	prodavac.setNaziv(prodavacInfo.getNaziv());
    	prodavac.setPutanjaSlike(prodavacInfo.getPutanjaSlike());
    	
    	/*
    	if (prodavacInfo.getListaArtikla() != null) {
	    	for (Long idArtikla : prodavacInfo.getListaArtikla()) {
	    		prodavac.getListaArtikla().add(artiklService.findOne(idArtikla));
	    	}
    	}
    	
    	if (prodavacInfo.getListaAkcija() != null) {
	    	for (Long idAkcije : prodavacInfo.getListaAkcija()) {
	    		prodavac.getListaAkcija().add(akcijaService.findOne(idAkcije));
	    	}
    	}*/
    	
    	prodavac = prodavciService.save(prodavac);
    	ProdavacFrontendDTO prodavacFrontendDTO = new ProdavacFrontendDTO(prodavac);
    	
        return new ResponseEntity<ProdavacFrontendDTO>(prodavacFrontendDTO, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", consumes = "application/json")
    public  ResponseEntity<ProdavacFrontendDTO> updateProdavac(@PathVariable(name = "id") Long id,@RequestBody ProdavacBackendDTO prodavacInfo){
    	Prodavac prodavac = prodavciService.findOne(id);
        if(prodavac == null){
            return new ResponseEntity<ProdavacFrontendDTO>(HttpStatus.BAD_REQUEST);
        }

        System.out.println("PRODAVAC LOZINKA: "+ prodavacInfo.getLozinkaKorisnika());
        prodavac.setIme(prodavacInfo.getImeKorisnika());
    	prodavac.setPrezime(prodavacInfo.getPrezimeKorisnika());
    	prodavac.setKorisnickoIme(prodavacInfo.getKorisnickoIme());
    	if (prodavacInfo.getLozinkaKorisnika() != null) {
    		prodavac.setLozinka(passwordEncoder.encode(prodavacInfo.getLozinkaKorisnika()));
    	}
    	
    	prodavac.setBlokiran(prodavacInfo.isBlokiran());
    	//prodavac.setTipKorisnika(prodavacInfo.getTipKorisnika());
    	prodavac.setAdresa(prodavacInfo.getAdresa());
    	//prodavac.setPoslujeOd(prodavacInfo.getPoslujeOd());
    	prodavac.setEmail(prodavacInfo.getEmail());
    	prodavac.setNaziv(prodavacInfo.getNaziv());
    	prodavac.setPutanjaSlike(prodavacInfo.getPutanjaSlike());
    	
    	/*
    	if (prodavacInfo.getListaArtikla() != null) {
	    	for (Long idArtikla : prodavacInfo.getListaArtikla()) {
	    		prodavac.getListaArtikla().add(artiklService.findOne(idArtikla));
	    	}
    	}
    	
    	if (prodavacInfo.getListaAkcija() != null) {
	    	for (Long idAkcije : prodavacInfo.getListaAkcija()) {
	    		prodavac.getListaAkcija().add(akcijaService.findOne(idAkcije));
	    	}
    	}*/
    	
    	prodavac = prodavciService.save(prodavac);
    	ProdavacFrontendDTO prodavacFrontendDTO = new ProdavacFrontendDTO(prodavac);
    	
        return new ResponseEntity<ProdavacFrontendDTO>(prodavacFrontendDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteProdavac(@PathVariable(name = "id") Long id){

    	Prodavac prodavac = prodavciService.findOne(id);
        if(prodavac == null){
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        prodavciService.remove(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
	
}
