package com.projekat.pmsu_osa.controllers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projekat.pmsu_osa.models.dto.AdministratorBackendDTO;
import com.projekat.pmsu_osa.models.dto.AdministratorFrontendDTO;
import com.projekat.pmsu_osa.models.dto.AkcijaBackendDTO;
import com.projekat.pmsu_osa.models.dto.AkcijaFrontendDTO;
import com.projekat.pmsu_osa.models.dto.ArtiklFrontendDTO;
import com.projekat.pmsu_osa.models.entity.Administrator;
import com.projekat.pmsu_osa.models.entity.Akcija;
import com.projekat.pmsu_osa.models.entity.Artikl;
import com.projekat.pmsu_osa.security.TokenUtils;
import com.projekat.pmsu_osa.services.IAdministratorService;
import com.projekat.pmsu_osa.services.IAkcijaService;
import com.projekat.pmsu_osa.services.IArtiklService;
import com.projekat.pmsu_osa.services.IProdavacService;

@CrossOrigin
@RestController
@RequestMapping(value = "/Akcije")
public class AkcijaController {

	@Autowired
	private TokenUtils tokenUtils;
	
	@Autowired
    private IAkcijaService akcijeService;
	
	@Autowired
    private IArtiklService artikliService;
    
	@Autowired IProdavacService prodavacService;
	
    @PreAuthorize("hasAnyRole('ADMINISTRATOR')")
    @GetMapping
    public ResponseEntity<List<AkcijaFrontendDTO>> allAkcije(){

        List<Akcija> akcije =  akcijeService.findAll();
        List<AkcijaFrontendDTO> akcijeFrontendDTO = new ArrayList<AkcijaFrontendDTO>();
        
        
        for (Akcija akcija: akcije) {
        	System.out.println("ARTIKLI AKCIJA" + akcija.getArtikli());
        	AkcijaFrontendDTO akcijaFrontendDTO = new AkcijaFrontendDTO(akcija);
        	akcijeFrontendDTO.add(akcijaFrontendDTO);
        }
        System.out.println(akcijeFrontendDTO);
        return new ResponseEntity<List<AkcijaFrontendDTO>>(akcijeFrontendDTO,HttpStatus.OK);
    }
    
    
    @PreAuthorize("hasAnyRole('KUPAC','ADMINISTRATOR','PRODAVAC')")
    @GetMapping(value = "/Prodavac/{id}")
    public ResponseEntity<List<AkcijaFrontendDTO>> akcijeZaProdavca(@PathVariable(name = "id") Long idProdavca){

        List<Akcija> akcije =  akcijeService.findAll();
        List<AkcijaFrontendDTO> akcijeFrontendDTO = new ArrayList<AkcijaFrontendDTO>();
        
        
        for (Akcija akcija: akcije) {
        	if (akcija.getProdavac().getIdKorisnika() == idProdavca) {
        		AkcijaFrontendDTO akcijaFrontendDTO = new AkcijaFrontendDTO(akcija);
        		akcijeFrontendDTO.add(akcijaFrontendDTO);
        	}
        }
        System.out.println(akcijeFrontendDTO);
        return new ResponseEntity<List<AkcijaFrontendDTO>>(akcijeFrontendDTO,HttpStatus.OK);
    }
    
    @PreAuthorize("hasAnyRole('KUPAC','ADMINISTRATOR','PRODAVAC')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<AkcijaFrontendDTO> findAkcija(@PathVariable(name = "id") Long id){

        Akcija akcija = akcijeService.findOne(id);
        if(akcija == null){
            return new ResponseEntity<AkcijaFrontendDTO>(HttpStatus.BAD_REQUEST);
        }
        AkcijaFrontendDTO akcijaFrontendDTO = new AkcijaFrontendDTO(akcija);
        
        return new ResponseEntity<AkcijaFrontendDTO>(akcijaFrontendDTO, HttpStatus.OK);
    }

	@PreAuthorize("hasAnyRole('PRODAVAC')")
    @PostMapping(consumes = "application/json")
    public ResponseEntity<AkcijaFrontendDTO> saveAkcija(HttpServletRequest request, @RequestBody AkcijaBackendDTO akcijaInfo){
        
		// provjera da korisnik ne dodaje artikle koji NISU njegovi na akciju
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		String token = httpServletRequest.getHeader("Authorization");
		if (token != null) {
			if(token.startsWith("Bearer ")) {
				token = token.substring(7);
			}
		}
		
		Long idKorisnika = tokenUtils.getIdFromToken(token);
		System.out.println(idKorisnika);
		if (idKorisnika != null) {
			for (Long idArtikla : akcijaInfo.getArtikli()) {
				if (artikliService.findOne(idArtikla).getProdavac().getIdKorisnika() != idKorisnika) {
					return new ResponseEntity<AkcijaFrontendDTO>(HttpStatus.FORBIDDEN);
				}
			}
		}
		
		else {
			return new ResponseEntity<AkcijaFrontendDTO>(HttpStatus.UNAUTHORIZED);
		}
		
		
    	Akcija akcija = new Akcija();
    	if (akcijaInfo.getIdProdavca() != null && prodavacService.findOne(akcijaInfo.getIdProdavca()) != null) {
        	akcija.setProdavac(prodavacService.findOne(akcijaInfo.getIdProdavca()));
        }
    	else {
    		return new ResponseEntity<AkcijaFrontendDTO>(HttpStatus.BAD_REQUEST);
    	}
    	akcija.setOdKad(LocalDateTime.parse(akcijaInfo.getPocetakAkcije()));
    	akcija.setDoKad(LocalDateTime.parse(akcijaInfo.getKrajAkcije()));
    	akcija.setProcenat(akcijaInfo.getProcenat());
    	akcija.setTekst(akcijaInfo.getTekst());
    	System.out.println(akcijaInfo.getArtikli());
    	akcija.setArtikli(akcijaInfo.getArtikli());
    	System.out.println(akcija.getArtikli());
    	// ovo radim jer kad se sejvuje akcija gubi artikle
    	akcija = akcijeService.save(akcija);
    	AkcijaFrontendDTO akcijaFrontendDTO = new AkcijaFrontendDTO(akcija);
    	
        return new ResponseEntity<AkcijaFrontendDTO>(akcijaFrontendDTO, HttpStatus.CREATED);
    }

	@PreAuthorize("hasAnyRole('PRODAVAC')")
    @PutMapping(value = "/{id}", consumes = "application/json")
    public  ResponseEntity<AkcijaFrontendDTO> updateAkcija(HttpServletRequest request, @PathVariable(name = "id") Long id,@RequestBody AkcijaBackendDTO akcijaInfo){
    	
        Akcija akcija = akcijeService.findOne(id);
        if(akcija == null){
            return new ResponseEntity<AkcijaFrontendDTO>(HttpStatus.BAD_REQUEST);
        }
        
        
        // provjera da korisnik kome pripada akcija je izmenjuje
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		String token = httpServletRequest.getHeader("Authorization");
		if (token != null) {
			if(token.startsWith("Bearer ")) {
				token = token.substring(7);
			}
		}
		
		Long idKorisnika = tokenUtils.getIdFromToken(token);
		System.out.println(idKorisnika);
		if (idKorisnika != null) {			
			if (idKorisnika != akcija.getProdavac().getIdKorisnika()) {
				return new ResponseEntity<AkcijaFrontendDTO>(HttpStatus.FORBIDDEN);
			}
		}
		
		else {
			return new ResponseEntity<AkcijaFrontendDTO>(HttpStatus.UNAUTHORIZED);
		}
        
        
        if (akcijaInfo.getIdProdavca() != null && prodavacService.findOne(akcijaInfo.getIdProdavca()) != null) {
        	akcija.setProdavac(prodavacService.findOne(akcijaInfo.getIdProdavca()));
        }
        akcija.setOdKad(LocalDateTime.parse(akcijaInfo.getPocetakAkcije()));
    	akcija.setDoKad(LocalDateTime.parse(akcijaInfo.getKrajAkcije()));
    	akcija.setProcenat(akcijaInfo.getProcenat());
    	akcija.setTekst(akcijaInfo.getTekst());
    	akcija.setArtikli(akcijaInfo.getArtikli());
    	
    	akcija = akcijeService.save(akcija);
    	AkcijaFrontendDTO akcijaFrontendDTO = new AkcijaFrontendDTO(akcija);
    	
        return new ResponseEntity<AkcijaFrontendDTO>(akcijaFrontendDTO, HttpStatus.OK);
    }

	@PreAuthorize("hasAnyRole('PRODAVAC')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteAkcija(HttpServletRequest request, @PathVariable(name = "id") Long id){

    	Akcija akcija = akcijeService.findOne(id);
        if(akcija == null){
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        
        // provjera da korisnik kome pripada akcija je izmenjuje
 		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
 		String token = httpServletRequest.getHeader("Authorization");
 		if (token != null) {
 			if(token.startsWith("Bearer ")) {
 				token = token.substring(7);
 			}
 		}
 		
 		Long idKorisnika = tokenUtils.getIdFromToken(token);
 		System.out.println(idKorisnika);
 		if (idKorisnika != null) {			
 			if (idKorisnika != akcija.getProdavac().getIdKorisnika()) {
 				return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
 			}
 		}
 		
 		else {
 			return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
 		}
        
        akcijeService.remove(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
	
}
