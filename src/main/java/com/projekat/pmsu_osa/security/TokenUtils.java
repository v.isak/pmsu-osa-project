package com.projekat.pmsu_osa.security;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.projekat.pmsu_osa.services.implementation.UserDetailsServiceImpl;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class TokenUtils {

	@Value("biloKojiString") // random secret moze biti bilo sta
	private String secret;
	
	@Value("3600") // 3600s = 1h
	private Long expiration;
	
	
	public String getUsernameFromToken(String token) {
		String username;
		try {
			Claims claims = this.getClaimsFromToken(token);
			username = claims.getSubject();
		}
		catch(Exception e) {
			username = null;
		}
		return username;
	}
	
	private Claims getClaimsFromToken(String token) {
		
		Claims claims;
		try {
			claims = Jwts.parser().setSigningKey(this.secret) // izvlacenje payload-a
					.parseClaimsJws(token).getBody();
		}
		catch(Exception e) {
			claims = null;
		}
		return claims;
		
	}
	
	
	public Long getIdFromToken(String token) {
		Long id;
		try {
			Claims claims = this.getClaimsFromToken(token);
			id = Long.valueOf(claims.get("id").toString());
		}
		catch(Exception e) {
			id = null;
		}
		return id;
	}
	
	
	public String getRoleFromToken(String token) {
		String role;
		try {
			Claims claims = this.getClaimsFromToken(token);
			role = String.valueOf(claims.get("role").toString());
		}
		catch(Exception e) {
			role = null;
		}
		return role;
	}
	
	
	private Date getExpirationDateFromToken(String token) {
		Date expirationDate;
		try {
			final Claims claims = this.getClaimsFromToken(token);
			expirationDate = claims.getExpiration();
		}
		catch(Exception e) {
			expirationDate = null;
		}
		return expirationDate;
	}
	
	private boolean isTokenExpired(String token) {
		final Date expirationDate = this.getExpirationDateFromToken(token);
		return expirationDate.before(new Date(System.currentTimeMillis()));
	}
	
	public boolean validateToken(String token, UserDetails userDetails) {
		final String username = getUsernameFromToken(token);
		return username.equals(userDetails.getUsername()) && !isTokenExpired(token);
	}
	
	
	
	public String generateToken(UserDetails userDetails, UserDetailsServiceImpl userDetailsService) {
		
		Map<String, Object> claims = new HashMap<>();
		claims.put("id", userDetailsService.loadUserObjectByUsername(userDetails.getUsername()).getIdKorisnika());
		claims.put("sub", userDetails.getUsername());
		claims.put("role", userDetails.getAuthorities().toArray()[0]);
		claims.put("created", new Date(System.currentTimeMillis()));
		
		return Jwts.builder().setClaims(claims)
			.setExpiration(new Date(System.currentTimeMillis() + expiration * 1000))
			.signWith(SignatureAlgorithm.HS512, secret).compact();
	}
	
	
}
