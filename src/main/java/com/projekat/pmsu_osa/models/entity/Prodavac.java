package com.projekat.pmsu_osa.models.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
//END OF LOMBOK
@Entity
@Table(name = "prodavac")
public class Prodavac extends Korisnik{

	
	@Column(name = "poslujeOd", unique = false, nullable = false)
    private LocalDateTime poslujeOd;
    
    @Column(name = "email", unique = false, nullable = false)
    private String email;
    
    @Column(name = "adresa", unique = false, nullable = false)
    private String adresa;
    
    @Column(name = "naziv", unique = false, nullable = false)
    private String naziv;
    
    @Column(name = "putanja_slike", nullable = false, unique = false)
    private String putanjaSlike;
    
    /*
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "prodavac")
    private Set<Artikl> listaArtikla;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "prodavac")
    private Set<Akcija> listaAkcija ;
    */

    
}
