package com.projekat.pmsu_osa.models.entity;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Data
//END OF LOMBOK
@Entity
@Table(name = "porudzbina")
public class Porudzbina {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_porudzbine", nullable = false, unique = true)
    private Long idPorudzbine;
	
	@ManyToOne
    @JoinColumn(name = "id_korisnika", referencedColumnName = "id_korisnika", nullable = true)
    @JsonIgnore
    private Kupac kupac;
	
	@Column(name = "satnica", nullable = false, unique = false)
    private LocalDateTime satnica;
	
	@Column(name = "dostavljeno", nullable = false, unique = false)
    private boolean dostavljeno;
	
	@Column(name = "ocena", nullable = false, unique = false)
    private float ocena;
	
	@Column(name = "komentar", nullable = true, unique = false)
    private String komentar;
	
	@Column(name = "anoniman_komentar", nullable = false, unique = false)
    private boolean anonimanKomentar;
	
	@Column(name = "arhivirani_komentar", nullable = false, unique = false)
    private boolean arhiviraniKomentar;
	
	@Column(name = "cena_porudzbine", nullable = false, unique = false)
    private float cenaPorudzbine;
	
	/*
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "porudzbina")
    private Set<Stavka> listaStavki;*/

    
}
