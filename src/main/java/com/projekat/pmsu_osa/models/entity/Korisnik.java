package com.projekat.pmsu_osa.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import com.projekat.pmsu_osa.enums.TipKorisnika;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
//END OF LOMBOK
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Entity
public abstract class Korisnik {

	@Id
    //identity inkrementuje za 1
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_korisnika", unique = true, nullable = false)
    private long idKorisnika;
	
	@Column(name = "ime_korisnika", unique = false, nullable = false)
	private String ime;
	
	@Column(name = "prezime_korisnika", unique = false, nullable = false)
	private String prezime;
	
	@Column(name = "korisnicko_ime", unique = false, nullable = false)
	private String korisnickoIme;
	
	@Column(name = "lozinka", unique = false, nullable = false)
	private String lozinka;
	
	@Column(name = "blokiran", unique = false, nullable = false)
	private boolean blokiran;
	
	@Enumerated(EnumType.STRING)
    @Column(name = "tip_korisnika")
    private TipKorisnika tipKorisnika;

}
