package com.projekat.pmsu_osa.models.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
//END OF LOMBOK
@Entity
@Table(name = "akcija")
public class Akcija {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_akcije", nullable = false, unique = true)
    private Long idAkcije;

	@ManyToOne
    @JoinColumn(name = "id_korisnika", referencedColumnName = "id_korisnika", nullable = true)
    @JsonIgnore
    private Prodavac prodavac;
	
	@Column(name = "procenat", nullable = false, unique = false)
    private int procenat;
	
	@Column(name = "pocetak_akcije", nullable = false, unique = false)
    private LocalDateTime odKad;
	
	@Column(name = "kraj_akcije", nullable = false, unique = false)
    private LocalDateTime doKad;
	
	@Column(name = "tekst", nullable = false, unique = false)
    private String tekst;

	@ElementCollection
    private List<Long> artikli = new ArrayList<>();
    
}
