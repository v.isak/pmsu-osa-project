package com.projekat.pmsu_osa.models.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Data
//END OF LOMBOK
@Entity
@Table(name = "artikl")
public class Artikl {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_artikla", nullable = false, unique = true)
    private Long idArtikla;
	

	@ManyToOne
    @JoinColumn(name = "id_korisnika", referencedColumnName = "id_korisnika", nullable = true)
    @JsonIgnore
    private Prodavac prodavac;
	
	@Column(name = "naziv", nullable = false, unique = false)
    private String naziv;
	
	@Column(name = "opis", nullable = false, unique = false)
    private String opis;
	
	@Column(name = "cena", nullable = false, unique = false)
    private double cena;
	
	@Column(name = "putanja_slike", nullable = false, unique = false)
    private String putanjaSlike;

	@ElementCollection
    private List<Long> akcije = new ArrayList<>();
    
}
