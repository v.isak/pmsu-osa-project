package com.projekat.pmsu_osa.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
//END OF LOMBOK
@Entity
@Table(name = "stavka")
public class Stavka {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_stavke", nullable = false, unique = true)
    private Long idStavke;
	
	@ManyToOne
    @JoinColumn(name = "id_artikla", referencedColumnName = "id_artikla", nullable = true)
    @JsonIgnore
    private Artikl artikal;
	
	@ManyToOne
    @JoinColumn(name = "id_porudzbine", referencedColumnName = "id_porudzbine", nullable = true)
    @JsonIgnore
    private Porudzbina porudzbina;
    
	@Column(name = "kolicina", nullable = false, unique = false)
    private int kolicina;
}

    
