package com.projekat.pmsu_osa.models.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

import com.projekat.pmsu_osa.models.entity.Porudzbina;
import com.projekat.pmsu_osa.models.entity.Stavka;

public class PorudzbinaBackendDTO implements Serializable {

	private Long idKupca;
	private LocalDateTime satnica;
	private boolean dostavljeno;
	private float ocena;
	private String komentar;
	private boolean anonimanKomentar;
	private boolean arhiviraniKomentar;
	private float cenaPorudzbine;
	
	
	public PorudzbinaBackendDTO() {
		super();
	}

	public PorudzbinaBackendDTO(Long idKupca, LocalDateTime satnica, boolean dostavljeno, float ocena,
			String komentar, boolean anonimanKomentar, boolean arhiviraniKomentar, float cenaPorudzbine) {
		super();
		this.idKupca = idKupca;
		this.satnica = satnica;
		this.dostavljeno = dostavljeno;
		this.ocena = ocena;
		this.komentar = komentar;
		this.anonimanKomentar = anonimanKomentar;
		this.arhiviraniKomentar = arhiviraniKomentar;
		this.cenaPorudzbine = cenaPorudzbine;
	}
	
	public PorudzbinaBackendDTO(Porudzbina porudzbina) {
		super();
		this.idKupca = porudzbina.getKupac().getIdKorisnika();
		this.satnica = porudzbina.getSatnica();
		this.dostavljeno = porudzbina.isDostavljeno();
		this.ocena = porudzbina.getOcena();
		this.komentar = porudzbina.getKomentar();
		this.anonimanKomentar = porudzbina.isAnonimanKomentar();
		this.arhiviraniKomentar = porudzbina.isArhiviraniKomentar();
		this.cenaPorudzbine = porudzbina.getCenaPorudzbine();
	}

	
	public float getCenaPorudzbine() {
		return cenaPorudzbine;
	}

	public void setCenaPorudzbine(float cenaPorudzbine) {
		this.cenaPorudzbine = cenaPorudzbine;
	}
	
	public Long getIdKupca() {
		return idKupca;
	}

	public void setIdKupca(Long idKupca) {
		this.idKupca = idKupca;
	}

	public LocalDateTime getSatnica() {
		return satnica;
	}

	public void setSatnica(LocalDateTime satnica) {
		this.satnica = satnica;
	}

	public boolean isDostavljeno() {
		return dostavljeno;
	}

	public void setDostavljeno(boolean dostavljeno) {
		this.dostavljeno = dostavljeno;
	}

	public float getOcena() {
		return ocena;
	}

	public void setOcena(float ocena) {
		this.ocena = ocena;
	}

	public String getKomentar() {
		return komentar;
	}

	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}

	public boolean isAnonimanKomentar() {
		return anonimanKomentar;
	}

	public void setAnonimanKomentar(boolean anonimanKomentar) {
		this.anonimanKomentar = anonimanKomentar;
	}

	public boolean isArhiviraniKomentar() {
		return arhiviraniKomentar;
	}

	public void setArhiviraniKomentar(boolean arhiviraniKomentar) {
		this.arhiviraniKomentar = arhiviraniKomentar;
	}

	
}
