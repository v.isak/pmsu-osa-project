package com.projekat.pmsu_osa.models.dto;

import java.io.Serializable;

import com.projekat.pmsu_osa.enums.TipKorisnika;
import com.projekat.pmsu_osa.models.entity.Administrator;

public class AdministratorFrontendDTO implements Serializable{
	
	private Long idKorisnika;
	private String imeKorisnika;
    private String prezimeKorisnika;
    private String korisnickoIme;
    private String lozinkaKorisnika;
    private boolean blokiran;
    private TipKorisnika tipKorisnika;
    
    
    
	public AdministratorFrontendDTO() {
		super();
	}



	public AdministratorFrontendDTO(String imeKorisnika, String prezimeKorisnika, String korisnickoIme,
			String lozinkaKorisnika, boolean blokiran, TipKorisnika tipKorisnika) {
		super();
		this.imeKorisnika = imeKorisnika;
		this.prezimeKorisnika = prezimeKorisnika;
		this.korisnickoIme = korisnickoIme;
		this.lozinkaKorisnika = lozinkaKorisnika;
		this.blokiran = blokiran;
		this.tipKorisnika = tipKorisnika;
	}
	
	public AdministratorFrontendDTO(Administrator admin) {
		super();
		this.idKorisnika = admin.getIdKorisnika();
		this.imeKorisnika = admin.getIme();
		this.prezimeKorisnika = admin.getPrezime();
		this.korisnickoIme = admin.getKorisnickoIme();
		this.lozinkaKorisnika = admin.getLozinka();
		this.blokiran = admin.isBlokiran();
		this.tipKorisnika = admin.getTipKorisnika();
	}


	
	

	public Long getIdKorisnika() {
		return idKorisnika;
	}



	public void setIdKorisnika(Long idKorisnika) {
		this.idKorisnika = idKorisnika;
	}



	public String getImeKorisnika() {
		return imeKorisnika;
	}



	public void setImeKorisnika(String imeKorisnika) {
		this.imeKorisnika = imeKorisnika;
	}



	public String getPrezimeKorisnika() {
		return prezimeKorisnika;
	}



	public void setPrezimeKorisnika(String prezimeKorisnika) {
		this.prezimeKorisnika = prezimeKorisnika;
	}



	public String getKorisnickoIme() {
		return korisnickoIme;
	}



	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}



	public String getLozinkaKorisnika() {
		return lozinkaKorisnika;
	}



	public void setLozinkaKorisnika(String lozinkaKorisnika) {
		this.lozinkaKorisnika = lozinkaKorisnika;
	}



	public boolean isBlokiran() {
		return blokiran;
	}



	public void setBlokiran(boolean blokiran) {
		this.blokiran = blokiran;
	}



	public TipKorisnika getTipKorisnika() {
		return tipKorisnika;
	}



	public void setTipKorisnika(TipKorisnika tipKorisnika) {
		this.tipKorisnika = tipKorisnika;
	}
		
    

}
