package com.projekat.pmsu_osa.models.dto;

import java.io.Serializable;
import java.util.Set;

import com.projekat.pmsu_osa.enums.TipKorisnika;
import com.projekat.pmsu_osa.models.entity.Kupac;
import com.projekat.pmsu_osa.models.entity.Porudzbina;

public class KupacBackendDTO implements Serializable {

	
	private String imeKorisnika;
    private String prezimeKorisnika;
    private String korisnickoIme;
    private String lozinkaKorisnika;
    private boolean blokiran;
    private TipKorisnika tipKorisnika;
    
    private String adresa;
    private Set<Long> listaPorudzbina;
    
    
    
    
    
	public KupacBackendDTO() {
		super();
	}

	public KupacBackendDTO(String imeKorisnika, String prezimeKorisnika, String korisnickoIme, String lozinkaKorisnika,
			boolean blokiran, TipKorisnika tipKorisnika, String adresa) {
		super();
		this.imeKorisnika = imeKorisnika;
		this.prezimeKorisnika = prezimeKorisnika;
		this.korisnickoIme = korisnickoIme;
		this.lozinkaKorisnika = lozinkaKorisnika;
		this.blokiran = blokiran;
		this.tipKorisnika = tipKorisnika;
		this.adresa = adresa;
		//this.listaPorudzbina = listaPorudzbina;
	}
	
	public KupacBackendDTO(Kupac kupac) {
		super();
		this.imeKorisnika = kupac.getIme();
		this.prezimeKorisnika = kupac.getPrezime();
		this.korisnickoIme = kupac.getKorisnickoIme();
		this.lozinkaKorisnika = kupac.getLozinka();
		this.blokiran = kupac.isBlokiran();
		this.tipKorisnika = kupac.getTipKorisnika();
		this.adresa = kupac.getAdresa();
		/*for (Porudzbina porudzbina : kupac.getListaPorudzbina()) {
			this.listaPorudzbina.add(porudzbina.getIdPorudzbine());
		}*/
	}
	

	public String getImeKorisnika() {
		return imeKorisnika;
	}

	public void setImeKorisnika(String imeKorisnika) {
		this.imeKorisnika = imeKorisnika;
	}

	public String getPrezimeKorisnika() {
		return prezimeKorisnika;
	}

	public void setPrezimeKorisnika(String prezimeKorisnika) {
		this.prezimeKorisnika = prezimeKorisnika;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinkaKorisnika() {
		return lozinkaKorisnika;
	}

	public void setLozinkaKorisnika(String lozinkaKorisnika) {
		this.lozinkaKorisnika = lozinkaKorisnika;
	}

	public boolean isBlokiran() {
		return blokiran;
	}

	public void setBlokiran(boolean blokiran) {
		this.blokiran = blokiran;
	}

	public TipKorisnika getTipKorisnika() {
		return tipKorisnika;
	}

	public void setTipKorisnika(TipKorisnika tipKorisnika) {
		this.tipKorisnika = tipKorisnika;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public Set<Long> getListaPorudzbina() {
		return listaPorudzbina;
	}

	public void setListaPorudzbina(Set<Long> listaPorudzbina) {
		this.listaPorudzbina = listaPorudzbina;
	}
	
}
