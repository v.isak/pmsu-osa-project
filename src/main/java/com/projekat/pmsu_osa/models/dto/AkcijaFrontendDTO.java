package com.projekat.pmsu_osa.models.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import com.projekat.pmsu_osa.models.entity.Akcija;
import com.projekat.pmsu_osa.models.entity.Artikl;

public class AkcijaFrontendDTO implements Serializable {

	private Long idAkcije;
	private Long idProdavca;
	private int procenat;
	private String pocetakAkcije;
	private String krajAkcije;
	private String tekst;
	private List<Long> artikli;
	
	
	
	public AkcijaFrontendDTO() {
		super();
	}


	public AkcijaFrontendDTO(Long idAkcije, Long idProdavca, int procenat, String pocetakAkcije,
			String krajAkcije, String tekst, List<Long> artikli) {
		super();
		this.idAkcije = idAkcije;
		this.idProdavca = idProdavca;
		this.procenat = procenat;
		this.pocetakAkcije = pocetakAkcije;
		this.krajAkcije = krajAkcije;
		this.tekst = tekst;
		this.artikli = artikli;
	}
	
	
	public AkcijaFrontendDTO(Akcija akcija) {
		super();
		this.idAkcije = akcija.getIdAkcije();
		if (akcija.getProdavac() != null) {
			this.idProdavca = akcija.getProdavac().getIdKorisnika();
		}
		this.procenat = akcija.getProcenat();
		this.pocetakAkcije = akcija.getOdKad().toString();
		this.krajAkcije = akcija.getDoKad().toString();
		this.tekst = akcija.getTekst();
		this.artikli = akcija.getArtikli();
	}


	
	
	public List<Long> getArtikli() {
		return artikli;
	}


	public void setArtikli(List<Long> artikli) {
		this.artikli = artikli;
	}


	public Long getIdAkcije() {
		return idAkcije;
	}


	public void setIdAkcije(Long idAkcije) {
		this.idAkcije = idAkcije;
	}


	public Long getIdProdavca() {
		return idProdavca;
	}


	public void setIdProdavca(Long idProdavca) {
		this.idProdavca = idProdavca;
	}


	public int getProcenat() {
		return procenat;
	}


	public void setProcenat(int procenat) {
		this.procenat = procenat;
	}


	public String getPocetakAkcije() {
		return pocetakAkcije;
	}


	public void setPocetakAkcije(String pocetakAkcije) {
		this.pocetakAkcije = pocetakAkcije;
	}


	public String getKrajAkcije() {
		return krajAkcije;
	}


	public void setKrajAkcije(String krajAkcije) {
		this.krajAkcije = krajAkcije;
	}


	public String getTekst() {
		return tekst;
	}


	public void setTekst(String tekst) {
		this.tekst = tekst;
	}
	
	
	
	
	
	
}
