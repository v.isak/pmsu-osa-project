package com.projekat.pmsu_osa.models.dto;

import java.io.Serializable;

import com.projekat.pmsu_osa.models.entity.Korisnik;

public class KorisnikFrontendDTO implements Serializable{

	Long idKorisnika;
	String ime;
	String prezime;
	String korisnickoIme;
	boolean blokiran;
	
	
	public KorisnikFrontendDTO() {
		super();
	}

	public KorisnikFrontendDTO(Long idKorisnika, String ime, String prezime, String korisnickoIme, boolean blokiran) {
		super();
		this.idKorisnika = idKorisnika;
		this.ime = ime;
		this.prezime = prezime;
		this.korisnickoIme = korisnickoIme;
		this.blokiran = blokiran;
	}
	
	public KorisnikFrontendDTO(Korisnik korisnik) {
		super();
		this.idKorisnika = korisnik.getIdKorisnika();
		this.ime = korisnik.getIme();
		this.prezime = korisnik.getPrezime();
		this.korisnickoIme = korisnik.getKorisnickoIme();
		this.blokiran = korisnik.isBlokiran();
	}
	
	
	
	public Long getIdKorisnika() {
		return idKorisnika;
	}

	public void setIdKorisnika(Long idKorisnika) {
		this.idKorisnika = idKorisnika;
	}

	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getKorisnickoIme() {
		return korisnickoIme;
	}
	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}
	public boolean isBlokiran() {
		return blokiran;
	}
	public void setBlokiran(boolean blokiran) {
		this.blokiran = blokiran;
	}
	
	
}
