package com.projekat.pmsu_osa.models.dto;

import java.io.Serializable;

public class PrijavaDTO implements Serializable{

	private String username;
	private String password;
	
	
	
	public PrijavaDTO() {
		super();
	}

	public PrijavaDTO(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	
	
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
