package com.projekat.pmsu_osa.models.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

import com.projekat.pmsu_osa.enums.TipKorisnika;
import com.projekat.pmsu_osa.models.entity.Akcija;
import com.projekat.pmsu_osa.models.entity.Artikl;
import com.projekat.pmsu_osa.models.entity.Prodavac;

public class ProdavacBackendDTO implements Serializable {

	
	private String imeKorisnika;
    private String prezimeKorisnika;
    private String korisnickoIme;
    private String lozinkaKorisnika;
    private boolean blokiran;
    private TipKorisnika tipKorisnika;
    
    private LocalDateTime poslujeOd;
    private String email;
    private String adresa;
    private String naziv;
    private String putanjaSlike;
    private Set<Long> listaArtikla;
    private Set<Long> listaAkcija ;
    
    
    
    
    
	public ProdavacBackendDTO() {
		super();
	}

	public ProdavacBackendDTO(String imeKorisnika, String prezimeKorisnika, String korisnickoIme,
			String lozinkaKorisnika, boolean blokiran, TipKorisnika tipKorisnika, LocalDateTime poslujeOd, String email,
			String adresa, String naziv, String putanjaSlike) {
		super();
		this.imeKorisnika = imeKorisnika;
		this.prezimeKorisnika = prezimeKorisnika;
		this.korisnickoIme = korisnickoIme;
		this.lozinkaKorisnika = lozinkaKorisnika;
		this.blokiran = blokiran;
		this.tipKorisnika = tipKorisnika;
		this.poslujeOd = poslujeOd;
		this.email = email;
		this.adresa = adresa;
		this.naziv = naziv;
		this.putanjaSlike = putanjaSlike;
	}
	
	public ProdavacBackendDTO(Prodavac prodavac) {
		super();
		this.imeKorisnika = prodavac.getIme();
		this.prezimeKorisnika = prodavac.getPrezime();
		this.korisnickoIme = prodavac.getKorisnickoIme();
		this.lozinkaKorisnika = prodavac.getLozinka();
		this.blokiran = prodavac.isBlokiran();
		this.tipKorisnika = prodavac.getTipKorisnika();
		this.poslujeOd = prodavac.getPoslujeOd();
		this.email = prodavac.getEmail();
		this.adresa = prodavac.getAdresa();
		this.naziv = prodavac.getNaziv();
		this.putanjaSlike = prodavac.getPutanjaSlike();
		
		/*for (Artikl artikl : prodavac.getListaArtikla()) {
			this.listaArtikla.add(artikl.getIdArtikla());
		}
		for (Akcija akcija : prodavac.getListaAkcija()) {
			this.listaArtikla.add(akcija.getIdAkcije());
		}*/
	}
	
	

	public String getImeKorisnika() {
		return imeKorisnika;
	}

	public void setImeKorisnika(String imeKorisnika) {
		this.imeKorisnika = imeKorisnika;
	}

	public String getPrezimeKorisnika() {
		return prezimeKorisnika;
	}

	public void setPrezimeKorisnika(String prezimeKorisnika) {
		this.prezimeKorisnika = prezimeKorisnika;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinkaKorisnika() {
		return lozinkaKorisnika;
	}

	public void setLozinkaKorisnika(String lozinkaKorisnika) {
		this.lozinkaKorisnika = lozinkaKorisnika;
	}

	public boolean isBlokiran() {
		return blokiran;
	}

	public void setBlokiran(boolean blokiran) {
		this.blokiran = blokiran;
	}

	public TipKorisnika getTipKorisnika() {
		return tipKorisnika;
	}

	public void setTipKorisnika(TipKorisnika tipKorisnika) {
		this.tipKorisnika = tipKorisnika;
	}

	public LocalDateTime getPoslujeOd() {
		return poslujeOd;
	}

	public void setPoslujeOd(LocalDateTime poslujeOd) {
		this.poslujeOd = poslujeOd;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Set<Long> getListaArtikla() {
		return listaArtikla;
	}

	public void setListaArtikla(Set<Long> listaArtikla) {
		this.listaArtikla = listaArtikla;
	}

	public Set<Long> getListaAkcija() {
		return listaAkcija;
	}

	public void setListaAkcija(Set<Long> listaAkcija) {
		this.listaAkcija = listaAkcija;
	}

	public String getPutanjaSlike() {
		return putanjaSlike;
	}

	public void setPutanjaSlike(String putanjaSlike) {
		this.putanjaSlike = putanjaSlike;
	}
	
	
}
