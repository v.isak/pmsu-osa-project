package com.projekat.pmsu_osa.models.dto;

import java.io.Serializable;

import com.projekat.pmsu_osa.models.entity.Stavka;

public class StavkaBackendDTO implements Serializable {

	
	private Long idArtikla;
	private Long idPorudzbine;
	private int kolicina;
	
	
	
	
	
	public StavkaBackendDTO() {
		super();
	}

	public StavkaBackendDTO(Long idArtikla, Long idPorudzbine, int kolicina) {
		super();
		this.idArtikla = idArtikla;
		this.idPorudzbine = idPorudzbine;
		this.kolicina = kolicina;
	}

	public StavkaBackendDTO(Stavka stavka) {
		super();
		this.idArtikla = stavka.getArtikal().getIdArtikla();
		this.idPorudzbine = stavka.getPorudzbina().getIdPorudzbine();
		this.kolicina = stavka.getKolicina();
	}

	
	

	public Long getIdArtikla() {
		return idArtikla;
	}

	public void setIdArtikla(Long idArtikla) {
		this.idArtikla = idArtikla;
	}

	public Long getIdPorudzbine() {
		return idPorudzbine;
	}

	public void setIdPorudzbine(Long idPorudzbine) {
		this.idPorudzbine = idPorudzbine;
	}

	public int getKolicina() {
		return kolicina;
	}

	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}
	
}
