package com.projekat.pmsu_osa.models.dto;

import java.io.Serializable;

import com.projekat.pmsu_osa.models.entity.Artikl;

public class ArtiklFrontendDTO implements Serializable {

	
	private Long idArtikla;
	private Long idProdavca;
	private String naziv;
	private String opis;
	private Double cena;
	private String putanjaSlike;
	
	
	
	
	public ArtiklFrontendDTO() {
		super();
	}


	public ArtiklFrontendDTO(Long idArtikla, Long idProdavca, String naziv, String opis, Double cena,
			String putanjaSlike) {
		super();
		this.idArtikla = idArtikla;
		this.idProdavca = idProdavca;
		this.naziv = naziv;
		this.opis = opis;
		this.cena = cena;
		this.putanjaSlike = putanjaSlike;
	}
	
	
	public ArtiklFrontendDTO(Artikl artikl) {
		super();
		this.idArtikla = artikl.getIdArtikla();
		if (artikl.getProdavac() != null) {
			this.idProdavca = artikl.getProdavac().getIdKorisnika();
		}
		this.naziv = artikl.getNaziv();
		this.opis = artikl.getOpis();
		this.cena = artikl.getCena();
		this.putanjaSlike = artikl.getPutanjaSlike();
	}

	
	

	public Long getIdArtikla() {
		return idArtikla;
	}


	public void setIdArtikla(Long idArtikla) {
		this.idArtikla = idArtikla;
	}


	public Long getIdProdavca() {
		return idProdavca;
	}


	public void setIdProdavca(Long idProdavca) {
		this.idProdavca = idProdavca;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public String getOpis() {
		return opis;
	}


	public void setOpis(String opis) {
		this.opis = opis;
	}


	public Double getCena() {
		return cena;
	}


	public void setCena(Double cena) {
		this.cena = cena;
	}


	public String getPutanjaSlike() {
		return putanjaSlike;
	}


	public void setPutanjaSlike(String putanjaSlike) {
		this.putanjaSlike = putanjaSlike;
	}
	
	
	
	
	
	
}
