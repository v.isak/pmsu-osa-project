package com.projekat.pmsu_osa.models.dto;

import java.io.Serializable;

import com.projekat.pmsu_osa.models.entity.Stavka;

public class SlikaDTO implements Serializable {

	private String slikaString;
	private String slikaRelativePath;
	
	
	public SlikaDTO() {
		super();
	}
	
	public SlikaDTO(String slikaString) {
		super();
		this.slikaString = slikaString;
	}
	
	public SlikaDTO(String slikaString, String slikaRelativePath) {
		super();
		this.slikaString = slikaString;
		this.slikaRelativePath = slikaRelativePath;
	}
	
	
	public String getSlikaString() {
		return slikaString;
	}
	public void setSlikaString(String slikaString) {
		this.slikaString = slikaString;
	}
	public String getSlikaRelativePath() {
		return slikaRelativePath;
	}
	public void setSlikaRelativePath(String slikaRelativePath) {
		this.slikaRelativePath = slikaRelativePath;
	}
	
}
