package com.projekat.pmsu_osa.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projekat.pmsu_osa.models.entity.Kupac;
import com.projekat.pmsu_osa.models.entity.Prodavac;


public interface ProdavacRepozitorijum extends JpaRepository<Prodavac, Long>{

	Optional<Prodavac> findFirstByKorisnickoIme(String korisnickoIme);

	Prodavac findByKorisnickoIme(String korisnickoIme);
}
