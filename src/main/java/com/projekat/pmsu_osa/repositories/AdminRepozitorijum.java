package com.projekat.pmsu_osa.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projekat.pmsu_osa.models.entity.Administrator;

public interface AdminRepozitorijum extends JpaRepository<Administrator, Long>{

	Optional<Administrator> findFirstByKorisnickoIme(String korisnickoIme);
	
	Administrator findByKorisnickoIme(String korisnickoIme);
}
