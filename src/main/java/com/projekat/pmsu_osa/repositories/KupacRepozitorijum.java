package com.projekat.pmsu_osa.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projekat.pmsu_osa.models.entity.Administrator;
import com.projekat.pmsu_osa.models.entity.Kupac;

public interface KupacRepozitorijum extends JpaRepository<Kupac, Long>{

	Optional<Kupac> findFirstByKorisnickoIme(String korisnickoIme);

	Kupac findByKorisnickoIme(String korisnickoIme);
}
