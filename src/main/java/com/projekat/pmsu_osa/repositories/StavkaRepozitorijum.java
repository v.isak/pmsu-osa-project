package com.projekat.pmsu_osa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projekat.pmsu_osa.models.entity.Stavka;

public interface StavkaRepozitorijum extends JpaRepository<Stavka, Long>{

}
