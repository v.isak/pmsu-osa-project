package com.projekat.pmsu_osa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projekat.pmsu_osa.models.entity.Porudzbina;

public interface PorudzbinaRepozitorijum extends JpaRepository<Porudzbina, Long>{

}
