package com.projekat.pmsu_osa.services.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.projekat.pmsu_osa.models.entity.Administrator;
import com.projekat.pmsu_osa.models.entity.Artikl;
import com.projekat.pmsu_osa.models.entity.Kupac;
import com.projekat.pmsu_osa.repositories.ArtiklRepozitorijum;
import com.projekat.pmsu_osa.repositories.KupacRepozitorijum;
import com.projekat.pmsu_osa.services.IKupacService;

@Service
public class KupacService implements IKupacService{
	
	@Autowired
    private KupacRepozitorijum kupacRepozitorijum;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

    @Override
    public Kupac findOne(Long id) {
        return kupacRepozitorijum.getOne(id);
    }
    
    @Override
    public Kupac findByUsername(String username) {
        return kupacRepozitorijum.findByKorisnickoIme(username);
    }


    @Override
    public List<Kupac> findAll() {
        return kupacRepozitorijum.findAll();
    }

    @Override
    public Kupac save(Kupac kupac) {
    	Optional<Kupac> kupacRep = kupacRepozitorijum.findFirstByKorisnickoIme(kupac.getKorisnickoIme());
    	
        return kupacRepozitorijum.save(kupac);
    }

    @Override
    public void remove(Long id) {
    	Kupac kupac = kupacRepozitorijum.getOne(id);
    	kupacRepozitorijum.delete(kupac);
    }
	
	
}
