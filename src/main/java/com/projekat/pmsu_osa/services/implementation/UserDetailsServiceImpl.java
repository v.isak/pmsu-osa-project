package com.projekat.pmsu_osa.services.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.projekat.pmsu_osa.models.entity.Administrator;
import com.projekat.pmsu_osa.models.entity.Korisnik;
import com.projekat.pmsu_osa.models.entity.Kupac;
import com.projekat.pmsu_osa.models.entity.Prodavac;
import com.projekat.pmsu_osa.services.IAdministratorService;
import com.projekat.pmsu_osa.services.IKupacService;
import com.projekat.pmsu_osa.services.IProdavacService;

@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private IAdministratorService adminService;
	
	@Autowired
	private IKupacService kupacService;
	
	@Autowired
	private IProdavacService prodavacService;
	
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
	
		Administrator admin = adminService.findByUsername(username);
		Kupac kupac = kupacService.findByUsername(username);
		Prodavac prodavac = prodavacService.findByUsername(username);
		
		// nije pronadjen
		if(admin == null && kupac == null && prodavac == null) {
			
			throw new UsernameNotFoundException("Ne postoji korisnik sa korisnickim imenom: '"+ username + "'");
		}
		
		// pronadjen
		else {
			// admin
			if (admin != null) {
				if (admin.isBlokiran()) {
					throw new UsernameNotFoundException("Korisnik sa korisnickim imenom: '"+ username + "' je blokiran");
				}
				
				List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
				String role = "ROLE_" + admin.getTipKorisnika().toString();
				grantedAuthorities.add(new SimpleGrantedAuthority(role));
			
				return new org.springframework.security.core.userdetails.User(
						admin.getKorisnickoIme().trim(), 
						admin.getLozinka().trim(), 
						grantedAuthorities);
			}
			
			// kupac
			else if (kupac != null) {
				if (kupac.isBlokiran()) {
					throw new UsernameNotFoundException("Korisnik sa korisnickim imenom: '"+ username + "' je blokiran");
				}
				
				List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
				String role = "ROLE_" + kupac.getTipKorisnika().toString();
				grantedAuthorities.add(new SimpleGrantedAuthority(role));
			
				return new org.springframework.security.core.userdetails.User(
						kupac.getKorisnickoIme().trim(), 
						kupac.getLozinka().trim(), 
						grantedAuthorities);
			}
			
			// prodavac
			else if (prodavac != null) {
				if (prodavac.isBlokiran()) {
					throw new UsernameNotFoundException("Korisnik sa korisnickim imenom: '"+ username + "' je blokiran");
				}
				
				List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
				String role = "ROLE_" + prodavac.getTipKorisnika().toString();
				grantedAuthorities.add(new SimpleGrantedAuthority(role));
			
				return new org.springframework.security.core.userdetails.User(
						prodavac.getKorisnickoIme().trim(), 
						prodavac.getLozinka().trim(), 
						grantedAuthorities);
			}
			
			// stavljeno radi greskse al ne bi trebalo da dodje do ovoga
			else {
				throw new UsernameNotFoundException("Ne postoji korisnik sa korisnickim imenom: '"+ username + "'");
			}
		
		}
	
	}
	
	public Korisnik loadUserObjectByUsername(String username) throws UsernameNotFoundException{
	
		Administrator admin = adminService.findByUsername(username);
		Kupac kupac = kupacService.findByUsername(username);
		Prodavac prodavac = prodavacService.findByUsername(username);
		
		// nije pronadjen
		if(admin == null && kupac == null && prodavac == null) {
			
			throw new UsernameNotFoundException("Ne postoji korisnik sa korisnickim imenom: '"+ username + "'");
		}
		
		// pronadjen
		else {
			// admin
			if (admin != null) {
				return (Korisnik) admin;
			}
			
			// kupac
			else if (kupac != null) {
				return (Korisnik) kupac;
			}
			
			// prodavac
			else if (prodavac != null) {
				return (Korisnik) prodavac;
			}
			
			// stavljeno radi greskse al ne bi trebalo da dodje do ovoga
			else {
				throw new UsernameNotFoundException("Ne postoji korisnik sa korisnickim imenom: '"+ username + "'");
			}
		
		}
	
	}
	
}
