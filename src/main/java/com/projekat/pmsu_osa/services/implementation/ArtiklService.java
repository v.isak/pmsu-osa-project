package com.projekat.pmsu_osa.services.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projekat.pmsu_osa.models.entity.Akcija;
import com.projekat.pmsu_osa.models.entity.Artikl;
import com.projekat.pmsu_osa.repositories.AkcijaRepozitorijum;
import com.projekat.pmsu_osa.repositories.ArtiklRepozitorijum;
import com.projekat.pmsu_osa.services.IArtiklService;

@Service
public class ArtiklService implements IArtiklService{

	@Autowired
    private ArtiklRepozitorijum artiklRepozitorijum;

    @Override
    public Artikl findOne(Long id) {
        return artiklRepozitorijum.getOne(id);
    }

    @Override
    public List<Artikl> findAll() {
        return artiklRepozitorijum.findAll();
    }

    @Override
    public Artikl save(Artikl artikl) {
        return artiklRepozitorijum.save(artikl);
    }

    @Override
    public void remove(Long id) {
    	Artikl artikl = artiklRepozitorijum.getOne(id);
    	artiklRepozitorijum.delete(artikl);
    }
	
}
