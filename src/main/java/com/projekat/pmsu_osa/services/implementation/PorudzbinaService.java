package com.projekat.pmsu_osa.services.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projekat.pmsu_osa.models.entity.Kupac;
import com.projekat.pmsu_osa.models.entity.Porudzbina;
import com.projekat.pmsu_osa.repositories.KupacRepozitorijum;
import com.projekat.pmsu_osa.repositories.PorudzbinaRepozitorijum;
import com.projekat.pmsu_osa.services.IPorudzbinaService;

@Service
public class PorudzbinaService implements IPorudzbinaService{

	@Autowired
    private PorudzbinaRepozitorijum porudzbinaRepozitorijum;

    @Override
    public Porudzbina findOne(Long id) {
        return porudzbinaRepozitorijum.getOne(id);
    }

    @Override
    public List<Porudzbina> findAll() {
        return porudzbinaRepozitorijum.findAll();
    }

    @Override
    public Porudzbina save(Porudzbina porudzbina) {
        return porudzbinaRepozitorijum.save(porudzbina);
    }

    @Override
    public void remove(Long id) {
    	Porudzbina porudzbina = porudzbinaRepozitorijum.getOne(id);
    	porudzbinaRepozitorijum.delete(porudzbina);
    }
	
}
