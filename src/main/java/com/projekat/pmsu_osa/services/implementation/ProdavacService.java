package com.projekat.pmsu_osa.services.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.projekat.pmsu_osa.models.entity.Kupac;
import com.projekat.pmsu_osa.models.entity.Porudzbina;
import com.projekat.pmsu_osa.models.entity.Prodavac;
import com.projekat.pmsu_osa.repositories.PorudzbinaRepozitorijum;
import com.projekat.pmsu_osa.repositories.ProdavacRepozitorijum;
import com.projekat.pmsu_osa.services.IProdavacService;

@Service
public class ProdavacService implements IProdavacService{
	
	@Autowired
    private ProdavacRepozitorijum prodavacRepozitorijum;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

    @Override
    public Prodavac findOne(Long id) {
        return prodavacRepozitorijum.getOne(id);
    }
    
    @Override
    public Prodavac findByUsername(String username) {
        return prodavacRepozitorijum.findByKorisnickoIme(username);
    }

    @Override
    public List<Prodavac> findAll() {
        return prodavacRepozitorijum.findAll();
    }

    @Override
    public Prodavac save(Prodavac prodavac) {
    	Optional<Prodavac> prodavacRep = prodavacRepozitorijum.findFirstByKorisnickoIme(prodavac.getKorisnickoIme());
    	
        return prodavacRepozitorijum.save(prodavac);
    }

    @Override
    public void remove(Long id) {
    	Prodavac prodavac = prodavacRepozitorijum.getOne(id);
    	prodavacRepozitorijum.delete(prodavac);
    }
	
}
