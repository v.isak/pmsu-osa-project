package com.projekat.pmsu_osa.services.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projekat.pmsu_osa.models.entity.Administrator;
import com.projekat.pmsu_osa.models.entity.Akcija;
import com.projekat.pmsu_osa.repositories.AdminRepozitorijum;
import com.projekat.pmsu_osa.repositories.AkcijaRepozitorijum;
import com.projekat.pmsu_osa.services.IAkcijaService;

@Service
public class AkcijaService implements IAkcijaService {

	@Autowired
    private AkcijaRepozitorijum akcijaRepozitorijum;

    @Override
    public Akcija findOne(Long id) {
        return akcijaRepozitorijum.getOne(id);
    }

    @Override
    public List<Akcija> findAll() {
        return akcijaRepozitorijum.findAll();
    }

    @Override
    public Akcija save(Akcija akcija) {
        return akcijaRepozitorijum.save(akcija);
    }

    @Override
    public void remove(Long id) {
    	Akcija akcija = akcijaRepozitorijum.getOne(id);
    	akcijaRepozitorijum.delete(akcija);
    }
	
}
