package com.projekat.pmsu_osa.services.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projekat.pmsu_osa.models.entity.Prodavac;
import com.projekat.pmsu_osa.models.entity.Stavka;
import com.projekat.pmsu_osa.repositories.ProdavacRepozitorijum;
import com.projekat.pmsu_osa.repositories.StavkaRepozitorijum;
import com.projekat.pmsu_osa.services.IStavkaService;

@Service
public class StavkaService implements IStavkaService{

	@Autowired
    private StavkaRepozitorijum stavkaRepozitorijum;

    @Override
    public Stavka findOne(Long id) {
        return stavkaRepozitorijum.getOne(id);
    }

    @Override
    public List<Stavka> findAll() {
        return stavkaRepozitorijum.findAll();
    }

    @Override
    public Stavka save(Stavka stavka) {
        return stavkaRepozitorijum.save(stavka);
    }

    @Override
    public void remove(Long id) {
    	Stavka stavka = stavkaRepozitorijum.getOne(id);
    	stavkaRepozitorijum.delete(stavka);
    }
	
}
