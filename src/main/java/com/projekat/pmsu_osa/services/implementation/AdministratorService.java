package com.projekat.pmsu_osa.services.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.projekat.pmsu_osa.models.entity.Administrator;
import com.projekat.pmsu_osa.repositories.AdminRepozitorijum;
import com.projekat.pmsu_osa.services.IAdministratorService;

@Service
public class AdministratorService implements IAdministratorService{
	
	@Autowired
    private AdminRepozitorijum adminRepozitorijum;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

    @Override
    public Administrator findOne(Long id) {
        return adminRepozitorijum.getOne(id);
    }
    
    @Override
    public Administrator findByUsername(String username) {
        return adminRepozitorijum.findByKorisnickoIme(username);
    }

    @Override
    public List<Administrator> findAll() {
        return adminRepozitorijum.findAll();
    }

    @Override
    public Administrator save(Administrator administrator) {
    	Optional<Administrator> adminRep = adminRepozitorijum.findFirstByKorisnickoIme(administrator.getKorisnickoIme());
    	
        return adminRepozitorijum.save(administrator);
    }

    @Override
    public void remove(Long id) {
        Administrator administrator = adminRepozitorijum.getOne(id);
        adminRepozitorijum.delete(administrator);
    }
	
}
