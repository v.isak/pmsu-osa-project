package com.projekat.pmsu_osa.services;

import java.util.List;

import com.projekat.pmsu_osa.models.entity.Akcija;

public interface IAkcijaService {

	Akcija findOne(Long id);

    List<Akcija> findAll();

    Akcija save(Akcija akcija);

    void remove(Long id);
	
}
