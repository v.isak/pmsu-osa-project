package com.projekat.pmsu_osa.services;

import java.util.List;

import com.projekat.pmsu_osa.models.entity.Porudzbina;

public interface IPorudzbinaService {

	Porudzbina findOne(Long id);

    List<Porudzbina> findAll();

    Porudzbina save(Porudzbina porudzbina);

    void remove(Long id);
	
}
