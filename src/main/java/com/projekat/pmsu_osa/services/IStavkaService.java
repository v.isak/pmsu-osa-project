package com.projekat.pmsu_osa.services;

import java.util.List;

import com.projekat.pmsu_osa.models.entity.Stavka;

public interface IStavkaService {

	Stavka findOne(Long id);

    List<Stavka> findAll();

    Stavka save(Stavka stavka);

    void remove(Long id);
	
}
