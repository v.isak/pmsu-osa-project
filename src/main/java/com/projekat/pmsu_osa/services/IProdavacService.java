package com.projekat.pmsu_osa.services;

import java.util.List;

import com.projekat.pmsu_osa.models.entity.Prodavac;

public interface IProdavacService {

	Prodavac findOne(Long id);

    List<Prodavac> findAll();

    Prodavac save(Prodavac prodavac);

    void remove(Long id);

	Prodavac findByUsername(String username);
	
}
