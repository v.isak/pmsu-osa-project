package com.projekat.pmsu_osa.services;

import java.util.List;

import com.projekat.pmsu_osa.models.entity.Artikl;

public interface IArtiklService {

	Artikl findOne(Long id);

    List<Artikl> findAll();

    Artikl save(Artikl artikl);

    void remove(Long id);
	
}
