package com.projekat.pmsu_osa.services;


import java.util.List;

import com.projekat.pmsu_osa.models.entity.Administrator;

public interface IAdministratorService {

    Administrator findOne(Long id);

    List<Administrator> findAll();

    Administrator save(Administrator administrator);

    void remove(Long id);

	Administrator findByUsername(String username);
}
