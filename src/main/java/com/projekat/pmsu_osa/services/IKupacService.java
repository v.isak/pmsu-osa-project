package com.projekat.pmsu_osa.services;

import java.util.List;

import com.projekat.pmsu_osa.models.entity.Kupac;

public interface IKupacService {

	Kupac findOne(Long id);

    List<Kupac> findAll();

    Kupac save(Kupac kupac);

    void remove(Long id);

	Kupac findByUsername(String username);
	
}
